// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.util;

import net.sourceforge.dibdib.all.join.CsvCodecIf;

import java.util.Arrays;
import net.sourceforge.dibdib.config.*;

/** Basic methods plus dummy implementation of interface for debugging. */
public class CsvCodec0 implements CsvCodecIf {
//=====

public static final CsvCodec0 instance = new CsvCodec0();

@Override
public void init( char platform, Object... parameters ) {
}

@Override
public byte[] compress( byte[] xyData, int xOffset4Reuse, int to ) {
	return Arrays.copyOfRange( xyData, xOffset4Reuse, to );
}

@Override
public byte[] decompress( byte[] xData, int length ) {
	return Arrays.copyOf( xData, length );
}

@Override
public byte[] encode( byte[] compressedData, int from, int to, byte[] key, int keyInfo, byte[] keyData, byte[] signatureKey )
	throws Exception {
	byte[] out = encode4DerivedKey( compressedData, from, to, key );
	return Arrays.copyOf( out, out.length );
}

@Override
/** For debugging: use '0' for key, ignoring pass */
public byte[] encode4DerivedKey( byte[] compressedData, int from, int to, byte[] pass ) throws Exception {
	byte[] key = UtilMisc.createHeaderSalt16( Dib2Constants.magicBytes, new byte[] { 1, 0, 0 }, this );
	byte[] out = Arrays.copyOf( key, to - from + key.length );
	key = new byte[ 16 ];
	for (int i0 = to - from - 1; i0 >= 0; -- i0) {
		out[ key.length + i0 ] = (byte) (key[ i0 % 16 ] ^ compressedData[ from + i0 ]);
	}
	return UtilMisc.toPacket( Dib2Constants.RFC4880_EXP2, null, out, 0, out.length );
}

@Override
/** For debugging: pass is not used */
public byte[] decode( byte[] data, int offset, int len, byte[] keyOrPass, byte[] signatureKey ) throws Exception {
	int offs = offset;
	if (data[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		offs += UtilMisc.getPacketHeaderLen( data, offs );
	} else {
		if ((data[ offs ] != Dib2Constants.magicBytes[ 0 ]) || (data[ offs + Dib2Constants.magicBytes.length + 1 ] != getMethodTag())) {
			return null;
		}
	}
	return Arrays.copyOfRange( data, offs + 16, offset + len );
}

@Override
public byte getMethodTag() {
	return '0'; // dummy - plain
}

//@Override
//public StringBuffer getLog() {
//	return null;
//}

@Override
public byte[] getInitialValue( int len ) {
	return Arrays.copyOf( "0123456789".getBytes(), len );
}

public static byte[] readEncoded( String path, byte[] pass, byte[] yHeader ) {
	//int[] yVersion ) {
	byte[] dat = null;
//	int iHeader1 = -1;
//	int iHeader2 = -1;
	CsvCodecIf decoder = Dib2Config.csvCodecs[ 0 ];
	if (null != yHeader) {
		yHeader[ 0 ] = 0;
	}
	try {
//		byte[] salted16 = new byte[ 16 ];
//		System.arraycopy( xMagicBytes, 0, salted16, 0, xMagicBytes.length );
		dat = UtilMisc.readFile( path, Dib2Constants.fileStrucVersion );
		for (CsvCodecIf c0 : Dib2Config.csvCodecs) {
			int hdlen = UtilMisc.getPacketHeaderLen( dat, 0 );
			if (dat[ hdlen + 3 ] == c0.getMethodTag()) {
				if (null != yHeader) {
					hdlen = (hdlen > yHeader.length) ? yHeader.length : hdlen;
					System.arraycopy( dat, 0, yHeader, 0, hdlen );
				}
				decoder = c0;
				break;
			}
		}
		dat = decoder.decode( dat, 0, dat.length, pass, null );
		dat = decoder.decompress( dat, dat.length );
//		iHeader1 = 1 + UtilMisc.indexOf( dat, new byte[] { '\t', Dib2Constants.magicBytes[ 0 ], Dib2Constants.magicBytes[ 1 ] } );
//		iHeader2 = UtilMisc.indexOf( dat, new byte[] { '\n' } );
	} catch (Exception e) {
//		StringBuffer buf = decoder.getLog();
//		if (null != buf) {
//			buf.append( "\nINFO Data not readable/ not given/ decoding error. Starting with empty file/ " + path + "/ " + e.getMessage() );
//		}
		Dib2Config.log( "codec", "Error: " + e + e.getMessage() + "/ " + path );
	}
//	if ((iHeader1 <= 2) || (iHeader1 >= iHeader2) || (dat[ iHeader1 + 3 ] < '0') || (dat[ iHeader1 + 3 ] > '9')) {
//		StringBuffer buf = decoder.getLog();
//		buf.append( "\nINFO Header error/ " + path + " " + iHeader1 + iHeader2 );
//		return null;
//	}
//	String version = new String( Arrays.copyOfRange( dat, iHeader1 + 2, iHeader2 ) );
//	version = version.replaceAll( "[^0-9]", "" );
//	if (null != yVersion) {
//		yVersion[ 0 ] = Integer.parseInt( version );
//	}
	return dat;
}

public static int writeEncoded(
	byte[] txt,
	int from, int to, String path, byte[] pass ) {
//	String header = new String( Dib2Constants.magicBytes );
//	header = header + "\t" + UtilMisc.toDate4Millis() //,
//		+ "\t" + header + ' ' + Dib2Constants.VERSION_STRING + '\n';
//	byte[] hd = headerWithMagicBytes.getBytes();
	int rc = -1;
	try {
//		offset -= hd.length;
//		int len = length; // + hd.length;
//		System.arraycopy( hd, 0, txt, offset, hd.length );
		rc = -2;
		byte[] compr = Dib2Config.csvCodecs[ 0 ].compress( txt, from, to );
		rc = -3;
		final int coFrom = compr[ 0 ] + UtilMisc.getPacketHeaderLen( compr, compr[ 0 ] );
		final int coTo = coFrom + UtilMisc.getPacketLen( compr, 1 + compr[ 0 ] );
		byte[] enc = Dib2Config.csvCodecs[ 0 ].encode4DerivedKey( compr, coFrom, coTo, pass );
		rc = -4;
		UtilMisc.writeFile( path, enc, 0, enc.length, null );
	} catch (Exception e) {
		Dib2Config.log( "codec", "error " + e + e.getMessage() );
		return rc;
	}
	return 0;
}

//=====
}
