// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.map;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import net.sourceforge.dibdib.all.util.UtilMisc;
import net.sourceforge.dibdib.config.Dib2Constants;

/**
 * Element (record) of categorical database (currently saved as CSV line), also used as Token:
 * name (atomic) + propositional main category (+ secondary categories)
 * + time stamp + source of information
 * + (propositional) data.
 * (Constraint logic, name as partial ID)
 * Prolog interpretation: (CATEGORY ATOM), (CATEGORY.DATA_LABEL ATOM DATA_ENTRY).
 */
public class QMapping_1 {
//=====

public static final String version = "0.2";
public static final String fieldNames = //,
	"" // OID
		+ "\tNAME" //,
		+ "\tCATS" //,
		+ "\tTIME" //,
		+ "\tCONTRIB" //,
		+ "\tSRCS" //,
		+ "\tRECV" //,
		+ "\tDAT1" //,
		+ "\tDAT2" //,
		+ "\tDAT3" //,
		+ "\tDAT4" //,
		+ "\tDAT5" //,
		+ "\tDAT6" //,
		+ "\tDAT7" //,
		+ "\tDAT8" //,
		+ "\tDAT9";

///// -- Immutable - 'as if' (whenever not enforced by Java)

/** Object ID, locally unique, eventual consistency (= label for pre-defined categories) */
public final String oid;

/** Name of (quasi-atomic) element, e.g. 'Charlie Brown' or 'Brown.Charlie' */
public final String label;

public final String[] categoryOids;

// Nickname not stored here.
// Time stamp below.

public final String contributorOid;
public final String[] sources;
public final String[] shareReceiverOids;
public final String[] listElements;

/** Shash of atom */
public final String shash;

public final String[] listShashes;

///// -- Mutable (for ease of Token handling)

/** Optionally negative for temporary data. */
public long qTimeStamp;

///** Name + timeStamp + ... + original value string. */
//public QMapping_1( String xmLabel, String xmCategoryOid, long xmTimeStamp, String xmContributorOid, String xmValue ) {
//	oid = UtilMisc.createId( (0 < xmValue.length()) ? xmValue : xmLabel );
//	label = xmLabel;
//	categoryOids = new String[] { xmCategoryOid };
//	qTimeStamp = xmTimeStamp;
//	contributorOid = xmContributorOid;
//	originalList = xmValue;
//
//	// TODO
//	shash = null;
//	listShashes = null;
//	listElements = null;
//	sources = null;
//	shareReceiverOids = null;
//}

/** OID/null + Name + Cats + timeStamp/-1 + contributorOid + values. */
public QMapping_1( String objectId, String xmLabel, String xmCategoryOids, long xmTimeStamp, String xmContributorOid, String... xmValues ) {
	oid = (null != objectId) //,
	? objectId //,
		: UtilMisc.createId( ((0 < xmValues.length) && (0 < xmValues[ 0 ].length())) ? xmValues[ 0 ] : xmLabel );
	label = xmLabel;
	categoryOids = xmCategoryOids.split( "[^0-~]+" );
	qTimeStamp = (0 <= xmTimeStamp) ? xmTimeStamp : UtilMisc.currentTimeMillisLinearized();
	contributorOid = (null == xmContributorOid) ? "NN" : xmContributorOid;
	listElements = xmValues;

	// TODO
	listShashes = null;
	shash = null;
	sources = null;
	shareReceiverOids = null;
}

public QMapping_1( String[] a0, int iOid, int flagsMarkShift4Time ) {
	oid = a0[ 0 ];
	label = a0[ 1 + iOid ];
	categoryOids = a0[ 2 + iOid ].split( "[^0-~]+" );
	long time = UtilMisc.toMillis4Date( a0[ 3 ] );
	if (0 != (flagsMarkShift4Time & 1)) {
		time = time ^ (time & Dib2Constants.TIME_SHIFTED);
	}
	long min = (0 != (flagsMarkShift4Time & 2)) ? 1 : time;
	qTimeStamp = UtilMisc.alignTime( time, min );
	contributorOid = a0[ 4 + 2 * iOid ];
	listElements = Arrays.copyOfRange( a0, 7 + 3 * iOid, a0.length );

	// TODO
	listShashes = null;
	shash = null;
	sources = null;
	shareReceiverOids = null;
}

@Override
public String toString() {
	return label + '@' + UtilMisc.toShortDate4Millis( qTimeStamp ) + '=' //,
		+ ((0 < listElements.length) ? listElements[ 0 ] : "");
}

/** Create encoded list of mappings with header "dm(TTT)N.N" (time TTT, version N.N).
 * -- "dm(..)N.N" starts plain-text CSV with header row (version N.N).
 * -- "dm^Cxxx" precedes encoded container as salt value.
*/
public static byte[] toCsvMap( QMapping_1[] entries, int count ) {
	String header = new String( Dib2Constants.magicBytes );
	// old: header += "\t" + UtilMisc.toDate4Millis() + ... + '\n';
	header += "(" + UtilMisc.toShortDate4Millis() + ')' + version + fieldNames + '\n';
	StringBuilder out = new StringBuilder( header );
	for (QMapping_1 entry : entries) {
		if (0 >= count) {
			break;
		}
		-- count;
		out.append( entry.oid );
		out.append( '\t' ).append( entry.label );
		out.append( '\t' ).append( (0 < entry.categoryOids.length) ? entry.categoryOids[ 0 ] : "" );
		for (int i0 = 1; i0 < entry.categoryOids.length; ++ i0) {
			out.append( ' ' ).append( entry.categoryOids[ i0 ] );
		}
		out.append( '\t' ).append( UtilMisc.toDate4Millis( entry.qTimeStamp ) );
		out.append( '\t' ).append( entry.contributorOid );
		out.append( "\t\t" ); // sources, recv
		for (String el : entry.listElements) {
			out.append( '\t' ).append( el.replace( '\n', '\t' ) );
		}
		out.append( '\n' );
	}
	try {
		return out.toString().getBytes( "UTF-8" );
	} catch (UnsupportedEncodingException e) {
	}
	return out.toString().getBytes();
}

public static QMapping_1[] fromCsvMap( byte[] map, int flagsMarkShift4Time ) {

	if (map.length <= 2) {
		return new QMapping_1[ 0 ];
	}
	int i1 = 1;
	int count = 0;
	int i0 = 0;
	int iOid = 0;
	QMapping_1[] out = new QMapping_1[ 24 ];
	if ((map[ 0 ] == Dib2Constants.magicBytes[ 0 ]) && (map[ 1 ] == Dib2Constants.magicBytes[ 1 ])) {
		// Skip header:
		i0 = 1 + UtilMisc.indexOf( map, new byte[] { '\n' } );
		iOid = (map[ 2 ] == '\t') ? -1 : 0;
	}
	for (; i0 < map.length; i0 = i1 + 1) {
		i1 = UtilMisc.indexOf( map, new byte[] { '\n' }, i0 );
		if (i1 < 0) {
			i1 = map.length;
		}
		String line;
		try {
			line = new String( map, i0, i1 - i0, "UTF-8" );
		} catch (UnsupportedEncodingException e) {
			line = new String( map, i0, i1 - i0 );
		}
		if ((line.indexOf( "\t" ) < 0) && (line.indexOf( "," ) > 0)) {
			line = line.replaceAll( "\"? *, *\"?", "\t" );
		}
		String[] a0 = line.split( "\t" ); //, 6 + iOid );
		if (5 > a0.length) {
			continue;
		}
		try {
			if (count >= out.length) {
				out = Arrays.copyOf( out, 2 * count );
			}
			out[ count ] = new QMapping_1( a0, iOid, flagsMarkShift4Time );
			++ count;
		} catch (Exception e) {
			// Ignore.
		}
	}
	return Arrays.copyOf( out, count );
}
//=====
}
