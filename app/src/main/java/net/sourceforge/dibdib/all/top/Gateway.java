// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

import java.util.HashMap;

public final class Gateway {
//=====

public static HashMap< String, Pipe > pipeRegistry = new HashMap< String, Pipe >();
public static HashMap< String, ThreadUser > threadUserRegistry = new HashMap< String, ThreadUser >();

public static Pipe[] forceInit;

//=====
}
