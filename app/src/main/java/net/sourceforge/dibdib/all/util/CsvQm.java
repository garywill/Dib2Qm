// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.util;

import java.io.File;
import java.util.*;
import net.sourceforge.dibdib.all.join.ContextIf;
import net.sourceforge.dibdib.all.map.QMapping_1;
import net.sourceforge.dibdib.all_joined.chat.Contact;
import net.sourceforge.dibdib.config.Dib2Config;
import net.vreeken.quickmsg.message;

/**
 * Temporary simplification for categorical database: see QMapping.
 * ==> CSV with tabs (commas if tabs missing) as single line (trailing '\n' as '\t'):
 */
public class CsvQm {
//=====

public final String qDbFileName = "DibdibQM.dm";
public final ContextIf context;

protected static boolean zByNames = false;
private static byte[] zPhrase = null; // access_code + imap_pass
private static long zLastSave = 0;
private static boolean zLoadSuccess = false;
private static boolean zStray = false;
private static ArrayList< Contact > zContacts = new ArrayList< Contact >();
private static HashMap< Long, message > zMessages = new HashMap< Long, message >();
protected static HashMap< String, byte[] > zPrefs = new HashMap< String, byte[] >();

public CsvQm( ContextIf context ) {
	// Might be called from subclass:
	this.context = (null == context) ? ((ContextIf) this) : context;
	if (0 >= zPrefs.size()) {
		zPrefs.put( "pub", new byte[ 0 ] );
		zPrefs.put( "sec", new byte[ 0 ] );
//		zPrefs.put( "imap_pass", new byte[ 0 ] );
		zContacts
			.add( new Contact( Contact.TYPE_PERSON, Contact.KEYSTAT_SENT, "NN", "N.N.",
				UtilMisc.toMillis4Date( "990815.08:15" ) ) );
	}
}

/** Setter.
 * @param wPhrase not cloned!
 */
public void setPhrase( byte[] wPhrase ) {
//	if (null != wPhrase) {
	zPhrase = wPhrase;
	zLastSave = UtilMisc.currentTimeMillisLinearized();
//	}
}

public void setByNames( boolean byNames ) {
	zByNames = byNames;
}

public synchronized void close() {
	save();
}

public synchronized void save() {
	write( new File( context.getFilesDir(), qDbFileName ), true, true );
}

public String fileReady() { // boolean checkAccess ) {
//	if ((null == zPhrase) && checkAccess) {
//		return null;
//	}
	File file = new File( context.getFilesDir(), qDbFileName );
	return file.isFile() ? file.getAbsolutePath() : null;
}

public void write() {
	write( new File( context.getFilesDir(), qDbFileName ), false, true );
}

public synchronized void write( File file, boolean immediately, boolean backupOld ) {
	if (null == zPhrase) {
		return;
	}
	if (!immediately && ((zLastSave + 60 * 1000) >= UtilMisc.currentTimeMillisLinearized())) {
		return;
	}
	Dib2Config.log( "save", " .. " + zContacts.size() );
	String path = file.getAbsolutePath() + ".tmp";
	File pathF = new File( path );
	if (pathF.isFile()) {
		pathF.delete();
	}
	zLastSave = UtilMisc.currentTimeMillisLinearized();
//	int[] aLen = new int[ 1 ];
//	for (int iCodec = 0; iCodec < Dib2Config.csvCodecs.length; ++ iCodec) {
	byte[] dat = exportLines(); // 100, aLen );
	Dib2Config.log( "exportLines", "ok " + dat.length );

//		String header = new String( Dib2Constants.magicBytes );
//		header = header + "\t" + UtilMisc.toDate4Millis() //,
//			+ "\t" + header + ' ' + Dib2Constants.VERSION_STRING + '\n';

	int ok = CsvCodec0.writeEncoded( dat, 0, dat.length, path, zPhrase );
	Dib2Config.log( "save", "ok? " + ok );
	if (0 <= ok) {
		if (file.isFile()) {
			if (backupOld) {
				path = file.getAbsolutePath();
				File old = new File( path + (immediately ? ".old" : ".bak") );
				if (old.exists()) {
					if (!immediately || !zLoadSuccess) {
						old = new File( path + ".bak" );
					}
					old.delete();
				}
				file.renameTo( old );
			} else {
				file.delete();
			}
		}
		pathF.renameTo( file );
	}
	zLastSave = UtilMisc.currentTimeMillisLinearized();
}

public int load( boolean useBackup ) {
	if (null == zPhrase) {
		return -1;
	}
	int rc = load( qDbFileName );
	Dib2Config.log( "load", "" + rc + " .. " + qDbFileName );
	if ((0 <= rc) || !useBackup) {
		zLoadSuccess = (0 < rc);
		return rc;
	}
//	Dib2Config.log( "load", Dib2Config.csvCodecs[ 0 ].getLog().toString() );
	Dib2Config.log( "load", " ..bak " );
	rc = load( qDbFileName + ".bak" );
	if (0 <= rc) {
		return rc;
	}
	Dib2Config.log( "load", " ..old " );
	return load( qDbFileName + ".old" );
}

protected int load( String fileName ) {
	if (null == zPhrase) {
		return -1;
	}
	Dib2Config.log( "load", " .. " + zContacts.size() );
	String path = new File( context.getFilesDir(), fileName ).toString();
	int out = importFile( path, zPhrase, true );
	zLastSave = UtilMisc.currentTimeMillisLinearized();
	return out;
}

public synchronized int importFile( String filePath, byte[] phrase, boolean replace ) {
//	int[] version = new int[ 1 ];
	byte[] header = new byte[ 4 ];
	byte[] dat = CsvCodec0.readEncoded( filePath, ((null == phrase) ? zPhrase : phrase), header );
	int version = header[ 2 ] & 0xff;
	if (null == dat) {
		Dib2Config.log( "import", "read failed." );
		return -1;
	}
	int count = 0;
	try {
		count = importLines( dat, replace, version );
	} catch (Exception e) {
		Dib2Config.log( "import", e.getMessage() );
		return -1;
	}
	Dib2Config.log( "import", "" + count );
	return count;
}

public synchronized void contact_add( Contact it ) {
	Dib2Config.log( "contact_add", "contact: " + it.address_get() );
	it.name_set( it.name_get().replace( '\t', ' ' ).replace( '\n', ' ' ) );
	it.address_set( it.address_get().replace( " ", "" ) );
//	Contact other = contact_get_by_name( it.name_get() );
//	other = (null == other) ? contact_get_person_by_address( it.address_get() ) //-
//		: ((other.address_get().equals( it.address_get() )) ? other : null);
	Contact other = contact_get_person_by_address( it.address_get() );
	if ((null != other) && (it.type_get() == Contact.TYPE_PERSON)) {
//		if (other.address_get().equals( it.address_get() )) {
		it.id_set( other.id_get() );
		String v0;
		v0 = it.name_get();
		if ((null == v0) || (0 >= v0.length()))
			it.name_set( other.name_get() );
		v0 = it.phone_get();
		if ((null == v0) || (0 >= v0.length()))
			it.phone_set( other.phone_get() );
		v0 = it.notes_get();
		if ((null == v0) || (0 >= v0.length()))
			it.notes_set( other.notes_get() );
		contact_update( it );
//		}
		return;
	}
	other = null;
	if (it.type_get() == Contact.TYPE_PERSON) {
		Contact check_contact = contact_get_person_by_address( it.address_get() );
		if (check_contact != null) {
			Dib2Config.log( "contact_add", "contact exists with address " + it.address_get() );
			return;
		}
	} else {
		Contact check_contact = contact_get_group_by_address_and_group( it.address_get(), it.group_get() );
		if (check_contact != null) {
			Dib2Config.log( "contact_add", "contact exists with address " + it.address_get() );
			return;
		}
		check_contact = contact_get_person_by_address( it.address_get() );
		if (null == check_contact) {
			other = new Contact( Contact.TYPE_PERSON,
				Contact.KEYSTAT_NONE, it.address_get(), it.address_get(), it.time_lastact_get() );
			other.id_set( zContacts.size() );
			zContacts.add( other );
		}
	}
	it.id_set( zContacts.size() );
	zContacts.add( it );
	write();
	Dib2Config.log( "contact_add", "done: " + zContacts.size() + it.address_get() );
}

public void contact_update( Contact it ) {
	it.name_set( it.name_get().replace( '\t', ' ' ).replace( '\n', ' ' ) );
	it.address_set( it.address_get().replace( " ", "" ) );
	Dib2Config.log( "contact_update", "update contact with id " + it.id_get() );
	Contact other = contact_get_person_by_address( it.address_get() );
	if (it.type_get() == Contact.TYPE_PERSON) {
		if ((other != null) && (it.id_get() != other.id_get())) {
			// possible race condition
			Dib2Config.log( "contact_update", "contact exists with address " + it.address_get() );
			zContacts.set( it.id_get(), null );
			it.id_set( other.id_get() );
		}
	} else if (null == other) {
		other = new Contact( Contact.TYPE_PERSON,
			Contact.KEYSTAT_NONE, it.address_get(), it.address_get(), it.time_lastact_get() );
		other.id_set( zContacts.size() );
		zContacts.add( other );
	}
	zContacts.set( it.id_get(), it );
	write();
}

public void contact_remove( Contact contact ) {
	if (1 >= contact.id_get()) {
		Dib2Config.log( "contact remove", "Can't remove myself or initial value." );
		return;
	}
	zContacts.set( contact.id_get(), null );
}

public Contact contact_get_by_name( String name ) {
	for (Contact ct : zContacts) {
		if ((ct != null) && name.equals( ct.name_get() )) {
			return ct;
		}
	}
	return null;
}

public Contact contact_get_person_by_address( String address ) {
	address = address.replace( " ", "" ).toLowerCase();
	for (Contact ct : zContacts) {
		if ((ct != null) && (ct.type_get() == Contact.TYPE_PERSON) && address.equals( ct.address_get().toLowerCase() )) {
			return ct;
		}
	}
	return null;
}

public List< Contact > contact_get_by_address( String address ) {
	address = address.replace( " ", "" ).toLowerCase();
	List< Contact > out = new ArrayList< Contact >();
	for (Contact ct : zContacts) {
		if ((ct != null) && address.equals( ct.address_get().toLowerCase() )) {
			out.add( ct );
		}
	}
	return out;
}

public List< Contact > contact_get_by_type( int type ) {
	List< Contact > out = new ArrayList< Contact >();
	for (Contact ct : zContacts) {
		if ((ct != null) && (type == ct.type_get())) {
			out.add( ct );
		}
	}
	return out;
}

public Contact contact_get_group_by_address_and_group( String address, int group ) {
	address = address.replace( " ", "" ).toLowerCase();
	for (Contact ct : zContacts) {
		if ((ct != null) && (Contact.TYPE_GROUP == ct.type_get()) && (group == ct.group_get())
			&& address.equals( ct.address_get().toLowerCase() )) {
			return ct;
		}
	}
	return null;
}

public Contact contact_get_by_id( int id ) {
	return ((id < 0) || (id >= zContacts.size())) ? null : zContacts.get( id );
}

public int message_get_unread_by_chat( int id, long since ) {
	int count = 0;
	for (message msg : zMessages.values()) {
		if ((id == msg.id_get()) && (1 < msg.from_get()) && (since < msg.time_get())) {
			++ count;
		}
	}
	return count;
}

public List< message > message_get_by_id( int id, int max ) {
	TreeMap< Long, message > map = new TreeMap< Long, message >();
	for (message msg : zMessages.values()) {
		if (id == msg.id_get()) {
			long ord = msg.time_get();
			while (map.containsKey( ord )) {
				++ ord;
			}
			map.put( ord, msg );
		}
	}
	ArrayList< message > out = new ArrayList< message >();
	for (Long tim : map.descendingKeySet()) {
		if (out.size() > max) {
			break;
		}
		out.add( 0, map.get( tim ) );
	}
	return out;
}

public synchronized void message_remove_all_by_contact( int id ) {
	for (boolean go = true; go;) {
		go = false;
		for (Long msgId : zMessages.keySet()) {
			if (id == zMessages.get( msgId ).id_get()) {
				zMessages.remove( msgId );
				go = true;
				break;
			}
		}
	}
	zStray = (0 == id) ? false : zStray;
}

public List< Contact > contact_get_all() {
	ArrayList< Contact > out = new ArrayList< Contact >();
	int first = zStray ? 0 : 1;
	if (zByNames) {
		TreeMap< String, Contact > map = new TreeMap< String, Contact >();
		for (Contact ct : zContacts) {
			if ((ct != null) && (first <= ct.id_get())) {
				String shash = QStr.shash( false, ct.name_get() )[ 0 ];
				if (null != map.get( shash )) {
					for (int i0 = 1; true; ++ i0) {
						if (null == map.get( shash + i0 )) {
							shash = shash + i0;
							break;
						}
					}
				}
				map.put( shash, ct );
			}
		}
		for (String shash : map.keySet()) {
			out.add( map.get( shash ) );
		}
	} else {
//		qCtx.log( "contact_get_all", "..." + zContacts.size() );
		TreeMap< Long, Contact > map = new TreeMap< Long, Contact >();
		for (Contact ct : zContacts) {
			if ((ct != null) && (first <= ct.id_get())) {
				long ord = ct.time_lastact_get();
				while (map.containsKey( ord )) {
					++ ord;
				}
				map.put( ord, ct );
			}
		}
		for (Long la : map.descendingKeySet()) {
			out.add( map.get( la ) );
		}
	}
	if (0 >= out.size()) {
		out.add( zContacts.get( 0 ) );
	}
//	qCtx.log( "contact_get_all", "" ); // + out );
	return out;
}

public List< Contact > contact_get_sendable() {
	TreeMap< String, Contact > map = new TreeMap< String, Contact >();
	for (Contact ct : zContacts) {
		if ((ct != null) && ((ct.type_get() == Contact.TYPE_GROUP)) || ((Contact.KEYSTAT_BIT_ACTIVE & ct.keystat_get()) != 0)) {
			if ((ct.address_get().indexOf( '@' ) > 0) && (0 < ct.id_get())) {
				map.put( ct.name_get(), ct );
			}
		}
	}
	ArrayList< Contact > out = new ArrayList< Contact >();
	for (String nam : map.keySet()) {
		out.add( map.get( nam ) );
	}
	return out;
}

public synchronized void message_add( message message ) {
	String txt = message.text_get();
	zStray = zStray || (0 == message.id_get());
	if (0 < txt.length()) {
		if (0 <= txt.indexOf( '\t' ) || ('\n' == txt.charAt( 0 )) || ('\n' == txt.charAt( txt.length() - 1 ))) {
			txt = txt.replace( '\t', '\n' ).replace( "\r\n", "\n" ).replace( '\r', '\n' );
			while (txt.startsWith( "\n" )) {
				txt = txt.substring( 1 );
			}
			while (txt.endsWith( "\n" )) {
				txt = txt.substring( 0, txt.length() - 1 );
			}
			message.text_set( txt );
		}
	}
	long mId = message.time_get() + message.id_get();
	while (zMessages.containsKey( mId )) {
		mId += 256;
	}
	zMessages.put( mId, message );
}

public synchronized void message_update( message message ) {
	if (0 <= message.text_get().indexOf( '\t' )) {
		message.text_set( message.text_get().replace( '\t', '\n' ) );
	}
	long mId = ((message.time_get() | 0xff) ^ 0xff) + message.id_get();
	zStray = zStray || (0 == message.id_get());
	while (zMessages.containsKey( mId )) {
		message msg = zMessages.get( mId );
		if ((message.id_get() == msg.id_get()) && (message.time_get() == msg.time_get())) {
			zMessages.put( mId, message );
		}
		mId += 256;
	}
	Dib2Config.log( "message_update", "failed " ); // + message.toString() );
}

public synchronized void preference_remove( String key ) {
	Dib2Config.log( "preference_remove", key );
	zPrefs.remove( key );
}

public synchronized void preference_set( String key, byte[] value ) {
	key = key.replace( '\t', ' ' ).replace( '\n', ' ' );
	value = (value == null) ? null : value.clone();
	//qCtx.log( "preference_add", key ); // + " " + UtilMisc.toHexString( value ) );
	zPrefs.put( key, value );
	write();
}

public byte[] preference_get( String key ) {
	key = key.replace( '\t', ' ' ).replace( '\n', ' ' );
	byte[] out = zPrefs.get( key );
	return (out == null) ? null : out.clone();
}

public byte[] exportLines() { // int offset, int[] outLen ) {
//	int[] pos = new int[] { offset };
	QMapping_1[] map = new QMapping_1[ zContacts.size() + zMessages.size() + zPrefs.size() + 1000 ];
	int cMap = 0;
//	byte[] out = new byte[ zContacts.size() * 16 + zMessages.size() * 32 + zPrefs.size() * 16 + 1000 + offset ];
	String contrib = zContacts.get( 0 ).name_get();
//	String contribTime = contrib + '\t' + UtilMisc.toDate4Millis();
//	byte[] line = UtilMisc.toBytes( contrib + "\tSRC\t" + contribTime );
//	out = UtilMisc.appendResize( out, pos, line );
	map[ cMap ++ ] = new QMapping_1( null, contrib, "SRC", -1, contrib );
	HashMap< String, String > oids = new HashMap< String, String >();
	for (String key : zPrefs.keySet()) {
		map = (cMap >= map.length) ? Arrays.copyOf( map, 2 * map.length ) : map;
		byte[] val = zPrefs.get( key );
		if (null == val) {
			continue;
		}
		map[ cMap ++ ] = new QMapping_1( null, key, "PREF", -1, contrib, UtilMisc.toHexString( val ) );
//		line = UtilMisc.toBytes( key + "\tPREF\t" + contribTime + '\t' + UtilMisc.toHexString( zPrefs.get( key ) ) );
//		out = UtilMisc.appendResize( out, pos, line );
	}
	for (Contact ct : zContacts) {
		if ((ct == null) || (ct.id_get() <= 0)) {
			continue;
		}
		String mems = ct.members_get_string().replace( ", ", "," );
		map = (cMap >= map.length) ? Arrays.copyOf( map, 2 * map.length ) : map;
		map[ cMap ++ ] = new QMapping_1( null, //- 
			ct.name_get(), // UtilMisc.toBytes( ct.name_get(), //-
			((Contact.TYPE_GROUP == ct.type_get()) ? "GROUP" : "CONTACT"), //- 
			-1, //-
			contrib, //-
			":EMAIL: " + ct.address_get(), //-
			":ADMIN: " + ct.group_get() + ',' + ct.keystat_get() + ',' + ct.time_lastact_get() / 1000 + ',' + ct.unread_get() / 1000, //-
			"" + ((ct.members_get() == null) ? (":PHONE: " + ct.phone_get()) : (":OTHER: " + mems)), //-
			":NOTES: " + ct.notes_get() //-
		);
		oids.put( map[ cMap - 1 ].label, map[ cMap - 1 ].oid );
//		out = UtilMisc.appendResize( out, pos, line );
	}
	for (message msg : zMessages.values()) {
		Contact ct = zContacts.get( msg.id_get() );
		if (null == ct) {
			ct = zContacts.get( 0 );
		}
		String dat = msg.text_get(); // .replace( '\n', '\t' );
		if (null != msg.uri_get() && !msg.text_get().startsWith( "::" )) {
			dat = "::" + msg.uri_get_string().replace( ":", "::" ) //,
				+ '\n' + dat;
		}
		if (msg.queue_get() != null) {
			dat = "::QUEUE::" + msg.queue_get() + '\n' + dat;
		}
		Contact cctrb = zContacts.get( msg.from_get() );
		cctrb = (null == cctrb) ? zContacts.get( 0 ) : cctrb;
		String ctrb = cctrb.name_get();
		ctrb = oids.containsKey( ctrb ) ? oids.get( ctrb ) : ctrb;
		map = (cMap >= map.length) ? Arrays.copyOf( map, 2 * map.length ) : map;
		map[ cMap ++ ] = new QMapping_1( null, //- 
//		line = UtilMisc.toBytes( ct.name_get() //-
			ct.name_get(), "MSG", msg.time_get(), ctrb, //-
			dat );
//		out = UtilMisc.appendResize( out, pos, line );
	}
//	outLen[ 0 ] = pos[ 0 ] - offset;
	return QMapping_1.toCsvMap( map, cMap );
}

public synchronized int importLines( byte[] dat, boolean replace, int version ) {
	int flags = (replace ? 0 : 2) | ((3 >= version) ? 1 : 0);
	QMapping_1[] map = QMapping_1.fromCsvMap( dat, flags );
	int count = 0;
	HashMap< String, String > oids = new HashMap< String, String >();
	HashSet< Long > allowDouble = new HashSet< Long >();
	for (int inx = 0; inx < map.length; ++ inx) {
		try {
			String name = map[ inx ].label;
			oids.put( map[ inx ].oid, name );
			String cats = "";
			for (String cat : map[ inx ].categoryOids) {
				cats += " " + cat;
			}
			cats = cats.trim();
			String contrib = map[ inx ].contributorOid;
			contrib = (oids.containsKey( contrib )) ? oids.get( contrib ) : contrib;
			long timestamp = map[ inx ].qTimeStamp;
//			if (3 >= version) {
//				timestamp = timestamp ^ (timestamp & Dib2Constants.TIME_SHIFTED);
//			}
			String[] data = map[ inx ].listElements;
//			qCtx.log( "load", "" + count + " " + name + " " + cats ); // + " " + data );
			if (cats.contains( "PREF" )) {
				// Do not override current entries when importing older data:
				if (replace || !zPrefs.containsKey( name ) || (null == zPrefs.get( name )) || (0 >= zPrefs.get( name ).length)) {
					zPrefs.put( name, UtilMisc.fromHexString( data[ 0 ] ) );
					++ count;
				}
			} else if (cats.contains( "GROUP" ) || cats.contains( "CONTACT" )) {
				boolean group = cats.contains( "GROUP" );
//				a0 = data.split( "\t", 4 );
				String[] a0 = map[ inx ].listElements;
				String email = a0[ 0 ].substring( a0[ 0 ].indexOf( ": " ) + 2 ).trim();
				String adm = a0[ 1 ].substring( a0[ 1 ].indexOf( ": " ) + 2 ).trim();
				String mems = ((2 >= a0.length) || !group) ? null //- 
					: a0[ 2 ].substring( a0[ 2 ].indexOf( ": " ) + 2 ).trim();
				String phone = ((2 >= a0.length) || group) ? null //- 
					: a0[ 2 ].substring( a0[ 2 ].indexOf( ": " ) + 2 ).trim();
				String notes = (3 >= a0.length) ? null //- 
					: a0[ 3 ].substring( a0[ 3 ].indexOf( ": " ) + 2 ).trim();
				a0 = adm.split( "," );
				Contact ct = new Contact( //-
					(group ? Contact.TYPE_GROUP : Contact.TYPE_PERSON), //-
					Integer.parseInt( a0[ 1 ] ), email, name, Long.parseLong( a0[ 2 ] ) * 1000 );
				ct.group_set( Integer.parseInt( a0[ 0 ] ) );
				ct.unread_set( Long.parseLong( a0[ 3 ] ) * 1000 );
				if (null != mems) {
					ct.members_set( Arrays.asList( mems.split( "," ) ) );
				}
				if (null != phone) {
					ct.phone_set( phone );
				}
				if (null != notes) {
					ct.notes_set( notes );
				}
				List< Contact > act = contact_get_by_address( email );
				// Do not override current entries when importing older data:
				if (group || (0 >= act.size())) {
					ct.id_set( zContacts.size() );
					zContacts.add( ct );
					++ count;
				} else if (replace && (1 <= act.size())) {
					Contact ct2 = act.get( 0 );
					if (0 >= ct2.name_get().length()) {
						ct2.name_set( ct.name_get() );
					}
					if (0 >= ct2.phone_get().length()) {
						ct2.phone_set( ct.phone_get() );
					}
					if (0 >= ct2.notes_get().length()) {
						ct2.notes_set( ct.notes_get() );
					}
					++ count;
				}
			} else {
				// Messages: assuming contacts are listed ahead of the messages.
				Contact ct = contact_get_by_name( name );
				if (null == ct) {
					ct = zContacts.get( 0 );
				}
				int id = ct.id_get();
				ct = contact_get_by_name( contrib );
				if (null == ct) {
					ct = zContacts.get( 0 );
				}
				int from = ct.id_get();
				String txt = data[ 0 ];
				for (int ix = 1; ix < data.length; ++ ix) {
					txt += "\n" + data[ ix ];
				}
				message msg = new message( id, from, timestamp, txt );
//				int part2 = txt.indexOf( '\n' );
//				part2 = (0 < part2) ? part2 : txt.length();
				if (txt.startsWith( "::QUEUE::" )) {
					msg.queue_set( data[ 0 ].substring( txt.lastIndexOf( "::" ) + 2 ) ); // txt.substring( txt.lastIndexOf( "::" ) + 2, part2 ) );
				} else if (txt.startsWith( "::" )) {
//					String uri = data.substring( 0, part2 );
					msg.uri_set( data[ 0 ] ); // uri );
				}
				long mId = msg.time_get() + msg.id_get();
				if (!zMessages.containsKey( mId ) || allowDouble.contains( mId )) {
					allowDouble.add( mId );
					message_add( msg );
					++ count;
				} else if (replace || (null == zMessages.get( mId ))) {
					message_update( msg );
					++ count;
				}
			}
		} catch (Exception e) {
			// Ignore.
		}
	}
	Dib2Config.log( "load", "" + count );
	return count;
}
//=====
}
