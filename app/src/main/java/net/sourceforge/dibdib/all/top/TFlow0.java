// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

/**
 * Token flow model: Petri net (Arc weight = 1) with typed Places
 * (Places impose guards on their preceding Transitions):
 * - Process (synchronized split): incoming Places with single Arcs + single Transition.
 * - Dispatcher (merged choice): single (unguarded) incoming Place + identity Transitions.
 * - Store (merged source): single guarded Place + copy Transitions with returning Arc.
 * - Plus Terminators (source, sink) for workflow: ThreadUser with external transition.
 */
public final class TFlow0 {
//=====

// Token-Oriented-Programming (TOP):
//
// IDEA
//
// Petri Arcs are implemented as simple pipes with a buffering end-point and a referencing start-point.
// Petri Places and Transitions are 'normalized' by specific element groups: Transposers and Stores.
// Transposers = active elements: Process, Dispatcher/ trigger, Terminator.
// Transposers have buffered ports for their incoming tokens (= arguments),
// plus virtual ports for their connections to Stores (= parameters).
// Outgoing ports (= results) are implemented as a simple reference to the peer's in-port buffer.
// ==> No queue or extra container for Places: avoids internal race conditions.
// ==> Token values as arguments, Store values as parameters for Transposers.
//
// MODEL
//
// PortBuffer 'x': buffering endpoint of Petri Arc for Petri Place 'O':
// - Process fires if all in-ports/ places have data.
// - Dispatcher collects data (unguarded) and passes it on according to Transition's guards.
// - Store is implemented as container with synchronized methods
//   ('put' for ports, 'pick' for Transitions).
//
//  Process;               Dispatcher:            Store ('synchronized', feeding data tokens)
//   ---x O -\             ---x\      /-|--->          <----.
//            \|--->            \    /            --x  v   /|--->
//   ---x O ---|           ----x O <              ---x O -<
//            /|--->            /    \            --x  ^   \|--->
//   ---x O -/             ---x/      \-|--->          <----'
//
// Error handling: asst() for assertions logs ERROR line.
// ==> Use special sink for errors and status messages, use error token for flushing pipes.
// ==> OR: Token may get swallowed, Terminator checks with popLog().
//
// IMPLEMENTATION
//
// Transposers and Stores are implemented within Contexts, which are connected by Gateways:
// Sources and Sinks as Context gates are the end-points for the Gateways.
// Tokens do not carry the 'full load' of data from the model above, because the
// data gets stored in stateful containers, i.e. a container implements the behavior of a Store.
// Stores become stateful (e.g. backed by files), Transposers are stateless!
// Tokens and the Container entries have a timestamp, i.e. containers keep latest versions.
// Tokens are used for presentation data (e.g. filtered data), Stores can also be used as
// optional read-only gates of the Context (e.g. for dumping all data).
// Consistency of the data is possible using the timestamps: only if all tokens with a certain
// timestamp have entered a sink and no token with a lower timestamp is left,
// then the Context data with that timestamp is considered done.
// If necessary, a rollback to that state of the Context data is possible.
;

public static TFlow0 instance;
public static StringBuffer log = new StringBuffer();
public final TfTransposerIf[] transposers;

public TFlow0( TfTransposerIf... transposers ) {
	assert instance == null;
	instance = this;
	this.transposers = transposers;
}

public static String popLog() {
	String out = log.toString();
	log = new StringBuffer();
	return out;
}

// Assertion:
// crash == true ==> on
// crash == false ==> log, remove token
// method commented ==> off
@SuppressWarnings( "rawtypes" )
public static boolean asst( boolean assertion, String error, Class... affected ) {
	//TODO
	return assertion;
}

public int step() {
	return -1;
}

//=====
}
