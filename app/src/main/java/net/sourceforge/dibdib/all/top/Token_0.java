// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

import net.sourceforge.dibdib.all.map.QMapping_1;

public class Token_0 extends QMapping_1 {
//=====

/** Currently, only 2 levels are used: high (>0), medium (<=0). */
public byte qPriority;

///// Read-only for other classes (quick access)
///// -- Immutable - 'as if'!

//Base class itself does not contain data.
public Token_0( String xmAtom, long xmTimeStamp ) {
	super( null, xmAtom, null, xmTimeStamp, null );
}

public Token_0( String xmAtom, long xmTimeStamp, String... xmValues ) {
	super( null, xmAtom, null, xmTimeStamp, null, xmValues );
}

//public Token_0( String xmAtom, long xmTimeStamp, Object[] xmMappings ) {
//	super( xmAtom, null, xmTimeStamp, xmMappings );
//}

public void setTimeStamp( long xmTimeStamp ) {
	qTimeStamp = xmTimeStamp;
}

public void setPriority( byte xmPriority ) {
	qPriority = xmPriority;
}

//=====
}
