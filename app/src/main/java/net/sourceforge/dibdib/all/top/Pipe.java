// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

/**
 * Simple pipe (String based connection between two processes): data is immediately processed
 * via caller's thread, which may be different from implementation's main thread.
 */
public interface Pipe {
//=====

/**
 * Take data from sender and process it using caller's thread.
 */
public void pipe( Pipe sender, String attrib, long[] timeStampEtc, String... vals );

//=====
}
