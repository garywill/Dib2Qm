// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.util;

import java.security.MessageDigest;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import net.sourceforge.dibdib.config.*;

/** 'Quick' string utils: text segmentation and sorting.
 * QString = sortable hash ('shash') + original string as required.
 */
/*
String handling, definitions:
'QString' as tokenized char string with shashes (sortable hashes):
qstring, packed: QX shash [QX shash]* QX QQ [ QX qelement [QX qelement]* QX]
string: token [ [separator/ indicator]+ token ]*
element: token/ separator/ indicator
qelement: token/ ([marker]* QM [spacer/label]* QM [separator/ indicator]+) 
(every spacer sequence becomes simple QX among shashes in qstring (for sorting),
indicator alone does not affect sorting)
shash: Q(normalize(token))
token: word (string of digits, ...) / float (date, ...)
sememe: predicate/ operator/ restrictor/ selector/ descriptor
expression: '(' operator [' ' term]* ')'
predication: '{' predicate [' ' term]+ '}'
substitution: '{' atom '}'
term: atom/ float/ group/ selector
group: descriptor [ '(' restrictor ')' ]
atom: word/ '<' word [' ' word]* '>'/ group '.' selector
word: [symbol/ alpha/ digit]* symbol/alpha/num [symbol/ alpha/ digit]*
restrictor/ descriptor/ ...: word
float: [d]+/ '0x' [d]+/ '0.' [d]+ ['e' ['+'/ '-'] d] ['~` d] ['_' [word '*' '/']+]/ ...
marker: (see below)

Float: normalized:
start with '0.' followed by '1'-'9' and more digits, plus exponent plus precision info plus unit
e.g.: 0.123e3~2_kg.m/s/s = (with last 2 digits plus binary rounding not reliable:) [123 - 64, 123 + 64] kg*m/sec^2.
Date (saved as (+/-)YYYYMMDD.fract_T: fractional part: h/24+min/24/60+sec/24/60/60):
(YY)YY-MM-DD(T.)hh:mm:ss.sTZD
DD.MM.YY, MM-DD/YY
Range (eg event):
a..b incl. b, a~~b excl. b

Shash (sortable hash):
with collation key as bitlist for words ('a' is sorted like 'A' etc.: normalization form NFKC, strength PRIMARY)
plus special float encoding for numbers,
use Unicode private area (0xE100..0xF7FF):
0000 0000 000.....  control chars
0000 0000 00000001  - QQ
0000 0000 00000010  - QX
0000 0000 00000011  - QM
1110 0001 0.......  format left
1110 0001 1.......  format right
1110 0010 0.......  struc basic
1110 0010 1.......  struc X
1110 0011 ........  negative float with extended range (RFU)
1110 01.. ........  negative float (>= double precision), pos. exponent: IEEE bits toggled
1110 10.. ........  negative float >= -2 (bits toggled (+1) for sorting!)
1110 1100           NaN, sorted ahead of 0
1110 1100 00000000  0
1110 11.. ........  positive float with neg. or 0 exponent: IEEE bits
1111 00.. ........  positive float with pos. exponent
1111 0100 ........  positive float with extended range (RFU)
1111 0101 ........  indicator for (condensed) collation key (depends on Java implementation)
1111 0101 0.......  - 7-bit encoded chars from 16 collation bits (first byte == 0x00)
1111 0101 10000101  - bytes from collation bits (first byte < 0x80, excluding 0x00)
1111 0101 10000110  - 16-bit uints (excl. 0) from 16 collation bits (first byte == 0x80)
1111 0101 10000111  - bytes from collation bits (first byte > 0x80, excluding 0x00)
1111 011. ........  9 significant bits of original char (e.g. control chars)  
0x0 as filler, subsequent words have to be >= 0x20 !
(used chars must not overlap with control chars!)
0....... 1........  for subsequent 7-bit pieces
1....... .........  for subsequent 15-bit pieces
........ ......... (excluding 0x00) for subsequent byte pieces

float with at least 2 bytes, value pieces encoded as IEEE floats with special toggling:
111. ..             (encoded bits for sign bit with 1 bit of exponent)
       .. ........  (remaining 10 bits of exponent for double precision, possibly toggled)
+ 1...............* (15 bits of value (significand), in several chunks as needed)
+ 0.......1.......  (optional: 7 bits precision info + unit info as follows)
+ 0.......1.......* (7 bit ASCII pieces from unit name or '.' for factor or '/')
e.g.:
E    type   exp  exp   val  val
1111 00 00 0000 0000 1 100 0000 0000 0000 = 1.1(2) * 2^1 = 11(2) = 3
1111 00 00 0000 0000 1 000 0000 0000 0000 = 1.0(2) * 2^1 = 2
1110 11 11 1111 1111 1 100 0000 0000 0000 = 1.1(2) * 2^0 = 1.1(2) = 1.5
1110 11 11 1111 1111 1 000 0000 0000 0001 = 1.000000000000001(2) * 2^0
1110 11 11 1111 1111 1 000 0000 0000 0000 1000 ... 0001 ~ 1.000...2
1110 11 11 1111 1111 1 000 0000 0000 0000 = 1.0(2) * 2^0 = 1
1110 11 00 0000 0001 1 000 0000 0000 0000 = 1.0(2) * 2^-1022
1110 11 00 0000 0000 1 000 0000 0000 0000 = 0
1110 10 00 0000 0001 1 000 0000 0000 0000 = -1
1110 10 00 0000 0000 1 000 0000 0000 0000 = -2
 */

/*
Cmp. icu-project.org:
-- Primary weights can be 1 to 4 bytes in length. (If a character has a 3-byte
CE primary weight, we'll call it a 3-byter, for example)
-- They have the restriction that no weight can be a proper initial sequence
of another. Eg, if X has weight 85 42, then no three or four byte weight
can start with 85 42.
-- It is important that the most frequent characters have short weights.
So, for example, in U5.2 we have {space, A-Z} being 1-byters.
-- The first bytes are important, and are allocated to special ranges.
[Java uses double bytes/ ...: Special case: 0x80..., trailing 0x00! for concatenation] 
 */

public final class QStr {
//=====

private static final String REGEX_SPACE_NL = "[\\s\\p{Z}]";
public static final Matcher MATCHER_SPACE_NL = Pattern.compile( REGEX_SPACE_NL ).matcher( "" );
public static final String REGEX_LINE_BREAK = "\\r?[\\n\\u0085\\u2028\\u2029]"; // "\\r?[\\n\\v\\u0085\\u2028\\u2029]";
public static final Matcher MATCHER_LINE_BREAK_TAB = Pattern.compile( "\\r?[\\n\u0085\u2028\u2029\\t]" ).matcher( "" ); // "\\r?[\\n\\v\\u0085\\u2028\\u2029\\t]"
public static final Matcher MATCHER_WORD_CONNECTOR = Pattern
	.compile( "[\\p{L}\\p{M}\\p{N}\\p{Pc}[\\p{InEnclosedAlphanumerics}&&\\p{So}]]+" )
	.matcher( "" );
public static final Matcher MATCHER_WORD_BASIC = Pattern.compile( "[\\p{L}\\p{M}\\p{N}]+" ).matcher( "" );
public static final Matcher MATCHER_WORD_SYMBOL = Pattern.compile( "[\\p{L}\\p{M}\\p{N}\\p{S}]+" ).matcher( "" );
public static final Matcher MATCHER_SYMBOLS = Pattern.compile( "\\p{S}+" ).matcher( "" );
//public static final Matcher MATCHER_LETTERS_CASED = Pattern.compile( "\\p{L&}+" ).matcher( "" );
public static final Matcher MATCHER_PUNCTUATION = Pattern.compile( "\\p{P}+" ).matcher( "" );
public static final Matcher MATCHER_CONTROLS_UNI = Pattern.compile( "\\p{Cc}+" ).matcher( "" );
public static final Matcher MATCHER_CONTROLS_ANSI = Pattern.compile( "\\p{Cntrl}+" ).matcher( "" );
public static final Matcher MATCHER_DIGITS = Pattern.compile( "\\p{Nd}+" ).matcher( "" );
public static final Matcher MATCHER_DIGITS_BASIC = Pattern.compile( "\\p{Digit}+" ).matcher( "" );
public static final Matcher MATCHER_HEXS = Pattern.compile( "\\p{XDigit}+" ).matcher( "" );
public static final Matcher MATCHER_NUMERICS = Pattern.compile( "\\p{N}+" ).matcher( "" );

private static final String REGEX_NUMBER = "([\\+\\-]?[1-9][0-9_\u00B7\\'\\.\\,]*[0-9])|([\\+\\-]?[0-9])";
private static final String REGEX_NUMBER_SEP = "[_\u00B7\\']";
private static final Matcher MATCHER_NUMBER_SEP = Pattern.compile( REGEX_NUMBER_SEP ).matcher( "" );
private static final String REGEX_UNIT_SUFFIX = "(_[\\./A-z0-9\\p{Sc}]+)?";
private static final String REGEX_DIGITS_TEL_ETC = "[\\+\\#0][0-9\\-\\*]+\\#?";
private static final Matcher MATCHER_DIGITS_TEL_ETC = Pattern.compile( REGEX_DIGITS_TEL_ETC ).matcher( "" );
private static final String REGEX_NUMBER_UNIT = REGEX_NUMBER + REGEX_UNIT_SUFFIX;
private static final Matcher MATCHER_NUMBER_UNIT = Pattern.compile( REGEX_NUMBER_UNIT ).matcher( "" );
private static final String REGEX_VALUE = "[\\+\\-]?[0-9_\u00B7\\'\\.\\,]*[0-9][eE][\\+\\-]?[0-9]+[\\~0-9]*" + REGEX_UNIT_SUFFIX;
private static final Matcher MATCHER_VALUE = Pattern.compile( REGEX_VALUE ).matcher( "" );
private static final String REGEX_VALUE_HEX = "[\\+\\-]?0[xX][0-9A-Fa-f_\u00B7\\'\\.\\,]*[0-9A-Fa-f]([pP][\\+\\-]?[0-9]+[\\~0-9]*)?"
	+ REGEX_UNIT_SUFFIX;
private static final Matcher MATCHER_VALUE_HEX = Pattern.compile( REGEX_VALUE_HEX ).matcher( "" );
private static final String REGEX_DATE = "\\-?[0-9]+\\-[0-9][0-9]\\-[0-9][0-9]T?[\\.0-9\\:\\+\\-]*";
private static final Matcher MATCHER_DATE = Pattern.compile( REGEX_DATE ).matcher( "" );
private static final Matcher MATCHER_DATE_D = Pattern.compile( "[0-9][0-9]\\.[0-9][0-9]\\.[12]?[0-9]?[0-9][0-9]" ).matcher( "" );
private static final String CHARS_NUM_FIRST = "+-0123456789#";
private static final String REGEX_WORD = "[\\w\\p{S}\\p{N}]*[\\p{L}\\p{S}][\\w\\p{S}\\p{N}]*";
private static final Matcher MATCHER_WORD = Pattern.compile( REGEX_WORD ).matcher( "" );
private static final String CHARS_STRUC = "()[]{}<>";
//private static final String CHARS_STRUC_LINE = "./=;|-*:";
private static final String CHARS_STRUC_MOD = "$?%#";
private static final String CHARS_FORMAT = "*_+^~#";
private static final String CHARS_MARKER = CHARS_FORMAT + CHARS_STRUC + CHARS_STRUC_MOD;

public static final char SHASH_QQ = (char) 1;
public static final char SHASH_QX = (char) 2;
public static final char SHASH_QM = (char) 3;
public static final char SHASH_FORMAT_LEFT = (char) 0xE100;
public static final char SHASH_FORMAT_RIGHT = (char) 0xE180;
public static final char SHASH_STRUC = (char) 0xE200;
public static final char SHASH_NEG = (char) 0xE400;
public static final char SHASH_POS = (char) 0xEC00;
public static final String SHASH_NAN = "" + (char) 0xEC00;

private static Collator coll = null;
private static char[] collMap = new char[ 0xff ];
private static int[] collDelta = new int[ 0x100 ];

/** Calculate sortable hash (shash) string for value.
 * @param repr String representation, to be parsed (or null).
 * @param value Value or additional (fractional) value.
 * @return shash.
 */
public static String double2Shash( String repr, double value ) {
	String shash = null;
	try {
		double val = (null == repr) ? value : (Double.parseDouble( repr ) + value);
		long bits = Double.doubleToRawLongBits( val );
		if (0 > val) {
			bits = -bits;
		}
		int exp = (int) ((bits & 0x7ff0000000000000L) >>> 52);
		long significand = ((bits & 0x000fffffffffffffL) << (4 * 15 - 52));
		char[] bits15 = new char[ 5 ];
		bits15[ 0 ] = (char) (((0 > val) ? SHASH_NEG : SHASH_POS) + exp);
		bits15[ 4 ] = (char) (0x8000 + (significand & 0x7fff));
		significand >>>= 15;
		bits15[ 3 ] = (char) (0x8000 + (significand & 0x7fff));
		significand >>>= 15;
		bits15[ 2 ] = (char) (0x8000 + (significand & 0x7fff));
		significand >>>= 15;
		bits15[ 1 ] = (char) (0x8000 + (significand & 0x7fff));
		shash = new String( bits15 );
		int count = 4;
		for (; count >= 2; -- count) {
			if (bits15[ count ] != 0x8000) {
				break;
			}
		}
		shash = shash.substring( 0, count + 1 );
	} catch (Exception e) {
	}
	return shash;
}

public static double shashNum2Double( String shash ) {
	if (0 >= shash.length()) {
		return 0;
	}
	if (SHASH_NEG > shash.charAt( 0 )) {
		return Double.NaN;
	}
	// Simplified:
	long bits = shash.charAt( 0 );
	boolean pos = (SHASH_POS <= shash.charAt( 0 ));
	bits -= pos ? SHASH_POS : SHASH_NEG;
	bits <<= 52;
	switch (shash.length()) {
		case 5:
			bits |= (shash.charAt( 4 ) & 0x7fffL) >>> 8;  // Fall through.
		case 4:
			bits |= (shash.charAt( 3 ) & 0x7fffL) << 7;
		case 3:
			bits |= (shash.charAt( 2 ) & 0x7fffL) << 22;
		case 2:
			bits |= (shash.charAt( 1 ) & 0x7fffL) << 37;
			break;
		default:
			return Double.NaN;
	}
	return Double.longBitsToDouble( pos ? bits : -bits );
}

public static String shash2String( String shash ) {
	int len = shash.length();
	if (0 >= len) {
		return "";
	} else if (0xf500 > shash.charAt( 0 )) {
		//TODO: Handle extra long floats.
		double val = shashNum2Double( shash );
		return (val == (long) val) ? ("" + (long) val) : ("" + val);
	} else if (0xf580 > shash.charAt( 0 )) {
		char last = shash.charAt( len - 1 );
		len = len * 2 - 1 - ((0 == (last & 0x7f)) ? 1 : 0);
		len = (0 >= len) ? 1 : len;
		char[] out = new char[ len ];
		out[ len - 1 ] = (char) (last & 0x7f);
		boolean high = false;
		for (int i0 = shash.length() - 2; i0 >= 0; -- i0) {
			out[ i0 * 2 + 1 ] = (char) ((shash.charAt( i0 + 1 ) >>> 8) & 0x7f);
			if (out[ i0 * 2 + 1 ] <= 3) {
				high = true;
			}
			out[ i0 * 2 ] = (char) (shash.charAt( i0 ) & 0x7f);
			if (out[ i0 * 2 ] <= 3) {
				high = true;
			}
		}
		if (!high) {
			for (int i0 = out.length - 1; i0 >= 0; -- i0) {
				out[ i0 ] = collMap[ out[ i0 ] ];
			}
			return new String( out );
		}
		len = 0;
		for (int i0 = 0; i0 < (out.length - 1); ++ i0) {
			if (3 >= out[ i0 ]) {
				out[ len ++ ] = (char) (0x2000 | (out[ i0 ] << 8) | out[ i0 + 1 ]);
				++ i0;
			} else {
				out[ len ++ ] = collMap[ out[ i0 ] ];
			}
		}
		return new String( Arrays.copyOf( out, len ) );
	} else if (0xf586 == shash.charAt( 0 )) {
		char[] out = new char[ shash.length() - 1 ];
		for (int i0 = shash.length() - 1; i0 > 0; -- i0) {
			char val = shash.charAt( i0 );
			out[ i0 - 1 ] = (val < 0x80) ? collMap[ val ] : (char) (val + collDelta[ val >>> 8 ]);
		}
		return new String( out );
	} else if (0xf600 <= shash.charAt( 0 )) {
		return "" + (char) (shash.charAt( 0 ) & 0x1ff) + shash.substring( 1 );
	}
	char last = shash.charAt( len - 1 );
	len = (len - 1) * 2 - ((0 == (last & 0x7f)) ? 1 : 0);
	len = (0 >= len) ? 1 : len;
	char[] out = new char[ len ];
	out[ len - 1 ] = collMap[ last & 0xff ];
	for (int i0 = shash.length() - 2; i0 > 0; -- i0) {
		out[ i0 * 2 ] = collMap[ (shash.charAt( i0 + 1 ) >>> 8) & 0xff ];
		out[ i0 * 2 - 1 ] = collMap[ shash.charAt( i0 ) & 0xff ];
	}
	out[ 0 ] = collMap[ (shash.charAt( 1 ) >>> 8) & 0xff ];
	return new String( out );
}

/** 
 * Tokenize text into string elements plus formatting and structuring elements, based on AsciiDoc.
 * @param xText plain text to be split
 * @return triples of data elements plus quoted text parts, normalized */
/*
formatLeft: ..' *'/ ..' _'/ ...:  *bold, _italic, +mono, ^sup, ~sub, #[attrib]#
(optional '.' after ' ' prevents using ' ' itself as separator)
formatRight: ..'* '/ ... / ..'# ' (succeeding ' ' or '\n' !)
special char: '#': after ' ' or '\n' as combiner (see below)
formatX: ' .'/ '\n<<<\n'=FF, '\n+\n' continuation with line break
struc: '\n\n' (explicit) overall end, [^ \n]'.'[^ \n] mid-num or selector,
'\n//' comment, \n==+ header, \n.[^ \n] or \n; title
'\n-* ' set, \n*+ list, \n.+ array, (these with optional label marker:) '::',
'\n| ' table row,
'[', ']', '<', ..., '{', '}', '${', '#{', ...
usage of '#' after ' ' and '\n' (#. erases preceding ' ' or '\n'):
#{ struc, #[ format/attrib, #[[ anchor/id, #( RFU, #< RFU,
'#'[0-9*#+-] marker for string of digits (e.g. telephone number),
#$ marker for float, #' marker for other string,
 */
public static String[] toElements( String xText ) {
	if ((null == xText) || (0 >= xText.length())) {
		return new String[] { "" };
	}
	String text = xText;
	if (!Normalizer.isNormalized( text, Normalizer.Form.NFKC )) {
		text = Normalizer.normalize( text, Normalizer.Form.NFKC );
	}
	// Replace control char's to make room for own usage.
	if ((0 <= text.indexOf( '\u0000' )) || (0 <= text.indexOf( '\u0001' )) || (0 <= text.indexOf( '\u0002' ))
		|| (0 <= text.indexOf( '\u0003' ))) {
		char[] tx = text.toCharArray();
		for (int i0 = tx.length - 1; 0 <= i0; -- i0) {
			if (3 >= tx[ i0 ]) {
				tx[ i0 ] += 0xf600;
			}
		}
		text = new String( tx );
	}
	ArrayList< String > out = new ArrayList< String >( text.length() / 4 );
	MATCHER_LINE_BREAK_TAB.reset( text );
	String left = "";
	int suppressBlanks = 1;
	// Split by line breaks.
	for (int lineOffs = 0, lineEnd, lineNext; lineOffs < text.length() //- 
	; lineOffs = lineNext, left = text.substring( lineEnd, lineNext )) {
		lineEnd = MATCHER_LINE_BREAK_TAB.find() ? MATCHER_LINE_BREAK_TAB.start() : text.length();
		lineNext = (lineEnd < text.length()) ? MATCHER_LINE_BREAK_TAB.end() : lineEnd;
		String line = text.substring( lineOffs, lineEnd ).trim();
		int pieceOffs = 0;
		if (line.isEmpty()) {
			// End of block.
			suppressBlanks = 0;
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') );
			out.add( "\n" );
			out.add( left );
			continue;
		} else if ("<<<".equals( line )) {
			// Page break.
			suppressBlanks = 0;
			out.add( "" );
			out.add( "\f" );
			out.add( left + line );
			continue;
		} else if ("+".equals( line )) {
			// Continuation.
			suppressBlanks = 1;
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') );
			out.add( "" );
			out.add( left + line );
			continue;
		} else if (line.startsWith( "// " )) {
			// Comment.
			suppressBlanks = 1;
			out.add( "" );
			out.add( "" );
			out.add( left + line );
			continue;
		} else if ((('.' == line.charAt( 0 )) || (';' == line.charAt( 0 ))) && (2 <= line.length()) && (' ' < line.charAt( 1 ))) {
			// Title.
			suppressBlanks = 1;
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') + (char) (SHASH_STRUC + ';') );
			out.add( "\n" );
			out.add( left + line.charAt( 0 ) );
			left = "";
			pieceOffs = 1;
		} else if ((0 <= "=-*.|;".indexOf( line.charAt( 0 ) )) && (1 <= line.indexOf( ' ' ))) {
			// Header/ list/ table.
			out.add( "" + (char) (SHASH_FORMAT_RIGHT + '\n') );
			out.add( "\n" );
			out.add( left );
			left = "";
			for (++ pieceOffs; pieceOffs < line.length(); ++ pieceOffs) {
				if (line.charAt( 0 ) != line.charAt( pieceOffs )) {
					break;
				}
			}
			if (' ' != line.charAt( pieceOffs )) {
				pieceOffs = 0;
			} else {
				suppressBlanks = 1;
				int iLabel = pieceOffs + line.indexOf( ':', pieceOffs );
				String label = "";
				if ((iLabel < (lineNext - 1)) && (':' == line.charAt( iLabel + 1 ))) {
					label = line.substring( pieceOffs + 1, iLabel ).trim();
					label += "::";
					pieceOffs = iLabel + 2;
				}
				++ pieceOffs;
				out.add( "" + (char) (SHASH_STRUC + line.charAt( 0 )) );
				out.add( label );
				out.add( line.substring( 0, pieceOffs ) );
			}
		}
		MATCHER_SPACE_NL.reset( line );
		// Split by words.
		for (int pieceNext; pieceOffs < line.length(); pieceOffs = pieceNext) {
			int pieceEnd = MATCHER_SPACE_NL.find() ? MATCHER_SPACE_NL.start() : line.length();
			pieceNext = (pieceEnd < line.length()) ? MATCHER_SPACE_NL.end() : pieceEnd;
			// Handle previous blank char's.
			if (pieceOffs >= pieceEnd) {
				char space = line.charAt( pieceOffs );
				int count = 0;
				while ((pieceOffs < pieceNext) && (space == line.charAt( pieceOffs + count ))) {
					++ count;
				}
				if (0 < count) {
					char[] blanks = new char[ count + ((0 < suppressBlanks) ? 0 : 1) ];
					Arrays.fill( blanks, ' ' );
					out.add( "" );
					out.add( new String( blanks ) );
					out.add( left + line.substring( pieceOffs, pieceOffs + count ) );
					left = "";
					suppressBlanks = 1;
				}
				pieceNext = pieceOffs + count;
				continue;
			}
			String piece = line.substring( pieceOffs, pieceEnd );
			int elOffs = 0;
			boolean forceFloat = false;
			boolean forceString = false;
			String format = "";
			String struc = "";
			String show = "";
			if (('#' == piece.charAt( 0 )) && ((1 >= pieceEnd) || (0 > CHARS_STRUC.indexOf( piece.charAt( 1 ) )))) {
				// Dangling '#'.
				if (piece.startsWith( "#." )) {
					suppressBlanks = 2;
					elOffs = 2;
				}
				forceString = true;
				struc = "" + (char) (SHASH_STRUC | '\'');
				if ((elOffs + 1) < pieceEnd) {
					++ elOffs;
					if ('$' == piece.charAt( elOffs )) {
						++ elOffs;
						forceString = false;
						forceFloat = true;
						struc = "" + (char) (SHASH_STRUC | '$');
					} else {
						elOffs += ('\'' == piece.charAt( elOffs )) ? 1 : 0;
					}
				}
				left += piece.substring( 0, elOffs );
			} else if ((0 <= (CHARS_MARKER + '.').indexOf( piece.charAt( 0 ) ))) {
				// Find all markers.
				for (; elOffs < piece.length(); ++ elOffs) {
					if ('.' == piece.charAt( elOffs )) {
						suppressBlanks = 2;
					} else if ('#' == piece.charAt( elOffs )) {
						if (piece.substring( elOffs ).startsWith( "#[" ) && line.contains( "]#" )) {
							// Attribute/ anchor.
							int to = pieceOffs + line.indexOf( "]#" ) + 2;
							out.add( "" + (char) (SHASH_FORMAT_LEFT | '#') );
							out.add( "" );
							out.add( line.substring( elOffs + 2, to - 2 ) );
							piece = "";
							elOffs = to;
						} else {
							// Structural.
							show += piece.charAt( elOffs );
							struc += (char) (SHASH_STRUC | piece.charAt( elOffs )); // (piece.substring( elOffs ).startsWith( "#{" ) ? '#' : '^'));
						}
					} else if ((0 <= CHARS_FORMAT.indexOf( piece.charAt( elOffs ) ))) {
						format += ("" + (char) (SHASH_FORMAT_LEFT | piece.charAt( elOffs )));
					} else if (0 <= CHARS_MARKER.indexOf( piece.charAt( elOffs ) )) {
						show += piece.charAt( elOffs );
						struc += (char) (SHASH_STRUC | piece.charAt( elOffs ));
					} else {
						break;
					}
				}
				if ((elOffs >= piece.length()) || (' ' >= piece.charAt( elOffs ))) {
					elOffs = 0;
				} else {
					left += piece.substring( 0, elOffs );
				}
			}
			// Eject matching pieces.
			String blanks = (0 >= suppressBlanks) ? " " : "";
			if ((0 < format.length() || (0 < blanks.length()))) {
				out.add( "" + format );
				out.add( blanks );
				out.add( left );
				left = "";
			}
			if (0 < struc.length()) {
				out.add( struc );
				out.add( show );
				out.add( left );
				left = "";
			}
			if (0 < left.length()) {
				out.add( "" );
				out.add( "" );
				out.add( left );
				left = "";
			}
			suppressBlanks -= (0 < suppressBlanks) ? 1 : 0;
			// Find end of text pieces.
			int iMark = piece.length() - 1;
			if (!forceFloat && !forceString) {
				for (; iMark > elOffs; -- iMark) {
					if (0 > (CHARS_FORMAT + CHARS_STRUC).indexOf( piece.charAt( iMark ) )) {
						break;
					}
				}
			}
			++ iMark;
			// Get text pieces.
			for (int next; elOffs < iMark; elOffs = next) {
				next = elOffs;
				String part = piece.substring( elOffs, iMark );
				if (!forceString
					&& (forceFloat || ((0 <= CHARS_NUM_FIRST.indexOf( piece.charAt( elOffs ) )) && !piece.substring( elOffs ).startsWith(
						"#{" )))) {
					if (MATCHER_VALUE_HEX.reset( part ).find() && (0 == MATCHER_VALUE_HEX.start())) {
						next += MATCHER_VALUE_HEX.end();
					} else if (MATCHER_DIGITS_TEL_ETC.reset( part ).find() && (0 == MATCHER_DIGITS_TEL_ETC.start())) {
						next += MATCHER_DIGITS_TEL_ETC.end();
					} else if (MATCHER_DATE.reset( part ).find() && (0 == MATCHER_DATE.start())) {
						next += MATCHER_DATE.end();
					} else if (MATCHER_VALUE.reset( part ).find() && (0 == MATCHER_VALUE.start())) {
						next += MATCHER_VALUE.end();
					} else if (MATCHER_NUMBER_UNIT.reset( part ).find() && (0 == MATCHER_NUMBER_UNIT.start())) {
						next += MATCHER_NUMBER_UNIT.end();
					} else {
						for (++ next; next < iMark; ++ next) {
							if (0 > CHARS_NUM_FIRST.indexOf( piece.charAt( next ) )) {
								break;
							}
						}
					}
					out.add( part.substring( 0, next ) );
					out.add( "" );
					out.add( "" );
				} else {
					int skip = (!forceString && MATCHER_WORD.reset( part ).find()) ? MATCHER_WORD.start() : -1;
					if (0 < skip) {
						out.add( part.substring( 0, skip ) );
					}
					next = (0 <= skip) ? MATCHER_WORD.end() : part.length();
					skip = (0 > skip) ? 0 : skip;
					out.add( part.substring( skip, next ) );
					out.add( "" );
					out.add( "" );
					next += elOffs;
				}
			}
			if (elOffs < iMark) {
				out.add( piece.substring( elOffs, iMark ) );
				out.add( "" );
				out.add( "" );
			}
			format = struc = show = "";
			if (iMark < piece.length()) {
				String lx = piece.substring( iMark );
				for (elOffs = iMark; elOffs < piece.length(); ++ elOffs) {
					if ((0 <= CHARS_FORMAT.indexOf( piece.charAt( elOffs ) ))) {
						format += ("" + (char) (SHASH_FORMAT_RIGHT | piece.charAt( elOffs )));
					} else {
						show += piece.charAt( elOffs );
						struc += (char) (SHASH_STRUC | (piece.charAt( elOffs ) & 0x7f));
					}
				}
				if (0 < format.length()) {
					out.add( "" + format );
					out.add( "" );
					out.add( lx );
					lx = "";
				}
				if (0 < struc.length()) {
					out.add( struc );
					out.add( show );
					out.add( lx );
				}
			}
			left = line.substring( pieceEnd, pieceNext );
		}
//		left = text.substring( lineEnd, lineNext );
	}
	if (0 < left.length()) {
		out.add( "" );
		out.add( "\n" );
		out.add( left );
	}
	return out.toArray( new String[ 0 ] );
}

public static String[] sortStd( String[] strings ) {
	CollationKey[] keys = new CollationKey[ strings.length ];
	for (int i0 = 0; i0 < strings.length; ++ i0) {
		keys[ i0 ] = coll.getCollationKey( strings[ i0 ] );
	}
	Arrays.sort( keys );
	String[] out = new String[ strings.length ];
	for (int i0 = 0; i0 < out.length; ++ i0) {
		out[ i0 ] = keys[ i0 ].getSourceString();
	}
	return out;
}

/** Calculate sortable hash (shash) values.
 * @param xSha1 for making it unique by appending SHA-1 value in case of long string
 * @param xaElements text or float or marker etc.
 * @return per element: up to (SHASH_MAX-2) chars or truncated (SHASH_MAX-1) chars or (SHASH_MAX-11) chars + X + SHA1
 */
public static String[] shash( boolean xSha1, String... xaElements ) {

	if (null == coll) {
		try {
			// Multilingual, using ICU's uca_rules.txt:
			RuleBasedCollator ucaDucet = new RuleBasedCollator( "& \\u0001 = \\u0002" );
			coll = ucaDucet;
		} catch (ParseException e) {
			coll = null;
		}
		if ((null == coll) || (null != Dib2Config.locale)) {
			coll = Collator.getInstance( (null != Dib2Config.locale) ? Dib2Config.locale : Locale.CANADA );
			// Case-independent:
			coll.setStrength( Collator.PRIMARY );
		}
		///// Override higher by lower values, map to lowercase.
		for (char c0 = (char) 0x1e; c0 != 0x1f; c0 -= (c0 != 1) ? 1 : -0xfe) {
			String s0 = "" + c0;
			byte[] tmp = coll.getCollationKey( "" + c0 ).toByteArray();
			if ((2 <= tmp.length) && ((0 == tmp[ 0 ]) || (0 == tmp[ 1 ]))) {
				collMap[ tmp[ (0 == tmp[ 0 ]) ? 1 : 0 ] & 0xff ] = s0.toLowerCase().charAt( 0 );
			}
		}
		for (int i0 = collMap.length - 1; i0 >= 0; -- i0) {
			collMap[ i0 ] = (0 >= collMap[ i0 ]) ? '^' : collMap[ i0 ];
		}
		for (int c0 = 0x100; c0 <= 0xff00; c0 += 0x100) {
			byte[] tmp = coll.getCollationKey( "" + (char) c0 ).toByteArray();
			collDelta[ c0 >>> 8 ] = -1;
			for (int i0 = 0; i0 < tmp.length; ++ i0) {
				if (tmp[ i0 ] == (byte) (c0 >>> 8)) {
					// tmp should have trailing '00' bytes.
					collDelta[ c0 >>> 8 ] = (c0 & 0xff) - (tmp[ i0 + 1 ] & 0xff);
					break;
				}
			}
		}
	}
	String[] out = new String[ xaElements.length ];
	int inx = 0;
	boolean forceString = false;
	for (String piece : xaElements) {
		if ((null == piece) || (0 >= piece.length())) {
			out[ inx ++ ] = piece;
			continue;
		} else if (0 <= piece.indexOf( SHASH_QM )) {
			forceString = (0 <= piece.indexOf( (char) (SHASH_STRUC | '\'') ));
			out[ inx ++ ] = (piece.contains( "" + SHASH_QM + SHASH_QM )) ? "" : ("" + SHASH_QX);
			continue;
		} else {
			forceString = false;
		}
		boolean num0 = !forceString && (0 <= CHARS_NUM_FIRST.indexOf( piece.charAt( 0 ) ));
		double frac = 0;
		if (num0 && MATCHER_VALUE_HEX.reset( piece ).matches()) {
			if ((0 > piece.indexOf( 'p' )) && (0 > piece.indexOf( 'P' ))) {
				piece += "p0";
			}
		} else if (num0 && MATCHER_DIGITS_TEL_ETC.reset( piece ).matches()) {
			num0 = false;
		} else if (num0 && (MATCHER_DATE.reset( piece ).matches()) || MATCHER_DATE_D.reset( piece ).matches()) {
			if (MATCHER_DATE_D.reset( piece ).matches()) {
				piece = piece.substring( 6 ) + '-' + piece.substring( 3, 5 ) + '-' + piece.substring( 0, 2 );
			}
			if (2 == piece.indexOf( '-' )) {
				piece = UtilMisc.prependCentury( piece );
			}
			if ((0 <= piece.indexOf( 'T' )) || (0 <= piece.indexOf( '.' ))) {
				int iHH = piece.indexOf( 'T' );
				iHH = (0 <= iHH) ? iHH : piece.indexOf( '.' );
				String hhmm = piece.substring( iHH + 1 );
				piece = piece.substring( 0, iHH );
				if (2 <= hhmm.length()) {
					frac = UtilMisc.toFraction4HHMM( hhmm );
				}
			}
			piece = piece.replace( "-", "" ) + "_T";
		} else if (num0 && MATCHER_VALUE.reset( piece ).matches()) {
		} else if (num0 && MATCHER_NUMBER_UNIT.reset( piece ).matches()) {
		} else if ( // floatError || 
		piece.matches( REGEX_WORD )) {
			num0 = false;
		}
		String shash = "";
		if (num0) {
			// Units:
			String units = null;
			int iUnit = piece.lastIndexOf( '_' );
			if ((iUnit < (piece.length() - 1)) && ('9' < piece.charAt( iUnit + 1 ))) {
				units = piece.substring( iUnit + 1 );
				piece = piece.substring( 0, iUnit );
			}
			// German decimal point etc.:
			if (0 <= piece.indexOf( ',' )) {
				if (piece.indexOf( ',' ) != piece.lastIndexOf( ',' )) {
					piece = piece.replace( ",", "" );
				} else {
					piece = piece.replace( ".", "" ).replace( ",", "." );
				}
			}
			piece = MATCHER_NUMBER_SEP.reset( piece ).replaceAll( "" );
			//TODO: Extra long floats.
			String shashTmp = double2Shash( piece, frac );
			shash = (null == shashTmp) ? shash : shashTmp;
			if (null == shashTmp) {
//				floatError = true;
				num0 = false;
				units = null;
			}
			if ((null != units) && (0 < units.length())) {
				//TODO: add precision info:
				char last = 0x80;
				for (char unit : units.toCharArray()) {
					if (last == 0) {
						last = (char) ((unit & 0x7f) << 8);
					} else {
						shash += (char) (last | 0x80 | (unit & 0x7f));
						last = 0;
					}
				}
				if (0 != last) {
					shash += last;
				}
			}
		}
		if (!num0) {
			boolean cut = (Dib2Constants.SHASH_MAX * 2) < piece.length();
			String px = cut ? piece.substring( 0, Dib2Constants.SHASH_MAX * 2 ) : piece;
			byte[] shashBytes = coll.getCollationKey( px ).toByteArray();
			int len = shashBytes.length;
			while ((0 < len) && (0 == shashBytes[ len - 1 ])) {
				-- len;
			}
			char[] shashChars = new char[ len * 2 + 3 ];
			int len2 = 1;
			///// Compress:
			if (1 >= len) {
				len2 = 0;
			} else if ((0x00 == shashBytes[ 0 ]) && (0 < shashBytes[ 1 ])) {
				byte[] pieces = new byte[ len * 2 + 3 ];
				shashChars[ 0 ] = (char) (0xf500 | shashBytes[ 1 ]);
				int i1 = 0;
				for (int i0 = 3; i0 < len; i0 += 2) {
					char val = (char) (((shashBytes[ i0 - 1 ] & 0xff) << 8) | (shashBytes[ i0 ] & 0xff));
					if (0 != val) {
						if (0x8000 <= val) {
							// Special Java marker?
							continue;
						} else if (0x7f <= val) {
							// Escaped?
							val += collDelta[ val >>> 8 ];
							pieces[ i1 ++ ] = (byte) ((val >>> 8) & 3);
						}
						pieces[ i1 ++ ] = (byte) (val & 0x7f);
					}
				}
				for (int i0 = 0; i0 < i1; i0 += 2) {
					int v0 = pieces[ i0 ] & 0x7f;
					int v1 = pieces[ i0 + 1 ] & 0x7f;
					shashChars[ len2 ++ ] = (char) (0x80 | (v0 << 8) | v1);
				}
			} else if ((shashBytes[ 0 ] == (byte) 0x80) && (0 == shashBytes[ 1 ])) {
				///// Special Java marker?
				shashChars[ 0 ] = 0xf586;
				for (int i0 = 1; i0 < len; i0 += 2) {
					int v0 = (((shashBytes[ i0 - 1 ] & 0xff) << 8) | (shashBytes[ i0 ] & 0xff));
					if ((0x8000 == v0) || (0 == v0)) {
						continue;
					}
					shashChars[ len2 ++ ] = (char) v0;
				}
			} else {
				shashChars[ 0 ] = (char) ((0x80 <= shashBytes[ 0 ]) ? 0xf587 : 0xf585);
				for (int i0 = 1; i0 < len; i0 += 2) {
					int v0 = (((shashBytes[ i0 - 1 ] & 0xff) << 8) | (shashBytes[ i0 ] & 0xff));
					if (0 == v0) {
						continue;
					}
					shashChars[ len2 ++ ] = (char) v0;
				}
			}
			shash = new String( Arrays.copyOf( shashChars, len2 ) );
			if (cut || (Dib2Constants.SHASH_MAX <= len2)) {
				if (xSha1) {
					shashChars = Arrays.copyOf( shashChars, Dib2Constants.SHASH_MAX );
					byte[] sha1;
					try {
						sha1 = MessageDigest.getInstance( "SHA-1" ).digest( piece.getBytes( "UTF-8" ) );
					} catch (Exception e) {
						sha1 = new byte[ 20 ];
					}
					shashChars[ Dib2Constants.SHASH_MAX - 11 ] = 0xffff;
					for (int i0 = 18; i0 >= 0; i0 -= 2) {
						shashChars[ Dib2Constants.SHASH_MAX - 10 + (i0 / 2) ] //-
						= (char) (((sha1[ i0 ] & 0xff) << 8) + (sha1[ i0 + 1 ] & 0xff));
					}
				} else {
					shashChars = Arrays.copyOf( shashChars, Dib2Constants.SHASH_MAX - 1 );
				}
				shash = new String( shashChars );
			}
		}
		out[ inx ++ ] = shash;
	}
	return out;
}
//=====
}
