// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

/**
 * Active TF component for Petri net Transition with preceding Places (= combinations of in-ports).
 */
public interface TfTransposerIf extends ThreadUser {
//=====

/**
 * Buffering end points of Petri Arc.
 */
public TfPortBuffer[] getInPorts();

/**
 * Simple reference to each peer's in-port, plus optional extra port for default/ fallback connection.
 */
public TfPortBuffer[] getOutPorts();

//	public String[] getInPortNames();
public String[] getOutPortNames();

public int connectTo( TfTransposerIf... listTo );

/**
 * @return minimum value from timeStamps (>= 0) if ready (otherwise < 0):
 */
public long ready();

/**
 * @return estimated number of remaining steps:
 */
public int go();

//=====
}
