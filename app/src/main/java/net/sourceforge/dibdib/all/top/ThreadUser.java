// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

public interface ThreadUser {
//=====

/**
 * @return 0 if yielding, < 0 if error:
 */
public int step();

//=====
}
