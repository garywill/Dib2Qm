// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.join;

import java.io.File;

public interface ContextIf {
//=====

void init( char platform, Object... parameters );

File getFilesDir( String... parameters );

void log( String... aMsg );

void toast( String msg );

String get( String pref, String defaultValue );

void set( String pref, String value );

//public void pref2db();
//
//public void db2pref();

//=====
}
