// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;

import net.sourceforge.dibdib.config.Dib2Config;

/**
 * Port buffer = Transposer's incoming port: endpoint of Petri Arc for simple pipe connection.
 */
public final class TfPortBuffer {
//=====

// Copy of config value => enable optimization.
private static final boolean INIT_linkedList = false;

protected TfLinkedToken kHead = null;
protected TfLinkedToken kTail = null;
protected Object[] buffer = new Object[ 4 ];
protected int iHead = 0;

///// As read-only access for other classes.
public int count = -1;
public Object head = null;

public final String name;

public class TfLinkedToken {
//===
public final TfLinkedToken prev;
public final Object token;

public TfLinkedToken( TfLinkedToken prev, Object token ) {
	this.prev = prev;
	this.token = token;
}
//===
};

/**
 * (Transposer's incoming port)
 * 
 * @param name Gets interned for fast comparison!
 */
public TfPortBuffer( String name ) {
	assert INIT_linkedList == Dib2Config.useLinkedList;
	this.name = (name == null) ? "" : name.intern();
}

public void init() {
	count = 0;
}

public void put( Object token ) {
	if (INIT_linkedList) {
		kTail = new TfLinkedToken( kTail, token );
		if (null == kHead) {
			kHead = kTail;
		}
		head = kHead.token;
	} else {
		int split = count;
//		if (0 < token.qPriority) {
//			for (split = iHead + count - 1; split > iHead; -- split) {
//				if (0 < buffer[ split ].qPriority) {
//					break;
//				}
//			}
//		}
		if (((iHead + count) >= buffer.length) || (split < count)) {
			Object[] p2 = buffer;
			if (count > (buffer.length / 2)) {
				p2 = new Object[ 2 * buffer.length ];
			}
			System.arraycopy( buffer, iHead, p2, 0, split );
			if (split < count) {
				System.arraycopy( buffer, iHead + split, p2, split + 1, count - split );
			}
			buffer = p2;
			iHead = 0;
		}
		buffer[ iHead + split ] = token;
		head = buffer[ iHead ];
	}
	++ count;
}

public Object take() {
	head = null;
	if (INIT_linkedList) {
		-- count;
		TfLinkedToken h0 = kHead;
		if (kTail == kHead) {
			kHead = null;
			kTail = null;
			if (null == h0) {
				return null;
			}
		} else {
			for (kHead = kTail; kHead.prev != h0; kHead = kHead.prev) {
				{
				} // NOP
			}
		}
		head = kHead.token;
		return h0.token;
	} else if (count <= 0) {
		return null;
	}
	Object out = buffer[ iHead ];
	-- count;
	if (count <= 0) {
		iHead = 0;
		head = null;
	} else {
		++ iHead;
		head = buffer[ iHead ];
	}
	return out;
}

public static int indexOf( TfPortBuffer[] list, String element ) {
	for (int inx = list.length - 1; inx >= 0; -- inx) {
		if (element == list[ inx ].name) {
			return inx;
		}
	}
	return -1;
}

//=====
}
