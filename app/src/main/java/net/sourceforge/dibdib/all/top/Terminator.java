// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.top;


/**
 * ThreadUser that bridges a Pipe implementation with an
 * external transition for Places of Petri net (sources/ sinks):
 */
public interface Terminator extends ThreadUser, Pipe {
//=====

public Object[] getPorts( boolean outgoing );

//=====
}
