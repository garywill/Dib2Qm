// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.util;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;
import java.util.zip.*;
import net.sourceforge.dibdib.all.join.CsvCodecIf;
import net.sourceforge.dibdib.config.Dib2Constants;

public final class UtilMisc {
//=====

public static int processMaxTimer = 1000;
public static int processMicroSteps4Timer = 30000;

public static final long TIME_2017_01_01 = 1483228800000L;

/** Keep sort order and hex onset, skip char 'O' for potential confusion with '0'. */
public static final char[] base64XChars = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ^_abcdefghijklmnopqrstuvwxyz~".toCharArray();

private static long minTimeVal = TIME_2017_01_01;
private static long minTimeLast = TIME_2017_01_01;
protected static int idStamp = (((2017 - 2000) & 0xf0) << 14) | (((2017 - 2000) & 0xf) << 12);
protected static long idCount = 1;
public static String qLastId;

/*
=====
Note:
W3 recommendation for ISO date format (www.w3.org/TR/NOTE-datetime):
Complete date:
  YYYY-MM-DD (eg 1997-07-16)
Complete date plus hours and minutes:
  YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
Complete date plus hours, minutes and seconds:
  YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
Complete date plus hours, minutes, seconds and a decimal fraction of a
second
  YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)

where:
 YYYY = four-digit year
 MM   = two-digit month (01=January, etc.)
 DD   = two-digit day of month (01 through 31)
 hh   = two digits of hour (00 through 23) (am/pm NOT allowed)
 mm   = two digits of minute (00 through 59)
 ss   = two digits of second (00 through 59)
 s    = one or more digits representing a decimal fraction of a second
 TZD  = time zone designator (Z or +hh:mm or -hh:mm)
=====
*/

// SimpleDateFormat is not thread-safe! 'X' works only for Java 7
private static final String DATE_FORMAT_ISO_Z = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
// Not needed here:
//	private static final String DATE_FORMAT_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
//	private static final String DATE_FORMAT_SHORT = "yyMMdd'.'HHmmss";
//	private static final String DATE_FORMAT_SHORT_TZ = "yyMMdd'.'HHmmss'.'SSSZ";

// Do not waste time by constantly accessing TZ data:
private static final SimpleDateFormat DATE_SDF = new SimpleDateFormat( DATE_FORMAT_ISO_Z );
private static int timeZoneOffset = 0;

/** Might need a fresh start later on, e.g. for daylight saving. */
public static boolean timeZoneDone = false;

public final static char[] HEX = "0123456789ABCDEF".toCharArray();
public final static byte[] NIBBLE = { 9, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, //-
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

public final static StringBuffer logBuffer = new StringBuffer( 22000 );

//public static void checkPlatform() {
// Check:
//	BigInteger P;
//	P = new BigInteger( 64, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check > 16: " + P );
//	P = new BigInteger( 64, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check > 16: " + P );
//	P = new BigInteger( 8, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check < 16: " + P );
//	P = new BigInteger( 8, 1, new Random( 42 ) );
//	Dib2Config.log( "receiver", "random check < 16: " + P );
//}

public static void checkTimeZone() {
	if (timeZoneDone) {
		// Leave the time zone for the current run, rather than messing up the internal time line.
		return;
	}
	timeZoneDone = true;
	timeZoneOffset = TimeZone.getDefault().getOffset( System.currentTimeMillis() ); // DATE_SDF.getTimeZone().getRawOffset();
	DATE_SDF.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
}

public static void keepLog( String tag, String msg ) {
	int size = 0;
	while (42000 < logBuffer.length()) {
		size = logBuffer.indexOf( "\n", 200 );
		size = (0 >= size) ? 100 : size;
		logBuffer.delete( 0, size );
	}
	logBuffer.append( tag ).append( "@" + toShortDate4Millis() + ": " ).append( msg ).append( "\n" );
}

public static void initLastId( String id ) {
	String current = createId( "" );
	if (current.compareTo( id ) <= 0) {
		long count = id.charAt( 4 ) & 0x3;
		for (int i0 = 5; i0 < (id.length() - 5); ++ i0) {
			int c0 = id.charAt( i0 ) - '0';
			c0 = (c0 >= 10) ? (c0 + '0' - 'A') : c0;
			count = (count << 6) + c0;
		}
		idCount = (count >>> 2) + 4;
		current = createId( "" );
		if (current.compareTo( id ) <= 0) {
			// Fallback.
			count = currentTimeMillisLinearized() / 1000;
			idCount = count % (12 * 60 * 60);
		}
	}
}

public static long currentTimeMillisLinearized() {
	long out = System.currentTimeMillis();
	if (out >= minTimeVal) {
		minTimeVal = out;
		out = out ^ (out & Dib2Constants.TIME_SHIFTED);
	} else {
		if (out > (minTimeLast + Dib2Constants.TIME_SHIFTED)) {
			++ minTimeVal;
			minTimeLast = out;
		}
		out = minTimeVal ^ (minTimeVal & Dib2Constants.TIME_SHIFTED);
		if (out >= (minTimeVal - 3605 * 1000)) {
			out = out | Dib2Constants.TIME_SHIFTED_HOUR;
		} else {
			out = out | Dib2Constants.TIME_SHIFTED_UNKNOWN;
		}
	}
	return out;
}

/** Set time value according to current time, or adjust overall timer.
 * @param value The original value.
 * @param minimum -1 for unshifted/ unmarked time value, >1 for potential change of overall timer.
 * @return
 */
public static long alignTime( long value, long minimum ) {
	final int hour = 3600 * 1000;
	final int minute = 60 * 1000;
	final int delta = 2 * minute;
	long current = currentTimeMillisLinearized();
	long val = (0 <= value) ? value : current;
	long ref = (0 <= minimum) ? minimum : current;
	if (ref > (current + Dib2Constants.TIME_SHIFTED)) {
		// Adjust overall timer.
		minTimeVal = ref;
		return ref | Dib2Constants.TIME_SHIFTED_UNKNOWN;
	} else if (val < (ref - delta)) {
		if (val >= (ref - 25 * hour)) {
			long out = val ^ (val & Dib2Constants.TIME_SHIFTED);
			while (out < (ref - delta)) {
				out += hour;
			}
			if (out <= current) {
				return out | Dib2Constants.TIME_SHIFTED_HOUR;
			}
		}
		return (ref ^ (ref & Dib2Constants.TIME_SHIFTED_HOUR)) | Dib2Constants.TIME_SHIFTED_UNKNOWN;
	} else if (val > (current + delta)) {
		if (val > (current + 25 * hour)) {
			return current | Dib2Constants.TIME_SHIFTED_UNKNOWN;
		}
		long out = val ^ (val & Dib2Constants.TIME_SHIFTED);
		while (out > (current + delta)) {
			out -= hour;
		}
		return out | Dib2Constants.TIME_SHIFTED_HOUR;
	}
	return (0 <= minimum) ? val : (val ^ (val & Dib2Constants.TIME_SHIFTED));
}

public static String toHexString( byte[] data ) {
	char[] out = new char[ 2 * data.length ];
	for (int i0 = data.length - 1; i0 >= 0; -- i0) {
		out[ i0 * 2 ] = HEX[ (data[ i0 ] & 0xf0) >>> 4 ];
		out[ i0 * 2 + 1 ] = HEX[ data[ i0 ] & 0xf ];
	}
	return new String( out );
}

public static String toHexString( char[] string ) {
	char[] out = new char[ 4 * string.length ];
	for (int i0 = string.length - 1; i0 >= 0; -- i0) {
		out[ i0 * 4 ] = HEX[ (string[ i0 ] & 0xf000) >>> 12 ];
		out[ i0 * 4 + 1 ] = HEX[ (string[ i0 ] & 0xf00) >>> 8 ];
		out[ i0 * 4 + 2 ] = HEX[ (string[ i0 ] & 0xf0) >>> 4 ];
		out[ i0 * 4 + 3 ] = HEX[ string[ i0 ] & 0xf ];
	}
	return new String( out );
}

public static byte[] fromHexString( String hex ) {
	char[] arr = hex.toCharArray();
	byte[] out = new byte[ (arr.length + 1) / 2 ];
	for (int i0 = out.length - 1; i0 >= 0; -- i0) {
		byte b0 = NIBBLE[ arr[ i0 * 2 ] & 0x1f ];
		byte b1 = NIBBLE[ arr[ i0 * 2 + 1 ] & 0x1f ];
		out[ i0 ] = (byte) ((b0 << 4) | b1);
	}
	return out;
}

/** @param msec UTC (optional, default: current time)
 * @return
 */
public static String toDate4Millis( long... msec ) {
	long v0 = ((null == msec) || (0 >= msec.length)) ? currentTimeMillisLinearized() : msec[ 0 ];
	if (!timeZoneDone) {
		checkTimeZone();
	}
	String date = DATE_SDF.format( new Date( v0 ) );
	return date.substring( 0, date.length() - 5 ).concat( "+0000" );
}

public static String toShortDate4Millis( long... msec ) {
	long v0 = ((null == msec) || (0 >= msec.length)) ? currentTimeMillisLinearized() : msec[ 0 ];
	if (!timeZoneDone) {
		checkTimeZone();
	}
	// Local time (without sdf.setTimeZone(TimeZone.getTimeZone()):
	String dx = DATE_SDF.format( new Date( v0 + timeZoneOffset ) );
	dx = dx.substring( 2, dx.length() - 9 ).replaceAll( "[^0-9T\\:]", "" ).replace( 'T', '.' );
	return dx;
}

public static String toShortDate4Millis( long msec, int len ) {
	if (len >= 0) {
		return toShortDate4Millis( msec ).substring( 0, len );
	}
	String out = toShortDate4Millis( msec );
	return out.substring( 0, out.length() + len );
}

public static String getShortDay() {
	return toShortDate4Millis( currentTimeMillisLinearized() ).substring( 0, 6 );
}

public static String prependCentury( String shortDate ) {
	String yyyy = UtilMisc.toDate4Millis().substring( 0, 4 );
	int pp = (yyyy.charAt( 0 ) & 0xf) * 10 + (yyyy.charAt( 1 ) & 0xf);
	if (yyyy.charAt( 2 ) > ((shortDate.charAt( 0 ) & 0xf) + 5)) {
		-- pp;
	} else if (yyyy.charAt( 2 ) < ((shortDate.charAt( 0 ) & 0xf) - 5)) {
		++ pp;
	}
	return "" + pp + shortDate;
}

public static long toMillis4Date( String date ) {
	int offs = 0;
	if (!timeZoneDone) {
		checkTimeZone();
	}
	// Short format?
	if ((date.length() <= 6) || date.matches( "[0-9][0-9][0-9][0-9][0-9][0-9]\\..*" )) {
		date = date.replace( ":", "" );
		// Use local time:
		int itz = date.indexOf( '+' );
		if (itz < 0) {
			itz = date.lastIndexOf( '-' );
		}
		if (itz < 0) {
			offs = timeZoneOffset;
			date = date.concat( "110101.120000.000+0000".substring( date.length() ) );
		} else {
			date = date.substring( 0, itz ).concat( "110101.120000.000".substring( itz ) ) //.
				.concat( date.substring( itz ) );
		}
		date = prependCentury( date.substring( 0, 2 ) ) // (date.charAt( 0 ) == '9') ? "19" : "20") + date.substring( 0, 2 )
			+ "-" + date.substring( 2, 4 ) + "-" + date.substring( 4, 6 ) //.
			+ "T" + date.substring( 7, 9 ) + ":" + date.substring( 9, 11 ) + ":" + date.substring( 11, 13 ) //.
			+ date.substring( 13 );
	}
	if (date.matches( "[0-9][0-9][^0-9].*" )) {
		date = prependCentury( date );
	}
	if (date.matches( "[0-9][0-9][0-9][0-9]\\-[0-9][0-9].*" )) {
		// TZ info?
		if (date.charAt( date.length() - 3 ) == ':') {
			char tzi = date.charAt( date.length() - 6 );
			if ((tzi == '+') || (tzi == '-')) {
				date = date.substring( 0, date.length() - 3 ) + date.substring( date.length() - 2 );
			}
		}
		try {
			final Date dx = DATE_SDF.parse( date );
			return dx.getTime() - offs;
		} catch (ParseException e) {
			{
			} // NOP
		}
	}
	date = date.replaceAll( "[^0-9]", "" );
	date = (4 >= date.length()) ? date : (date.substring( 0, 4 ) + '-' + date.substring( 4 ));
	date = (7 >= date.length()) ? date : (date.substring( 0, 7 ) + '-' + date.substring( 7 ));
	date = (10 >= date.length()) ? date : (date.substring( 0, 10 ) + 'T' + date.substring( 10 ));
	date = (13 >= date.length()) ? date : date.substring( 0, 13 );
	date = date.concat( "2000-01-01T12:00:00.000+0000".substring( date.length() ) );
	try {
		final Date dx = DATE_SDF.parse( date );
		return dx.getTime() - offs;
	} catch (ParseException e) {
		{
		} // NOP
	}
	return TIME_2017_01_01 | Dib2Constants.TIME_SHIFTED_UNKNOWN;
}

private static final Matcher MATCHER_DIGITS_BASIC = Pattern.compile( "\\p{Digit}+" ).matcher( "" );

public static double toFraction4HHMM( String xHhMm ) {
	String hhmm = xHhMm;
	double frac = (hhmm.charAt( 0 ) & 0xf) / 2.4;
	frac += (hhmm.charAt( 1 ) & 0xf) / 24.0;
	hhmm = hhmm.substring( (2 == hhmm.indexOf( ':' )) ? 3 : 2 );
	if (2 <= hhmm.length()) {
		frac += (hhmm.charAt( 0 ) & 0xf) / (24 * 6.0);
		frac += (hhmm.charAt( 1 ) & 0xf) / (24 * 60.0);
		hhmm = hhmm.substring( (2 == hhmm.indexOf( ':' )) ? 3 : 2 );
		if (2 <= hhmm.length()) {
			frac += (hhmm.charAt( 0 ) & 0xf) / (24 * 60 * 6.0);
			frac += (hhmm.charAt( 1 ) & 0xf) / (24 * 60 * 60.0);
			hhmm = hhmm.substring( (2 == hhmm.indexOf( '.' )) ? 3 : 2 );
			if (MATCHER_DIGITS_BASIC.reset( hhmm ).matches()) {
				frac += Double.parseDouble( "0." + hhmm ) / (24 * 60 * 60.0);
			}
		}
	}
	return frac;
}

/** Calculate limit or steps to limit.
 * 
 * @param count
 * @param previousLimit
 * @return
 */
public static long processTimer( int count, long previousLimit ) {
	if (previousLimit <= 0) {
		return currentTimeMillisLinearized() + processMaxTimer;
	}
	long now = currentTimeMillisLinearized() + 2;
	if (now >= previousLimit) {
		return Long.MIN_VALUE + 1;
	}
	return (processMaxTimer / 2 * count) / (processMaxTimer + (int) (now - previousLimit));
}

public static int binSearch( String[] sortedList, String key ) {
	int min = 0;
	int max = sortedList.length - 1;
	for (int i0 = (min + max) >> 1; min < max; -- max) {
		if (sortedList[ i0 ].compareTo( key ) <= 0) {
			min = i0;
		} else {
			max = i0;
		}
		i0 = (min + max) >> 1;
	}
	return min;
}

public static int indexOf( String[] list, String key ) {
	for (int i0 = 0; i0 < list.length; ++ i0) {
		if ((list[ i0 ] == key) || list[ i0 ].equals( key )) {
			return i0;
		}
	}
	return -1;
}

/** For sorted strings:
 * @param sorted0
* @param sorted1
* @return
 */
public static boolean containsAll( String[] sorted0, String... sorted1 ) {
	int i0 = sorted0.length - 1;
	int i1 = sorted1.length - 1;
	for (; (i0 >= 0) && (i1 >= 0); -- i0) {
		int cmp = (i0 > 0) ? 1 : 0;
		for (; cmp > 0; -- i0) {
			cmp = sorted1[ i1 ].charAt( 0 ) - sorted0[ i0 ].charAt( 0 );
		}
		cmp = (0 == cmp) ? sorted0[ i0 ].compareTo( sorted1[ i1 ] ) : cmp;
		if (0 > cmp) {
			return false;
		} else if (0 == cmp) {
			-- i1;
		}
	}
	return (i1 < 0);
}

/** For sorted strings:@param sorted0
* @param sorted1
* @return
 */
public static boolean containsOne( String[] sorted0, String... sorted1 ) {
	int i0 = sorted0.length - 1;
	int i1 = sorted1.length - 1;
	for (; (i0 >= 0) && (i1 >= 0); -- i0) {
		int cmp = (i0 > 0) ? 1 : 0;
		for (; cmp > 0; -- i0) {
			cmp = sorted1[ i1 ].charAt( 0 ) - sorted0[ i0 ].charAt( 0 );
		}
		cmp = (0 == cmp) ? sorted0[ i0 ].compareTo( sorted1[ i1 ] ) : cmp;
		if (0 > cmp) {
			++ i0;
			-- i1;
		} else if (0 == cmp) {
			return true;
		}
	}
	return false;
}

public static int indexOf( byte[] strUtf8, byte[] subStr, int offs ) {
	for (int i0 = offs; i0 < strUtf8.length; ++ i0) {
		if (strUtf8[ i0 ] == subStr[ 0 ]) {
			int i1 = 0;
			for (; i1 < subStr.length; ++ i1) {
				if (strUtf8[ i0 + i1 ] != subStr[ i1 ]) {
					break;
				}
			}
			if (i1 >= subStr.length) {
				return i0;
			}
		}
	}
	return -1;
}

public static int indexOf( byte[] strUtf8, byte[] subStr ) {
	return indexOf( strUtf8, subStr, 0 );
}

public static byte[] appendResize( byte[] xyArray, int[] xyPos, byte[] xToAppend ) {
	int newlen = xyArray.length;
	while ((xyPos[ 0 ] + xToAppend.length) >= newlen) {
		newlen *= 2;
	}
	if (newlen > xyArray.length) {
		xyArray = Arrays.copyOf( xyArray, newlen );
	}
	System.arraycopy( xToAppend, 0, xyArray, xyPos[ 0 ], xToAppend.length );
	xyPos[ 0 ] += xToAppend.length;
	xyArray[ xyPos[ 0 ] ++ ] = '\n';
	return xyArray;
}

public static byte[] appendClone( byte[] str, int offs, byte[] app ) {
	if ((str == null) || (str.length <= 0)) {
		return app;
	} else if ((app == null) || (app.length <= 0)) {
		return str;
	}
	offs = (offs >= 0) ? offs : str.length;
	byte[] out = new byte[ offs + app.length ];
	System.arraycopy( str, 0, out, 0, (offs < str.length) ? offs : str.length );
	System.arraycopy( app, 0, out, offs, app.length );
	return out;
}

public static byte[] replaceClone( byte[] strUtf8, byte chOld, byte chNew ) {
	int i0 = strUtf8.length - 1;
	for (; i0 >= 0; -- i0) {
		if (strUtf8[ i0 ] == chOld) {
			break;
		}
	}
	if (i0 < 0) {
		return strUtf8;
	}
	byte[] out = new byte[ strUtf8.length ];
	System.arraycopy( strUtf8, 0, out, 0, strUtf8.length );
	for (; i0 >= 0; -- i0) {
		if (out[ i0 ] == chOld) {
			out[ i0 ] = chNew;
		}
	}
	return out;
}

public static String toString( byte[] str ) {
	String out;
	try {
		out = new String( str, "UTF-8" );
	} catch (UnsupportedEncodingException e) {
		out = new String( str );
	}
	return out;
}

public static byte[] toBytes( String str ) {
	byte[] out;
	try {
		out = str.getBytes( "UTF-8" );
	} catch (UnsupportedEncodingException e) {
		out = str.getBytes();
	}
	return out;
}

public static String toId( String str, boolean includeSlash ) {
	// Expected to be ASCII char's:
	String sx = str.trim();
	// Empty string not allowed:
	if (sx.length() <= 0) {
		return ".";
	}
	if (includeSlash) {
		if (sx.matches( "[0-9A-Za-z\\._/]+" ) && !sx.contains( "//" )) {
			return str;
		}
		return sx.replace( ' ', '_' ).replace( ',', '/' ).replaceAll( "[^0-9A-Za-z\\._/]", "." ) //.
			.replaceAll( "[_\\./]+/[_\\./]*", "/" );
	}
	if (sx.matches( "[0-9A-Za-z\\._]+" )) {
		return str;
	}
	return sx.replace( ' ', '_' ).replaceAll( "[^0-9A-Za-z\\._]", "." );
}

public static String toField( String str ) {
	if ((str.indexOf( '\t' ) < 0) && (str.indexOf( '\n' ) < 0)) {
		return str;
	}
	return str.replace( '\t', '.' ).replace( '\n', '.' );
}

public static String toTabs( String str ) {
	if (str.indexOf( '\n' ) < 0) {
		return str;
	}
	return str.replace( '\n', '\t' );
}

public static byte[] toField( byte[] str ) {
	byte[] out = replaceClone( str, (byte) '\t', (byte) '.' );
	out = replaceClone( out, (byte) '\n', (byte) '.' );
	return out;
}

public static byte[] toTabs( byte[] str ) {
	return replaceClone( str, (byte) '\n', (byte) '\t' );
}

public static String toBase64X( int[] bits, long... vals ) {
	int len = 0;
	for (int b0 : bits) {
		len += b0 / 6;
	}
	char[] out = new char[ len ];
	int inx = len;
	for (int iv = vals.length - 1; iv >= 0; -- iv) {
		long val = vals[ iv ];
		for (int i0 = bits[ iv ] / 6 - 1; i0 >= 0; -- i0) {
			int b64 = 0x3f & (int) val;
			-- inx;
			out[ inx ] = base64XChars[ b64 ];
			val = val >>> 6;
		}
	}
	if (0 != inx) {
		return null;
	}
	return new String( out );
}

/**
 * Cmp. Wikipedia 'Jenkins_hash_function' (2015), http://www.burtleburtle.net/bob/hash/doobs.html.
 */
public static int hash32_condensedString( String... parts ) {
	int len = 0;
	for (String key : parts) {
		len += key.length();
	}
	int hash = len;
	for (String key : parts) {
		for (int i = key.length() - 1; i >= 0; -- i) {
			int c0 = key.charAt( i );
			if (c0 <= ' ') {
				continue;
			}
			int b0 = c0 >>> 8;
			int b1 = c0 & 0xff;
			if (b0 != 0) {
				hash += b0;
				hash += (hash << 10);
				hash ^= (hash >>> 6);
			}
			hash += b1;
			hash += (hash << 10);
			hash ^= (hash >>> 6);
		}
	}
	hash += (hash << 3);
	hash ^= (hash >>> 11);
	hash += (hash << 15);
	return hash;
}

/** Create object ID (OID) for UID = source (e-mail address/ web site) + created OID.
 *  The created ID itself, whether created locally or elsewhere, has to stay unique
 *  for local storage and 'as unique as possible' for collaborative data
 *  stored on any device (for tracing changes) ==> conflict resolution mechanism
 *  ('eventual consistency').
 * @param content Text or data for hash value as part of ID (less conflicts).
 * @return 12/ 16/ 20 chars depending on length of sequence count.
 */
public static String createId( String content ) {
	String dateHour = toDate4Millis().substring( 0, 15 );
	int year = (dateHour.charAt( 0 ) & 0xf) * 1000 + (dateHour.charAt( 1 ) & 0xf) * 100 + (dateHour.charAt( 2 ) & 0xf) * 10
		+ (dateHour.charAt( 3 ) & 0xf);
	int month = (dateHour.charAt( 5 ) & 0xf) * 10 + (dateHour.charAt( 6 ) & 0xf);
	int day2 = ((dateHour.charAt( 8 ) & 0xf) * 10 + (dateHour.charAt( 9 ) & 0xf)) << 1;
	int hour = ((dateHour.charAt( 11 ) & 0xf) * 10 + (dateHour.charAt( 12 ) & 0xf));
	day2 = day2 - ((12 > hour) ? 1 : 0);
//    hour2 += ('3' <= dateHour.charAt(14)) ? 1 : 0;
	int stamp = (((year - 2000) & 0xf0) << 14) | (((year - 2000) & 0xf) << 12) | (month << 6) | day2;
	if (stamp <= idStamp) {
		++ idCount;
	} else {
		idStamp = stamp;
		idCount = 1;
	}
	int len = content.length();
	int hash0 = (4096 > len) ? hash32_condensedString( content ) //,
		: hash32_condensedString( content.substring( 0, 1024 ), content.substring( len / 2, len / 2 + 1024 ),
			content.substring( len - 1024 ) );
	long hash = 0xffffffffL & hash0;
	// 16 bits = 2 * byteLen = 3 * b64Len - 2:
	int lenCount = 18 - 2;
	// 24 bits = 3 * byteLen = 4 * b64Len: (special case: first bits at '111111' for conflict resolution)
	long countHigh = (idCount >> 4);
	long lowNHash2 = ((idCount & 0xff) << 2) | (hash >>> 30);
	if (idCount >= (1L << 38)) {
		lenCount += 48;
		// Keep sort order of first byte: keep first bits at '110':
		countHigh |= 6L << 57;
	} else if (idCount >= (1 << 15)) {
		lenCount += 24;
		// First bits at '10':
		countHigh |= 2L << 34;
	} // else: first bit at '0'
	qLastId = toBase64X( new int[] { 24, lenCount - 4, 6, 30 }, idStamp, countHigh, lowNHash2, hash );
	return qLastId;
}

/** Create salt with 7 byte header and 9 random bytes.
 * @param xMagicBytes 2 bytes
 * @param xInfo 3 bytes: e.g. header block count, signature block count, key info
 * @return
 */
public static byte[] createHeaderSalt16( byte[] xMagicBytes, byte[] xInfo, CsvCodecIf encoder ) {
	long msec = System.currentTimeMillis();
	byte[] salt16 = new byte[ 16 ];
	byte[] rand = encoder.getInitialValue( 9 );
	System.arraycopy( rand, 0, salt16, 7, 9 );
	for (int i0 = 8; i0 >= 4; -- i0) {
		salt16[ i0 ] = (byte) (msec >> ((8 - i0) * 8));
	}
	if (null != xMagicBytes) {
		salt16[ 0 ] = xMagicBytes[ 0 ];
		salt16[ 1 ] = xMagicBytes[ 1 ];
		salt16[ 2 ] = Dib2Constants.fileStrucVersion;
		salt16[ 3 ] = encoder.getMethodTag();
		salt16[ 4 ] = (1 <= xInfo.length) ? xInfo[ 0 ] : salt16[ 4 ];
		salt16[ 5 ] = (1 <= xInfo.length) ? xInfo[ 1 ] : salt16[ 5 ];
		salt16[ 6 ] = (1 <= xInfo.length) ? xInfo[ 2 ] : salt16[ 6 ];
	}
	return salt16;
}

public static int getHeaderInfo( byte[] xHeader, int offs, byte[] yInfo ) {
	int version = xHeader[ offs + 2 ] & 0xff;
	if (null != yInfo) {
		yInfo[ 0 ] = 0;
		if (4 <= version) {
			System.arraycopy( xHeader, offs + 4, yInfo, 0, 3 );
		}
	}
	return version;
}

public static void writeFile( String path, byte[] dat, int offset, int length, byte[] xHeader ) throws Exception {
	RandomAccessFile file = new RandomAccessFile( path, "rw" );
	file.seek( 0 );
	if (xHeader != null) {
		file.write( xHeader );
	}
	file.write( dat, offset, length );
	file.setLength( file.getFilePointer() );
	file.close();
}

public static byte[] readFile( String xPath, int xExpectedFileStruc ) throws Exception {
	RandomAccessFile file = new RandomAccessFile( xPath, "r" );
	byte[] tlv = new byte[ 2 ];
	byte[] dat;
	int offs = 0;
	if (0 >= xExpectedFileStruc) {
		dat = new byte[ (int) file.length() ];
	} else {
		file.read( tlv );
		if (Dib2Constants.magicBytes[ 0 ] == tlv[ 0 ]) {
			// Old format.
			byte[] xlen = to4880Len( (int) file.length() );
			offs = xlen.length + 1;
			dat = new byte[ 1 + xlen.length + (int) file.length() ];
			dat[ 0 ] = (byte) Dib2Constants.RFC4880_EXP2;
			System.arraycopy( xlen, 0, dat, 1, xlen.length );
		} else {
			dat = new byte[ (int) file.length() ];
		}
		file.seek( 0 );
	}
	file.read( dat, offs, dat.length - offs );
	file.close();
	return dat;
}

/** OLD: little endian.
 * @param val >= 0
 * @return
 */
public static byte[] int2Tlv( int val ) {
	if (32 > val) {
		return new byte[] { (byte) val };
	}
	if (0x100 > val) {
		return new byte[] { 0x21, (byte) val };
	}
	byte[] little = new byte[] { (byte) (val & 0xff), (byte) ((val >>> 8) & 0xff), (byte) ((val >>> 16) & 0xff), (byte) (val >>> 24) };
	if (0x10000 > val) {
		return new byte[] { 0x22, little[ 0 ], little[ 1 ] };
	}
	if (0x1000000 > val) {
		return new byte[] { 0x23, little[ 0 ], little[ 1 ], little[ 2 ] };
	}
	return new byte[] { 0x24, little[ 0 ], little[ 1 ], little[ 2 ], little[ 3 ] };
}

public static int tlv2Len_OLD( byte[] packet, int offset ) {
	int pack0 = packet[ offset ] & 0xff;
	// Old format?
	if ((32 > pack0)) {
		return packet[ offset ];
	}
	if ((pack0 & 0x28) != 0x20) {
		// Unknown format.
		return -1;
	}
	// Old format.
	int len = packet[ offset ] & 0x7;
	int val = 0;
	for (int i0 = len - 1; i0 >= 0; -- i0) {
		val |= (packet[ offset + i0 + 1 ] & 0xff) << (i0 << 3);
	}
	return val;
}

/** RFC 4880: header len 2 if < 192, 3 if <= 8283, 6 otherwise.@param packet
 * @param offset
 * @return
 */
public static int getPacketLen( byte[] packet, int offset ) {
	final byte[] p = packet;
	final int o = offset;
	return (192 > (p[ o ] & 0xff)) ? (p[ o ] & 0xff) //,
		: ((223 >= (p[ o ] & 0xff)) ? (((((p[ o ] & 0xff) - 192) << 8) | (p[ o + 1 ] & 0xff)) + 192) //,
			: (((p[ o + 1 ] & 0xff) << 24) | ((p[ o + 2 ] & 0xff) << 16) | ((p[ o + 3 ] & 0xff) << 8) | (p[ o + 4 ] & 0xff)));
}

public static byte[] to4880Len( int len ) {
	return (192 > len) ? new byte[] { (byte) len } //,
		: ((8283 >= len) ? new byte[] { (byte) ((((len - 192) >> 8) & 0xff) + 192), (byte) (len - 192) } //,
			: new byte[] { (byte) 0xff, (byte) (len >> 24), (byte) (len >> 16), (byte) (len >> 8), (byte) len });
}

public static int getPacketHeaderLen( byte[] packet, int offset ) {
	return (192 > (packet[ offset + 1 ] & 0xff)) ? 2 //,
		: ((223 >= (packet[ offset + 1 ] & 0xff)) ? 3 : 6);
}

/** Cmp. RFC 4880. E.g. for tag 11 (literal): one-byte format ('b' binary/ 'u' UTF8),
 * len-of-filename, filename, 4-byte time stamp, data.
 * @param xTag e.g. 0x80 | 0x40 | 11=literal, | 62=experimental2
 * @param xName4Literal (currently NULL)
 * @param xyData
 * @param xOffs4Reuse >2 for keeping data in xyData[], -1 for new byte[]
 * @return new data starting at 0 or at data[0]
 */
public static byte[] toPacket( int xTag, byte[] xName4Literal, byte[] xyData, int xOffs4Reuse, int xEnd ) {
	// TODO: additional len if (xName4Literal != NULL)
	int len = ((0 > xEnd) ? xyData.length : xEnd) - xOffs4Reuse;
	byte[] qlen = to4880Len( len );
	byte[] out = (2 > xOffs4Reuse) ? new byte[ len + qlen.length + 1 ] : xyData;
	int offs = (2 > xOffs4Reuse) ? 0 : (xOffs4Reuse - qlen.length - 1);
	out[ 0 ] = (byte) offs;
	out[ offs ] = (byte) xTag;
	System.arraycopy( qlen, 0, out, offs + 1, qlen.length );
	return out;
}

/** OLD: little endian.
 * @param len
 * @return
 */
public static byte[] lvLen( long len ) {
	// bits: 00xxxxxx/ 010xxxxx
	if (len <= ((3 << 5) - 1)) {
		return new byte[] { (byte) len };
	}
	// bits: 0xxxxxxx
	if (len <= ((1 << 15) - 2)) {
		return new byte[] { (byte) (len & 0x7f), (byte) (len >> 7) };
	}
	// bits: 10xxxxxx, 110xxxxx, ...
	int lead = 0x80;
	byte[] out = new byte[ 9 ];
	int inx = 8;
	for (int shift = 57; shift >= 22; shift -= 7) {
		out[ inx ] = (byte) ((len >>> shift) & 0xff);
		lead += lead >> 1;
	}
	out[ 0 ] |= lead;
	return out;
}

/** Compress using Java's Deflater.
 * @param xyData data[offset] should contain part of magic byte (>= 0x30)
 * @return new data starting at 0 or at data[0]
 */
public static byte[] compress( int xTag, byte[] xyData, int xOffs4Reuse, int to ) {
	Deflater deflater = new Deflater( Deflater.DEFAULT_COMPRESSION, true );
	int len = to - xOffs4Reuse;
	byte[] full = to4880Len( len );
	deflater.setInput( xyData, xOffs4Reuse, len );
	deflater.finish();
	int outOffs = 22;
	byte[] out = new byte[ 2 * xyData.length + 2 * outOffs ];
	len = deflater.deflate( out, outOffs, out.length - outOffs );
	deflater.end();
	byte[] mb2 = Dib2Constants.magicBytes;
	byte[] time = to4880Len( (int) (currentTimeMillisLinearized() / 1000) );
	byte[] mbNStamp = new byte[] { mb2[ 0 ], mb2[ 1 ], (byte) xTag, (byte) (full.length + time.length) };
	mbNStamp = Arrays.copyOf( mbNStamp, mbNStamp.length + mbNStamp[ 3 ] );
	System.arraycopy( full, 0, mbNStamp, 4, full.length );
	System.arraycopy( time, 0, mbNStamp, 4 + full.length, time.length );
	byte[] header = (1 < xTag) ? mbNStamp : new byte[ xTag ];
	if ((len >= (xyData.length - xOffs4Reuse)) && (0 < xOffs4Reuse)) {
		// No compression:
		out = Arrays.copyOfRange( xyData, xOffs4Reuse, to );
		len = out.length;
		full = new byte[] { 0 };
	}
	byte[] ll = to4880Len( len + header.length );
	if (1 == header.length) {
		// Old format (magic bytes not used).
		byte[] tlv0 = int2Tlv( to - xOffs4Reuse );
		byte[] tlv1 = (len >= 32) ? int2Tlv( len ) : new byte[] { 0x21, (byte) len };
		xyData[ 0 ] = (byte) xOffs4Reuse;
		System.arraycopy( tlv0, 0, xyData, xOffs4Reuse, tlv0.length );
		// 0x50 + 0x2x: marker for binary data + len-of-len:
		xyData[ xOffs4Reuse + tlv0.length ] = (byte) (0x50 + tlv1[ 0 ]);
		System.arraycopy( tlv1, 1, xyData, xOffs4Reuse + 1 + tlv0.length, tlv1.length - 1 );
		System.arraycopy( out, outOffs, xyData, xOffs4Reuse + tlv0.length + tlv1.length, len );
		return xyData; // tlv0.length + tlv1.length + len;
	} else if (0 < header.length) {
		int offs = (xOffs4Reuse - header.length - 1 - ll.length);
		if (0 <= offs) {
			System.arraycopy( out, outOffs, xyData, xOffs4Reuse, len );
			out = xyData;
			outOffs = xOffs4Reuse;
		} else {
			out = Arrays.copyOf( out, outOffs + len );
			offs = outOffs - header.length - 1 - ll.length;
		}
		out[ 0 ] = (byte) offs;
		out[ offs ++ ] = (byte) Dib2Constants.RFC4880_EXP2;
		System.arraycopy( ll, 0, out, offs, ll.length );
		System.arraycopy( header, 0, out, offs + ll.length, header.length );
	} else {
		if (10 < xOffs4Reuse) {
			System.arraycopy( out, outOffs, xyData, xOffs4Reuse, len );
			out = xyData;
			outOffs = xOffs4Reuse;
		}
		out[ 0 ] = (byte) (outOffs - ll.length - full.length);
		System.arraycopy( full, 0, out, outOffs - ll.length - full.length, full.length );
		System.arraycopy( ll, 0, out, outOffs - ll.length, ll.length );
	}
	return out;
}

public static byte[] decompress( byte[] xData, int from, int to ) {
	int offs = from;
	int full = 6 * (to - from);
	if (xData[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		offs += (192 > (xData[ offs + 1 ] & 0xff)) ? 2 //,
			: ((223 >= (xData[ offs + 1 ] & 0xff)) ? 3 : 6);
	}
	if (0x27 > (xData[ from ] & 0xff)) {
		// Old format.
		full = tlv2Len_OLD( xData, from );
		offs += 1 + (xData[ from ] & 0x7);
		// -1 for error or bad value?
		if (full < (to - from)) {
			return xData;
		}
		// Expect 0x7x as marker for compressed data:
		if ((xData[ offs ] & 0x78) != 0x70) {
			return xData;
		}
		offs += 1 + (xData[ offs ] & 0x7);
	} else if (Dib2Constants.magicBytes[ 0 ] == xData[ offs ]) {
		// Skip markers:
		for (int i0 = 0; i0 <= Dib2Constants.magicBytes.length; ++ i0, ++ offs) {
			if ('z' == xData[ offs ]) {
				++ offs;
				break;
			}
		}
		full = getPacketLen( xData, offs + 1 );
		if (0 >= full) {
			return Arrays.copyOfRange( xData, from + 2, to );
		}
		offs += 1 + xData[ offs ];
	}
	Inflater inflater = new Inflater( true );
	inflater.setInput( xData, offs, to - offs );
	byte[] out = new byte[ full ];
	int done = 0;
	try {
		done = inflater.inflate( out );
		while (!inflater.finished()) {
			out = Arrays.copyOf( out, 2 * out.length );
			int add = inflater.inflate( out, done, out.length - done );
			if (0 >= add) {
				break;
			}
			done += add;
		}
	} catch (DataFormatException e) {
		return xData;
	}
	inflater.end();
//	if (done < (data.length / 2 - 32)) {
	return Arrays.copyOf( out, done );
}

//=====
}
