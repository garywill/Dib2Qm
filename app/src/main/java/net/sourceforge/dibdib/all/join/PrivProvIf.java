// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG, based on the
// corresponding API from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.join;

import net.vreeken.quickmsg.attachment;

public interface PrivProvIf {
//=====

public void init( ContextIf app_context );

public attachment pgpmime_id();

public attachment encrypt_sign( attachment unenc, String to );

public attachment key_attachment( String member );

public boolean load_keys();

public boolean public_keyring_add_key( byte[] xInputStream, String[] yKeyFound );

public attachment decrypt_verify( attachment attachment );

public String fingerprint( String addr );

public void public_keyring_remove_by_address( String addr );

//=====
}
