// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.util;

import java.util.*;
import net.sourceforge.dibdib.config.Dib2Constants;

/** Use handles for data structures. This looks like a killer for garbage
 * collection, but for most use cases, especially with file storage
 * and in connection with a string tokenizer, it is okay -- as long as the
 * top level (which is a 'long[]') is kept on the stack or heap.
 * TODO: Additional methods for garbage collection when saving ...
 */
public final class QStruct {

/*
Handles (long!, cmp. QStr):

.......... .   .... ...... .
shash << 3 f1  sub  index  f0

 */

private static final int COMBINED_BITS__24 = 24; // test 10; // 32;
/** For splitting hash table. MAX_BITS = 2 * MIN_INDEX_BITS. */
private static final int MIN_INDEX_BITS = 15; // test 4; // 15;
/** Top 4 bits as index to subtable */
private static final int MAX_INDEX_BITS = COMBINED_BITS__24 - 4;
private static final long FLAG_STRUCT = 1L << COMBINED_BITS__24;
private static final int FLAG_STRUCT_INDEXED__1 = 1;
static final long FLAG_VALUE_CAP__4 = 4;
static final long FLAG_VALUE_CAP_ALL__6 = 6;
static final long FLAG_VALUE_CAP_FIRST__4 = 4;
static final long FLAGS_STRUCT_TYPE = 3L << (COMBINED_BITS__24 + 1);

private static final int MAX_COLLISIONS = 4;
private static final int MAX_COLLISION_BUFFER = 2 * MAX_COLLISIONS + 2;
private static final int SPILL_INIT = 1 + (1 << MIN_INDEX_BITS) * 20 / 100;

/** Pairs of shash and string values. */
private static final String[][] mQStrs;
private static volatile int[] mcQStrs;
private static volatile int mQStrSubs;

//TODO
private static final long[][][] mStructs;
private static volatile int[] mcStructs;
//private static volatile int mStructSubs;

private static final HashMap< Integer, int[] > mSpill = new HashMap< Integer, int[] >();

static {
	mQStrSubs = 2; // Bits = MIN_INDEX_BITS;
	mQStrs = new String[ 1 << (COMBINED_BITS__24 - MAX_INDEX_BITS) ][];
	mQStrs[ 1 ] = new String[ (1 << MIN_INDEX_BITS) + MAX_COLLISION_BUFFER ];
	mQStrs[ 0 ] = new String[ SPILL_INIT ];
//	mQStrs[ 0 ][ 0 ] = "";
	mcQStrs = new int[ 1 << (COMBINED_BITS__24 - MAX_INDEX_BITS) ];
	mcQStrs[ 0 ] = 0; // 1;
	mStructs = new long[ 1 << (COMBINED_BITS__24 - MAX_INDEX_BITS) ][][];
	mStructs[ 1 ] = new long[ (1 << MIN_INDEX_BITS) + MAX_COLLISION_BUFFER ][];
	mStructs[ 0 ] = new long[ SPILL_INIT ][];
//	mStructs[ 0 ][ 0 ] = new long[ 0 ];
	mcStructs = new int[ 1 << (COMBINED_BITS__24 - MAX_INDEX_BITS) ];
	mcStructs[ 0 ] = 0; // 1;
}

public static long shash2handleOffset( String shash ) {
	final int len = shash.length();
	if (0 >= len) {
		return 0;
	}
	long offs = (shash.charAt( 0 ) & 0x1fffL) << (3 + 48);
	offs |= (1 >= len) ? 0 : ((long) shash.charAt( 1 ) << (3 + 32));
	if (4 >= len) {
		offs |= (2 >= len) ? 0 : ((long) shash.charAt( 2 ) << (3 + 16));
		offs |= (3 >= len) ? 0 : (shash.charAt( 3 ) << 3);
		// Last bit is 0:
		return offs;
	}
	offs |= ((long) shash.charAt( 2 ) & 0xffc0) << (3 + 16);
	return offs | FLAG_STRUCT_INDEXED__1;
}

public static String handle2Shash( long handle ) {
	if (0 == handle) {
		return "";
	} else if (-1 == handle) {
		return QStr.SHASH_NAN;
	} else if (0 == (handle & FLAG_STRUCT_INDEXED__1)) {
		char[] shash = new char[ 4 ];
		long inBytes = handle >>> 3;
		for (int i0 = 3; i0 >= 0; -- i0) {
			shash[ i0 ] = (char) inBytes;
			inBytes >>>= 16;
		}
		int len = 4;
		for (; len > 0; -- len) {
			if (0 != shash[ len - 1 ]) {
				break;
			}
		}
		shash[ 0 ] |= 0xe000;
		return new String( Arrays.copyOf( shash, len ) );
	} else if (1 == (FLAG_STRUCT & handle)) {
		StringBuilder shash = new StringBuilder( Dib2Constants.SHASH_MAX * 2 );
		long[] pieces = mStructs[ (int) handle >>> MAX_INDEX_BITS ][ (int) handle & ((1 << MAX_INDEX_BITS) - 1) ];
		for (long piece : pieces) {
			shash.append( handle2Shash( piece ) );
			if (Dib2Constants.SHASH_MAX <= shash.length()) {
				break;
			}
		}
		return (Dib2Constants.SHASH_MAX <= shash.length()) ? shash.substring( 0, Dib2Constants.SHASH_MAX - 1 ) : shash.toString();
	}
	return mQStrs[ 0xf & ((int) handle >>> MAX_INDEX_BITS) ][ (int) handle & ((1 << MAX_INDEX_BITS) - 2) ];
}

private static String handleValue2String( long handle ) {
	String out = QStr.shash2String( handle2Shash( handle ) );
	if ((0 == (handle & FLAG_VALUE_CAP__4)) || (0 >= out.length())) {
		return out;
	}
	return (FLAG_VALUE_CAP_ALL__6 == (handle & FLAG_VALUE_CAP_ALL__6)) ? out.toUpperCase() //,
		: ("" + (char) (out.charAt( 0 ) - 0x20) + out.substring( 1 ));
}

/**
 * @param handle
/** @param xahDelimiters (optional) infix, prefix, postfix
/** @return
 */
public static/*no need for: synchronized*/String handle2String( long handle, long... xahDelimiters ) {
	if (0 == handle) {
		return "";
	} else if (-1 == handle) {
		return "#^";
	} else if (0 == (handle & FLAG_STRUCT_INDEXED__1)) {
		return handleValue2String( handle );
	} else if (FLAG_STRUCT == (FLAG_STRUCT & handle)) {
		String pre = (xahDelimiters.length >= 2) ? handleValue2String( xahDelimiters[ 1 ] ) : "\n";
		String post = (xahDelimiters.length >= 3) ? handleValue2String( xahDelimiters[ 2 ] ) : "\r";
		String infix = "";
		String infix2 = (xahDelimiters.length >= 1) ? handleValue2String( xahDelimiters[ 0 ] ) : "\t";
		long[] pieces = mStructs[ (int) handle >>> MAX_INDEX_BITS ][ (int) handle & ((1 << MAX_INDEX_BITS) - 1) ];
		StringBuilder out = new StringBuilder( 5 * pieces.length );
		out.append( pre );
		for (long piece : pieces) {
			out.append( infix );
			out.append( handle2String( piece, xahDelimiters ) );
			infix = infix2;
		}
		out.append( post );
		return out.toString();
	}
	return mQStrs[ 0xf & ((int) handle >>> MAX_INDEX_BITS) ][ (int) handle & ((1 << MAX_INDEX_BITS) - 1) ];
}

/*private*/static int hash( long[] vals ) {
	int hash = vals.length;
	int i0 = -2 + ((128 > hash) ? hash : 128);
	for (; i0 >= 0; i0 -= 2) {
		hash += 31 * 31 * hash + 31 * (int) ((vals[ i0 + 1 ] >>> 32) | vals[ i0 + 1 ]) + (int) ((vals[ i0 ] >>> 32) | vals[ i0 ]);
	}
	for (i0 += 1; i0 >= 0; -- i0) {
		hash += 31 * hash + (int) ((vals[ i0 ] >>> 32) | vals[ i0 ]);
	}
	return hash;
}

public static/*no need for: synchronized*/long getHandle( double value ) {
	return shash2handleOffset( QStr.double2Shash( null, value ) );
}

public static/*no need for: synchronized*/long getHandle( String str ) {
	if (0 == str.length()) {
		return 0;
	}
	String shash = QStr.shash( false, str )[ 0 ];
	long offs = shash2handleOffset( shash );
	if (0 == (offs & FLAG_STRUCT_INDEXED__1)) {
		// Expected to be lowercase:
		String cmp = QStr.shash2String( shash );
		if ((str.length() == cmp.length()) && ((str.charAt( 0 ) & 0x1f) == (cmp.charAt( 0 ) & 0x1f))) {
			if (str.equals( cmp )) {
				return offs;
			} else if ((str.charAt( 0 ) + 0x20) == cmp.charAt( 0 )) {
				if (str.substring( 1 ).equals( cmp.substring( 1 ) )) {
					return offs | FLAG_VALUE_CAP_FIRST__4;
				} else if (str.equals( cmp.toUpperCase() )) {
					return offs | FLAG_VALUE_CAP_ALL__6;
				}
			}
		}
	}
	offs = offs & ~(2L * FLAG_STRUCT - 1);
	int hash = str.hashCode() | FLAG_STRUCT_INDEXED__1;
	for (int sub = mQStrSubs - 1; sub > 0; -- sub) {
		// hash & 0x..fff:
		int inx = hash & (mQStrs[ sub ].length - MAX_COLLISION_BUFFER - 1);
		inx = (inx >= ((1 << MAX_INDEX_BITS) - MAX_COLLISION_BUFFER)) ? (inx - MAX_COLLISION_BUFFER) : inx;
		for (int i0 = inx; i0 < (inx + MAX_COLLISIONS * 2); i0 += 2) {
			String el = mQStrs[ sub ][ i0 ];
			if (null == el) {
				break;
			} else if (str.equals( el )) {
				return offs | ((long) sub << MAX_INDEX_BITS) | i0;
			}
		}
	}
	int[] spilled = mSpill.get( hash );
	if (null != spilled) {
		for (int i0 = spilled.length - 1; i0 >= 0; -- i0) {
			// Potential race condition (null) would cause a miss, but does not hurt:
			if (str.equals( mQStrs[ 0 ][ spilled[ i0 ] ] )) {
				return offs | spilled[ i0 ];
			}
		}
	}
	return -1;
}

public static synchronized long makeHandle( String str ) {
	if (0 == str.length()) {
		return 0;
	}
	String shash = QStr.shash( false, str )[ 0 ];
	long offs = shash2handleOffset( shash );
	if (0 == (offs & FLAG_STRUCT_INDEXED__1)) {
		// Expected to be lowercase:
		String cmp = QStr.shash2String( shash );
		if ((str.length() == cmp.length()) && ((str.charAt( 0 ) & 0x1f) == (cmp.charAt( 0 ) & 0x1f))) {
			if (str.equals( cmp )) {
				return offs;
			} else if ((str.charAt( 0 ) + 0x20) == cmp.charAt( 0 )) {
				if (str.substring( 1 ).equals( cmp.substring( 1 ) )) {
					return offs | FLAG_VALUE_CAP_FIRST__4;
				} else if (str.equals( cmp.toUpperCase() )) {
					return offs | FLAG_VALUE_CAP_ALL__6;
				}
			}
		}
	}
	offs = offs & ~(2L * FLAG_STRUCT - 1);
	int put1 = -1;
	int put0 = -1;
	int hash = str.hashCode() | FLAG_STRUCT_INDEXED__1;
	for (int sub = mQStrSubs - 1; sub > 0; -- sub) {
		// hash & 0x..fff:
		int inx = hash & (mQStrs[ sub ].length - MAX_COLLISION_BUFFER - 1);
		inx = (inx >= ((1 << MAX_INDEX_BITS) - MAX_COLLISION_BUFFER)) ? (inx - MAX_COLLISION_BUFFER) : inx;
		for (int i0 = inx; i0 < (inx + MAX_COLLISIONS * 2); i0 += 2) {
			String el = mQStrs[ sub ][ i0 ];
			if (null == el) {
				// Keep first spot, but also fill empty spaces with short collision runs:
				if ((0 > put1) || (null == mQStrs[ sub ][ i0 + 2 ])) {
					put1 = sub;
					put0 = i0;
				}
				break;
			} else if (str.equals( el )) {
				return offs | ((long) sub << MAX_INDEX_BITS) | i0;
			}
		}
	}
	int[] spilled = mSpill.get( hash );
	if (null != spilled) {
		for (int i0 = spilled.length - 1; i0 >= 0; -- i0) {
			if (str.equals( mQStrs[ 0 ][ spilled[ i0 ] ] )) {
				return offs | spilled[ i0 ];
			}
		}
	}
	if (0 <= put1) {
		mQStrs[ put1 ][ put0 ] = str;
		mQStrs[ put1 ][ put0 - 1 ] = shash;
		++ mcQStrs[ put1 ];
		return offs | ((long) put1 << MAX_INDEX_BITS) | put0;
	}
	if (((mcQStrs[ mQStrSubs - 1 ] * 2) < (mQStrs[ mQStrSubs - 1 ].length / 2))
		|| (mQStrSubs >= (1 << (COMBINED_BITS__24 - MAX_INDEX_BITS)))) {
		///// Spill.
		if ((mcQStrs[ 0 ] * 2) >= mQStrs[ 0 ].length) {
			mQStrs[ 0 ] = Arrays.copyOf( mQStrs[ 0 ], 2 * mQStrs[ 0 ].length );
		}
		int i0 = mcQStrs[ 0 ] * 2 + 1;
		mQStrs[ 0 ][ i0 ] = str;
		mQStrs[ 0 ][ i0 - 1 ] = shash;
		spilled = (null == spilled) ? new int[ 1 ] : Arrays.copyOf( spilled, spilled.length + 1 );
		spilled[ spilled.length - 1 ] = i0;
		mSpill.put( hash, spilled );
		mcQStrs[ 0 ] ++;
		return offs | i0;
	}
	put1 = mQStrSubs;
	mQStrSubs *= 2;
	int newSize = ((mQStrSubs >= 16) ? (1 << MAX_INDEX_BITS) : (mQStrSubs * (1 << MIN_INDEX_BITS))) + MAX_COLLISION_BUFFER;
	for (int i0 = mQStrSubs / 2; i0 < mQStrSubs; ++ i0) {
		mQStrs[ i0 ] = new String[ newSize ];
	}
	put0 = hash & (mQStrs[ put1 ].length - MAX_COLLISION_BUFFER - 1);
	mQStrs[ put1 ][ put0 ] = str;
	mQStrs[ put1 ][ put0 - 1 ] = shash;
	++ mcQStrs[ put1 ];
	return offs | ((long) put1 << MAX_INDEX_BITS) | put0;
}

public static double handleNum2Double( long handle ) {
	if (-1 == handle) {
		return Double.NaN;
	}
	String shash = handle2Shash( handle );
	return (null == shash) ? Double.NaN : QStr.shashNum2Double( shash );
}

}
