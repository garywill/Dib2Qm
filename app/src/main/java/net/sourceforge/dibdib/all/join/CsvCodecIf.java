// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.all.join;

public interface CsvCodecIf {
//=====

void init( char platform, Object... parameters );

byte[] compress( byte[] xyData, int xOffset4Reuse, int to );

byte[] decompress( byte[] xData, int length );

/** Header bytes: 0..1 magic bytes, 2 version/format, 3 encoder, 4.. counts etc. */
byte[] encode( byte[] compressedData, int from, int to, byte[] key, int keyInfo, byte[] keyData, byte[] signatureKey ) throws Exception;

/** Header bytes: 0..1 magic bytes, 2 version/format, 3 encoder, 4.. counts etc. */
byte[] encode4DerivedKey( byte[] compressedData, int from, int to, byte[] pass ) throws Exception;

byte[] decode( byte[] data, int from, int to, byte[] keyOrPass, byte[] signatureKey ) throws Exception;

byte[] getInitialValue( int len );

byte getMethodTag();

//StringBuffer getLog();

//=====
}
