// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android.qm;

import android.content.Intent;
import android.net.Uri;
import android.view.*;
import net.sourceforge.dibdib.all_joined.chat.Contact;
import net.sourceforge.dibdib.android.dib2qm.R;

public final class MainActivity extends net.sourceforge.dibdib.android_joined.qm.MainActivity_1 {
// TODO: HTML tags
// WebView webView = (WebView) findViewById(R.id.webView);
// webView.loadDataWithBaseURL(null, htmlAsString, "text/html", "UTF-8", null);
/*
b	Bold
i	Italics
u	Underline
sub	Subtext
sup	Supertext
big	Big
small	Small
tt	Monospace
h1 … h6	Headlines
<font color="red"...
<blockquote>Example <a href=\"www...\">www</a></blockquote>
 */

@Override
public void send_msg( View view ) {
	super.send_msg( view );

	// todo: service for sending
	if (contact.equals( null )) {
		try {
			// case 1:
			String smsNumber = contact.phone_get().substring( 1 ); // without '+'
			Intent sendIntent = new Intent( "android.intent.action.MAIN" );
			sendIntent.setAction( Intent.ACTION_SEND );
			//sendIntent.setType("text/plain");
			//sendIntent.putExtra(Intent.EXTRA_TEXT, "text");
			Uri uri = Uri.fromFile( null ); //file in download area
			sendIntent.putExtra( Intent.EXTRA_STREAM, uri );
			// SetType("application/pdf")
			sendIntent.setType( "application/vnd.ms-word" );
			sendIntent.putExtra( "jid", smsNumber + "@s.whatsapp.net" );
			sendIntent.setPackage( "com.whatsapp" );
			startActivity( sendIntent );
		} catch (Exception e) {
			//Toast.makeText(this, "Error " + e, Toast.LENGTH_SHORT).show();
		}
	}
}

@Override
public boolean onCreateOptionsMenu( Menu menu ) {
	getMenuInflater().inflate( R.menu.contact, menu );
	return true;
}

@Override
public boolean onOptionsItemSelected( MenuItem item ) {
	switch (item.getItemId()) {
		case R.id.action_edit_contact: {
			if (Contact.TYPE_GROUP == contact.type_get()) {
				set_member_dialog( contact );
			} else {
				edit_contact_dialog( contact );
			}
			return true;
		}
		case R.id.action_set_name: {
			if (Contact.TYPE_GROUP == contact.type_get()) {
				group_name_dialog( contact );
			} else {
				edit_contact_dialog( contact );
			}
			return true;
		}
		case R.id.action_contact_dial: {
			contact_dial( contact );
			return true;
		}
		case R.id.action_fingerprint: {
			view_fingerprint_dialog( contact );
			return true;
		}
		case R.id.action_send_key: {
			send_key_dialog( contact );
			return true;
		}
		case R.id.action_verify: {
			verify_dialog( contact );
			return true;
		}
		case R.id.action_sync: {
			background.setOffline( -1 );
			prefs.set( "offline", "FALSE" );
			local_message.send_background( getApplicationContext(), true );
			return true;
		}

		case R.id.action_stop: {
			background.setOffline( 2 );
			prefs.set( "offline", "TRUE" );
			local_message.send_background( getApplicationContext(), false );
			return true;
		}

		case R.id.action_temp: {
			background.setOffline( 0 );
			prefs.set( "offline", "0" );
			local_message.send_background( getApplicationContext(), true );
			return true;
		}

		default:
			return super.onOptionsItemSelected( item );
	}
}

}
