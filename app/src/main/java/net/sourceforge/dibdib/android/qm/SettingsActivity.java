// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android.qm;

public final class SettingsActivity extends net.vreeken.quickmsg.SettingsActivity_0 {
//=====

@Override
public void onPause() {
	quickmsg_db db = new quickmsg_db( this );
	db.save();
	super.onPause();
}

//=====
}
