// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android.qm;

// import net.vreeken.quickmsg.*;

import localhost.optional.chat.*;
import net.sourceforge.dibdib.all.join.PrivProvIf;
import net.sourceforge.dibdib.config.Dib2Config;

public class preferences extends quickmsg_db {
//=====

public preferences( Object context ) {
	super( context );
//	if (null == Dib2Config.qContextQm) {
	Dib2Config.init( 'A', this );
	Pgp.privProvs = (null == Pgp.privProvs) //,
	? new PrivProvIf[] { new EcDhQm() /*net.vreeken.quickmsg.pgp*/} //,
		: Pgp.privProvs;
}

//=====
}
