// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android.qm;

import android.content.*;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import java.io.*;
import java.util.Arrays;
import net.sourceforge.dibdib.all.util.*;
import net.sourceforge.dibdib.config.*;

public class quickmsg_db extends CsvQm implements net.sourceforge.dibdib.all.join.ContextIf {
//=====

public final String qPassFileName = "DibdibP.txt";

public final Context qCtx;
//private static int zCountAttempts = 0;
private static String zAccessCodeHex = null;
private static String zPassImapHex = "";
private static String zPassSmtpHex = "";

public quickmsg_db( Object context ) {
	super( null );
	qCtx = (Context) context;
}

@Override
public void init( char platform, Object... parameters ) {
	// Cmp. constructor of subclass.
}

@Override
public String fileReady() { // boolean checkPass ) {
	String out = super.fileReady(); // checkPass );
	if (null != out) {
		return out;
	}
//	if (checkPass) {
//		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
//		String pass = SP.getString( "imap_pass", "" );
//		if ((null == pass) || (0 >= pass.length())) {
//			return null;
//		}
//		return super.fileReady( false );
//	}
	File dir = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS );
	if (!dir.exists()) {
		return null;
	}
	File file = new File( dir, qDbFileName );
	if (file.exists()) {
		return file.getAbsolutePath();
	}
	dir = new File( dir, "dibdib" );
	if (!dir.exists()) {
		return null;
	}
	String[] names = dir.list();
	if (null == names) {
		return null;
	}
	String name = null;
	long time = -1;
	for (String x0 : names) {
		if (x0.matches( qDbFileName.replace( ".", ".*" ) )) {
			long x1 = new File( dir, x0 ).lastModified();
			if (time < x1) {
				time = x1;
				name = x0;
			}
		}
	}
	if (0 < time) {
		return new File( dir, name ).getAbsolutePath();
	}
	return null;
}

public void export_db( boolean full )
{
//	Dib2Config.log( "qdb export", "start" );
	save();
	close();
//	if (!Environment.getExternalStorageState().equals(
//		Environment.MEDIA_MOUNTED )) {
//		Toast.makeText( qCtx, "SD STORAGE NOT AVAILABLE", Toast.LENGTH_LONG ).show();
//		return;
//	}
//	File sd = Environment.getExternalStorageDirectory();
	String log = UtilMisc.logBuffer.toString();
	set( "log", "" );
	File path = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS );
	Dib2Config.log( "qdb export", path.getAbsolutePath() );
	path = new File( path, "dibdib" );
	path.mkdirs();
	String name = full ? UtilMisc.getShortDay().substring( 4, 6 ) : "bak";
	name = qDbFileName.replace( ".dm", "." + name + ".dm" );
	File csvPath = new File( path, name );
	if (!full && csvPath.exists()
		&& ((csvPath.lastModified() + 12 * Dib2Constants.MAX_DELTA_ACCESS_CHECK) >= UtilMisc.currentTimeMillisLinearized())) {
		return;
	}
	write( csvPath, true, false );
	if (!full) {
		return;
	}
//	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
//	Editor editor = SP.edit();
//	editor.putString( "accessCodeDate", "0" );
//	editor.commit();
	Toast.makeText( qCtx, "ENCODED CSV FILE: " + csvPath.toString(), Toast.LENGTH_LONG ).show();
	AssetManager am = qCtx.getAssets();
	String zipPath = new File( path, "dibdib_src.7z" ).getAbsolutePath();
	try {
		byte[] buffer = log.getBytes();
		UtilMisc.writeFile( new File( path, "dibdib_log.tmp" ).getAbsolutePath(), buffer, 0, buffer.length, null );
		InputStream is = am.open( "assets.7z" );
		buffer = new byte[ 1800000 ];
		int len = 0;
		while (true) {
			len += is.read( buffer, len, buffer.length - len );
			if (len < (buffer.length)) {
				break;
			}
			buffer = Arrays.copyOf( buffer, buffer.length * 2 );
		}
		UtilMisc.writeFile( zipPath, buffer, 0, len, null );
	} catch (Exception e) {
		// Ignore.
	}
}

/**
 * Caller has to utilize the PGP keys.
 * @return
 */
public Boolean import_db( String... optionalPhrase )
{
	save();
	File path = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS );
	byte[] phrase = ((null == optionalPhrase) || (0 >= optionalPhrase.length) //-
		|| (null == optionalPhrase[ 0 ]) || (0 >= optionalPhrase[ 0 ].length())) //-
	? null
		: UtilMisc.toBytes( UtilMisc.toHexString( UtilMisc.toBytes( optionalPhrase[ 0 ] ) ) );
	int count = importFile( new File( path, qDbFileName ).getAbsolutePath(), phrase, ((phrase == null) || (0 == phrase.length)) );
	Toast.makeText( qCtx, (0 <= count) ? "OKAY" : "ERROR", Toast.LENGTH_SHORT ).show();
	return true;
}

@Override
public File getFilesDir( String... parameters ) {
	return qCtx.getFilesDir();
}

@Override
public void log( String... aMsg ) {
	Log.d( aMsg[ 0 ], aMsg[ 1 ] );
}

@Override
public void toast( String msg ) {
	Toast.makeText( qCtx, msg, Toast.LENGTH_LONG ).show();
}

public int setCombinedPhrase( String pass ) {

	if (null == qCtx) {
		return -1;
	}
//	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
	if (null == zAccessCodeHex) {
//		String acd = SP.getString( "accessCodeDate", "" );
//		if (1 >= acd.length()) {
		return -1;
//		}
//		zAccessCodeHex = acd.substring( 0, acd.indexOf( '/' ) );
//		String day = acd.substring( zAccessCodeHex.length() + 1 );
//		if (!UtilMisc.getShortDay().equals( day )) {
//			Editor editor = SP.edit();
//			editor.putString( "accessCodeDate", "0" );
//			editor.commit();
//			return -1;
//		}
	}
//	pass = (null == pass) ? SP.getString( "imap_pass", "" ) : pass;
	if (null == pass) {
		if ((null != zPassImapHex) && (2 < zPassImapHex.length())) {
			return 0;
		}
		File file = new File( context.getFilesDir(), qPassFileName );
		try {
			byte[] dat = UtilMisc.readFile( file.getAbsolutePath(), 0 );
			if (null == dat) {
				return -1;
			}
			dat = Dib2Config.csvCodecs[ 0 ].decode( dat, 0, dat.length, zAccessCodeHex.getBytes(), null );
			String three = new String( dat );
			int offs = 1 + three.charAt( 0 );
			pass = three.substring( 1 + offs, 1 + offs + three.charAt( offs ) );
			offs += 1 + three.charAt( offs );
			if (2 > pass.length()) {
				return -1;
			}
			zPassImapHex = pass;
			zPassSmtpHex = three.substring( 1 + offs, 1 + offs + three.charAt( offs ) );
		} catch (Exception e) {
			return -1;
		}
	} else {
		pass = UtilMisc.toHexString( UtilMisc.toBytes( pass ) );
	}
	Dib2Config.log( "qdb setCombinedPhrase", "" ); // + pass.length() );
	if (2 >= pass.length()) {
		return -1;
	}
//	db.set( "imap_pass", SP.getString( "imap_pass", "" ) );
//	Editor editor = SP.edit();
//	editor.putString( "imap_pass", pass );
//	editor.putString( "smtp_pass", pass );
//	editor.commit();
	zPassImapHex = pass;
	zPassSmtpHex = (2 >= zPassSmtpHex.length()) ? pass : zPassSmtpHex;
	byte[] phrase = UtilMisc.toBytes( zAccessCodeHex + zPassImapHex ); //UtilMisc.toHexString( UtilMisc.toBytes( pass ) ) );
	setPhrase( phrase );
	return 0;
}

public int load4Sp( boolean useBackup ) {
//	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
//	++ zCountAttempts;
	int rc = super.load( useBackup );
	if (0 >= rc) {
		String name = fileReady(); // false );
		Dib2Config.log( "qdb load4Sp", name + " " + useBackup );
		if ((null == name) || !useBackup) {
			return rc;
		}
		rc = importFile( name, null, true );
		if (0 >= rc) {
			return rc;
		}
	}
//	byte[] log = preference_get( "log" );
//	UtilMisc.logBuffer.insert( 0, "\n=====\n" ).insert( 0, //,
//		((null == log) ? "" : new String( log )) );
//	set( "log", "" );
	String last = get( "lastId", "" );
	if (8 < last.length()) {
		UtilMisc.initLastId( last );
	}
//	Editor editor = SP.edit();
//	for (String pref : zPrefs.keySet()) {
//		if (null != get( pref, null )) {
//			editor.putString( pref, get( pref, "" ) );
//		}
//	}
//	editor.commit();
	return rc;
}

@Override
public int load( boolean useBackup ) {
	Dib2Config.log( "qdb load", "bak " + useBackup );
	if (0 > setCombinedPhrase( null )) {
		return -1;
	}
	return load4Sp( useBackup );
}

public int load() {
	return load( true ); // 1 <= zCountAttempts );
}

@Override
public String get( String pref, String defaultValue )
{
	byte[] out = preference_get( pref );
//	if ((null == out) && (qCtx != null)) {
//		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
//		String val = SP.getString( pref, null );
//		if (null != val) {
//			return val;
//		}
//	}
	out = pref.equals( "imap_pass" ) ? UtilMisc.fromHexString( zPassImapHex ) : out;
	out = pref.equals( "smtp_pass" ) ? UtilMisc.fromHexString( zPassSmtpHex ) : out;
	return (null == out) ? defaultValue : UtilMisc.toString( out );
}

//@Override
//public String get( String pref )
//{
//	return get( pref, null );
//}

@Override
public void preference_set( String key, byte[] value ) {
	if (!key.endsWith( "_pass" )) {
		super.preference_set( key, value );
	} else if (key.startsWith( "imap" )) {
		String pass = UtilMisc.toHexString( value );
		zPassImapHex = (2 < pass.length()) ? pass : zPassImapHex;
		byte[] phrase = UtilMisc.toBytes( zAccessCodeHex + zPassImapHex );
		setPhrase( phrase );
	} else if (key.startsWith( "smtp" )) {
		String pass = UtilMisc.toHexString( value );
		zPassSmtpHex = (2 < pass.length()) ? pass : zPassSmtpHex;
	}
}

public void preference_add( String key, byte[] value ) {
	preference_set( key, value );
}

@Override
public void set( String pref, String value )
{
	preference_set( pref, (null == value) ? null : UtilMisc.toBytes( value ) );
//	if ((qCtx != null) && !pref.startsWith( "KEY." )) {
//		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
//		Editor editor = SP.edit();
//		if (null == value) {
//			editor.remove( pref );
//		} else {
//			editor.putString( pref, value );
//		}
//		editor.commit();
//	}
}

public void setAccessCodeDate( byte[] accessCode ) {
	zAccessCodeHex = (null == accessCode) ? zAccessCodeHex : UtilMisc.toHexString( accessCode );
//	byte[] toSave = preference_get( "save_ac" );
//	toSave = ((null == toSave) || (0 >= toSave.length) || (0 == toSave[ 0 ])) ? null : toSave;
//	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
//	Editor editor = SP.edit();
//	if (null == toSave) {
//		editor.putString( "accessCodeDate", "0" );
//	} else {
//		editor.putString( "accessCodeDate", zAccessCodeHex + "/" + UtilMisc.getShortDay() );
//	}
//	editor.commit();
}

public boolean checkAccessCodeDate( byte[] accessCode ) {
	if ((null == accessCode) || (0 >= accessCode.length)) {
		return false;
	}
	return UtilMisc.toHexString( accessCode ).equals( zAccessCodeHex );
}

@Override
public synchronized void save() {
	String ms = "" + (UtilMisc.currentTimeMillisLinearized() & 0xffff);
	byte[] dat = ("" + ((char) ms.length()) + ms + ((char) zPassImapHex.length()) + zPassImapHex + ((char) zPassSmtpHex.length()) + zPassSmtpHex)
		.getBytes();
	try {
		dat = Dib2Config.csvCodecs[ 0 ].encode4DerivedKey( dat, 0, dat.length, zAccessCodeHex.getBytes() );
		File file = new File( context.getFilesDir(), qPassFileName );
		UtilMisc.writeFile( file.getAbsolutePath(), dat, 0, dat.length, null );
	} catch (Exception e) {
		Dib2Config.log( "db save", "error " + e );
	}
	super.save();
}

public void pref2db()
{
	final String[] keys = {
		"display_name",
		"email_address",
		"imap_user",
		"smtp_user",
		"imap_server",
		"imap_port",
		"smtp_server",
		"smtp_port",
	};
	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
	preference_set( "imap_pass", SP.getString( "imap_pass", "" ).getBytes() );
	preference_set( "smtp_pass", SP.getString( "smtp_pass", "" ).getBytes() );
	Editor editor = SP.edit();
	editor.putString( "imap_pass", "" );
	editor.putString( "smtp_pass", "" );
	editor.commit();
	for (String key : keys) {
		preference_set( key, SP.getString( key, "" ).getBytes() );
	}
}

public void db2pref()
{
	final String[] keys = {
		"display_name",
		"email_address",
		"imap_user",
		"smtp_user",
		"imap_server",
		"imap_port",
		"smtp_server",
		"smtp_port",
	};
	if (qCtx != null) {
		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( qCtx );
		Editor editor = SP.edit();
		for (String key : keys) {
			String value = get( key, null );
			if (null == value) {
				editor.remove( key );
			} else {
				editor.putString( key, value );
			}
		}
		editor.commit();
	}
}

//=====
}
