// Copyright (C) Roland Horsch and others:
// -- Changes:  Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See dibdib/assets/license.txt for detailed information.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android_joined.qm;

import android.content.*;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.*;
import android.text.Layout.Alignment;
import android.text.method.LinkMovementMethod;
import android.text.style.*;
import android.util.*;
import android.view.View;
import android.widget.*;
import java.io.*;
import java.util.*;
import localhost.optional.chat.Pgp;
import net.sourceforge.dibdib.all.util.UtilMisc;
import net.sourceforge.dibdib.all_joined.chat.Contact;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android.qm.*;
import net.sourceforge.dibdib.config.*;
import net.vreeken.quickmsg.*;

//Due to old naming of classes.
@SuppressWarnings( "static-access" )
public class MainActivity_1 extends net.vreeken.quickmsg.MainActivity_0 {
//=====

// DIFF (_0):
// protected quickmsg_db db = new quickmsg_db(this);
// protected Contact contact;
// protected long unreadTime = 0;
// m.time_set(-1);
// (onCreate:) unreadTime = contact.unread_get(); checkBackground();
// Pgp pgp_enc = new Pgp(db); //context);

//public static final int MAXVIEW_MSGS_ADD = 5;
int maxview = Dib2Constants.MAXVIEW_MSGS_INIT;

@Override
public void onUserLeaveHint() {
	super.onUserLeaveHint();
	contact.unread_set( UtilMisc.currentTimeMillisLinearized() - 2000 );
}

@Override
public void onUserInteraction() {
	super.onUserInteraction();
	unreadTime = contact.unread_get();
}

@Override
protected void onResume() {
	super.onResume();
	contact = (null == contact) ? db.contact_get_by_id( 0 ) : contact;
	unreadTime = contact.unread_get();
	if (unreadTime < contact.time_lastact_get()) {
		contact.unread_set( contact.time_lastact_get() );
	}
}

@Override
public void send_msg_attachment( final Context context, Uri uri ) {
	EditText new_msg = (EditText) findViewById( R.id.new_msg );
	String message_text = new_msg.getText().toString();

	if (0 < background.unread) {
		background.alarmDone = true;
	}

	if (message_text.length() < 1 && uri == null)
		return;

	maxview = Dib2Constants.MAXVIEW_MSGS_INIT;
//	long time_now = UtilMisc.currentTimeMillisLinearized();
	final message m = new message();
	m.id_set( contact.id_get() );
	m.from_set( 1 );
	m.time_set( -1 ); // time_now );
	m.text_set( message_text );
	if (uri != null) {
		m.uri_set( uri );
	}

	boolean local = (contact.id_get() <= 1) || !contact.address_get().contains( "@" );
	if (local) {
		db.message_add( m );
//		Intent intent = new Intent();
		local_message.send_statusMsg( context, "?" );
		new_msg.setText( "" );
		return;
	}

	if ((contact.type_get() == Contact.TYPE_PERSON) && ((Contact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()) == 0)) {
		message_text = "Not sent (MISSING KEY):\n" + message_text;
		m.text_set( message_text );
	}
	final attachment unenc;
	db.message_add( m );

	Dib2Config.log( "mainactivity send_msg", "send to " + contact.name_get() + "id: " + contact.id_get() );

	quickmsg qmsg = new quickmsg();
	unenc = qmsg.send_message( this, db.contact_get_by_id( 1 ), contact, m );
//	Dib2Config.log( "mainactivity send_msg", "got message unencrypted" );
	final List< String > to_adds;
	if (contact.type_get() == Contact.TYPE_GROUP) {
		to_adds = contact.members_get();
		if ((null == to_adds) || (0 == to_adds.size()))
			return;
	} else {
		to_adds = new LinkedList< String >();
		to_adds.add( contact.address_get() );
	}

	new Thread( new Runnable() {
		@Override
		public void run() {
			Pgp pgp_enc = new Pgp( db );

			attachment id = pgp_enc.pgpmime_id();

			for (int i = 0; i < to_adds.size(); i ++) {
				String to = to_adds.get( i );
				if (to.equals( pgp.my_user_id ))
					continue;
				if ((contact.type_get() == Contact.TYPE_PERSON) && ((Contact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()) == 0))
					continue;

				attachment enc;
				try {
					enc = pgp_enc.encrypt_sign( unenc, to );
				} catch (OutOfMemoryError e) {
					Log.e( "send_msg", "Out of memory during encryption, attachment to big?" );
					enc = null;
				} catch (Exception e) {
					Log.e( "send_msg", "pgp exception (missing key?...): " + e );
					enc = null;
				}
				if (enc == null) {
					if (!m.text_get().contains( "[Error" )) {
						m.text_set( m.text_get() + "\n[Error!]" );
						db.message_update( m );
					}
					continue;
				}
				enc.disposition = "inline";

				Dib2Config.log( "mainactivity send_msg", "got message encrypted" );
				List< attachment > attachments = new LinkedList< attachment >();
				attachments.add( id );
				attachments.add( enc );

				String queue = background.mail.send( context, to, attachments, "encrypted" );
				if (queue != null) {
					m.queue_set( queue );
					db.message_update( m );
					local_message.send_statusMsg( context, "?" );
				}
			}
			Dib2Config.log( "mainactivity send_msg", "mail.send done" );
		}
	} ).start();

	new_msg.setText( "" );
}

@Override
public void display_contact() {
	List< message > messages = db.message_get_by_id( contact.id_get(), maxview + 1 );
	Boolean unread = false;
	Boolean more;
	Spanned span = new SpannableString( "" );

	if (messages.size() > maxview) {
		messages.remove( 0 );
		more = true;
	} else {
		more = false;
	}

	final Context context = this;
	DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	final int maxw = metrics.widthPixels / 2 + 1;
	final int maxh = metrics.heightPixels / 2 + 1;
	if (unreadTime > contact.unread_get()) {
		// Delayed message/ ...
		unreadTime = contact.unread_get();
	}

	for (int i = 0; i < messages.size(); i ++) {
		String uri_html = null;
		message m = messages.get( i );
		Boolean from_me = (m.from_get() == 1);
		Contact from = db.contact_get_by_id( m.from_get() );
		if (from == null) {
			Dib2Config.log( "display contact", "from == null" + m.from_get() );
			from = new Contact();
		}

		long time_ms = m.time_get();
		if ((time_ms > unreadTime) && !unread) {
			unread = true;
			if (time_ms > contact.unread_get()) {
				background.unread = (0 == background.unread) ? -1 : background.unread;
//				if (from_me) {
//					unreadTime = time_ms;
//				}
			}
		}
		int hdr_color;
		if (m.from_get() == 1) {
			hdr_color = 0xff808080;
		} else {
			hdr_color = 0xff0050a0;
		}
		int bgcolor = 0xff80ff80; // 0xffd0e8ff;
		int bgcolorborder = 0xff008040; // 0xff66b2ff;
		if (from_me) {
			bgcolor = 0xff80ffff; // 0xffe0e0e0;
			bgcolorborder = 0xff008080; // 0xffa0a0a0;
		}

		String hdr_name = from.name_get();
		String hdr_date = UtilMisc.toShortDate4Millis( time_ms, -3 ) + "\n";
		int diff = findTimeDiff( hdr_date ) + hdr_name.length() + 1;
		if (0 != (time_ms & Dib2Constants.TIME_SHIFTED)) {
			if (0 != (time_ms & Dib2Constants.TIME_SHIFTED_UNKNOWN)) {
				hdr_date = hdr_date.substring( 0, hdr_date.length() - 4 ) + "???\n";
			}
			if (0 != (time_ms & Dib2Constants.TIME_SHIFTED_HOUR)) {
				hdr_date = hdr_date.substring( 0, hdr_date.length() - 6 ) + "??" + hdr_date.substring( hdr_date.length() - 4 );
			}
		}
		SpannableString span_hdr = new SpannableString( hdr_name + " " + hdr_date );
		span_hdr.setSpan( new StyleSpan( Typeface.ITALIC ),
			span_hdr.length() - hdr_date.length(), span_hdr.length(),
			Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		span_hdr.setSpan( new StyleSpan( Typeface.BOLD ),
			0, hdr_name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		span_hdr.setSpan( new StyleSpan( Typeface.BOLD ),
			diff, diff + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		if (from_me) {
			span_hdr.setSpan( new AlignmentSpan.Standard( Alignment.ALIGN_OPPOSITE ),
				0, span_hdr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		} else if (!unread) {
			span_hdr.setSpan( new ForegroundColorSpan( hdr_color ),
				0, span_hdr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		}
		Uri uri = getUri( m.uri_get() );
		if (uri == null) {
			if (m.text_get().startsWith( "::" )) {
				uri = getUri( m.text_get() );
			}
		} else {
//			Dib2Config.log("display contact", "uri: " + uri.toString());
			ContentResolver cR = getContentResolver();
			String fulltype;
			boolean uri_fault = false;
			try {
				fulltype = cR.getType( uri );
			} catch (IllegalStateException e) {
				Log.e( "display contact", "could not get full type" );
				fulltype = "unknown type";
				uri_fault = true;
			}
			boolean handled = false;
			String handled_string = "";

			if (fulltype != null) {
				String type = fulltype.split( "/" )[ 0 ].toLowerCase();
				Dib2Config.log( "display contact", "fulltype: " + fulltype + ", type: " + type );

				if (type.equals( "image" )) {
					handled_string = "<img src='image#" + uri.toString() + "'>";
					handled = true;
				}
				if (type.equals( "video" )) {
					handled_string = "<img src='video#" + uri.toString() + "'>";
					handled = true;
				}
				if (type.equals( "audio" )) {
					handled_string = "<img src='audio#" + uri.toString() + "'>";
					handled = true;
				}
			}
			Dib2Config.log( "display contact", "handled_string: " + handled_string );
			if (uri_fault) {
				handled = false;
			}
			if (handled) {
				uri_html = "<p>";
				Intent intent = new Intent( Intent.ACTION_VIEW );
				intent.setData( uri );
				List< ResolveInfo > ia = this.getPackageManager().queryIntentActivities( intent, 0 );
				if (ia.size() > 0) {
					uri_html += "<a href='" + uri.toString() +
						"'>" +
						(handled ? handled_string : uri.toString()) +
						"</a>";
				} else {
					uri_html += handled ? handled_string : uri.toString();
				}
				uri_html += "</p>";
			}
			Dib2Config.log( "display contact", "uri_html: " + uri_html );
		}

		Spannable span_msg = new SpannableString( m.text_get() );
		span_msg.setSpan( new BulletSpan( from_me ? 3 : 0, 0 ),
			0, span_msg.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		Spanned span_uri = null;
		if (uri_html != null) {
			span_uri = Html.fromHtml( uri_html, new Html.ImageGetter() {
				@Override
				public Drawable getDrawable( final String img_source ) {
					String source_type = img_source.split( "#" )[ 0 ];
					String source = img_source.split( "#" )[ 1 ];
					Dib2Config.log( "getdrawable", "img_source: " + img_source + "source_type: " + source_type + " source: "
						+ source );
					Bitmap bm;

					if (source_type.equals( "image" )) {
						BitmapFactory.Options options = new BitmapFactory.Options();

						InputStream is;
						try {
							is = getContentResolver().openInputStream( Uri.parse( source ) );
						} catch (FileNotFoundException e) {
							Dib2Config.log( "getDrawable", e.getMessage() );
							return null;
						}

						options.inJustDecodeBounds = true;
						BitmapFactory.decodeStream( is, null, options );
						int h = options.outHeight;
						int w = options.outWidth;

						int scaleh = (maxh + h) / maxh;
						int scalew = (maxw + w) / maxw;

						options.inSampleSize = Math.max( scaleh, scalew );
						options.inJustDecodeBounds = false;

						try {
							is = getContentResolver().openInputStream( Uri.parse( source ) );
						} catch (FileNotFoundException e) {
							Dib2Config.log( "getDrawable", e.getMessage() );
							return null;
						}

						bm = BitmapFactory.decodeStream( is, null, options );
					} else if (source_type.equals( "video" )) {
						String[] projection = { MediaStore.Video.Media._ID };
						Cursor cursor = getContentResolver().query( Uri.parse( source ), projection, null, null, null );

						int column_index = cursor
							.getColumnIndexOrThrow( MediaStore.Video.Media._ID );

						cursor.moveToFirst();
						long video_id = cursor.getLong( column_index );
						cursor.close();

						ContentResolver crThumb = getContentResolver();

						bm = MediaStore.Video.Thumbnails.getThumbnail( crThumb, video_id, MediaStore.Video.Thumbnails.MICRO_KIND, null );
					} else if (source_type.equals( "audio" )) {
						Dib2Config.log( "get drawable", "audio uri" );
						Drawable dp = getResources().getDrawable( R.drawable.play );
						if (dp == null) {
							return null;
						}
						dp.setBounds( 0, 0, dp.getIntrinsicWidth(), dp.getIntrinsicHeight() );
						return dp;
					} else {
						Dib2Config.log( "get drawable", "unknown source type: " + source_type );
						return null;
					}

					Drawable d = new BitmapDrawable( getResources(), bm );
					d.setBounds( 0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight() );

					return d;
				}
			}, null );
		}

		span_msg.setSpan( new line_background_span(
			(unread ? bgcolor : 0xffffffff),
			(unread ? bgcolorborder : 0xff113333), !from_me ),
			0, span_msg.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
		span = (Spanned) TextUtils.concat( span, span_hdr );
		span = (Spanned) TextUtils.concat( span, span_msg );
		if (uri != null) {
			final String content = "::" + uri.toString();
			Spannable spOpen = new SpannableString( "  OPEN" );
			spOpen.setSpan( new ClickableSpan() {
				@Override
				public void onClick( View widget ) {
					Dib2Config.log( "display span", "click" );
					openFile( content );
				}
			}, 0, spOpen.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
			span = (Spanned) TextUtils.concat( span, spOpen );
		}

		String between = "<br>";
		if (uri_html != null) {
			span = (Spanned) TextUtils.concat( span, span_uri );
			between = "<p><br></p>";
		}

		String queue = m.queue_get();

		if (queue != null) {
			if (background.mail.queue_check( this, queue )) {
				between = "<p><font color='#990000'> *** Still in mail queue</font></p>";
			} else {
				m.queue_set( null );
				db.message_update( m );
				;
			}
		}
		Spanned between_span = Html.fromHtml( between );
		span = (Spanned) TextUtils.concat( span, between_span );
	}
//	if (contact.time_lastact_get() > contact.unread_get()) {
//		contact.unread_set( contact.time_lastact_get() - 55000 );
//	}
	db.contact_update( contact );

	if (more) {
		Spannable span_more = new SpannableString( "view more messages\n\n" );
		span_more.setSpan( new ClickableSpan() {
			@Override
			public void onClick( View view ) {
//				maxview += MAXVIEW_MSGS_ADD;
				maxview = maxview * 2 + Dib2Constants.MAXVIEW_MSGS_INIT;
//            	Dib2Config.log("view contacts", "new maxview: " + maxview);
				display_contact();
			}
		}, 0, span_more.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE );
		span = (Spanned) TextUtils.concat( span_more, span );
	}
	msg_viewer.setText( span );
	msg_viewer.setBackgroundColor( 0xff77eeee ); // rho
	msg_viewer.getRootView().setBackgroundColor( 0xff77eeee );
	msg_viewer.setMovementMethod( LinkMovementMethod.getInstance() );

	final ScrollView scrollview = ((ScrollView) findViewById( R.id.scrollView1 ));
	scrollview.post( new Runnable() {
		@Override
		public void run() {
			if (maxview > Dib2Constants.MAXVIEW_MSGS_INIT)
				scrollview.fullScroll( ScrollView.FOCUS_UP );
			else
				scrollview.fullScroll( ScrollView.FOCUS_DOWN );
		}
	} );
}
//=====
}
