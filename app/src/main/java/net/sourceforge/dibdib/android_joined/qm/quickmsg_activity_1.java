// Copyright (C) Roland Horsch and others:
// -- Changes:  Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See dibdib/assets/license.txt for detailed information.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android_joined.qm;

import android.app.AlertDialog;
import android.content.*;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.*;
import android.text.InputType;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import java.util.*;
import localhost.optional.chat.Pgp;
import net.sourceforge.dibdib.all.util.UtilMisc;
import net.sourceforge.dibdib.all_joined.chat.Contact;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android.qm.*;
import net.sourceforge.dibdib.config.*;
import net.vreeken.quickmsg.*;

//Due to old naming of classes.
@SuppressWarnings( "static-access" )
public abstract class quickmsg_activity_1 extends quickmsg_activity_add {
// net.vreeken.quickmsg.quickmsg_activity_0 {
// quickmsg.java DIFF:
// msg += "Time: " + message.time_get() /1000 + "\n";
// contact.time_lastact_set();
// _msg.time_set(Long.parseLong(value) *1000);
//=====

// Redundant duplication, in order to keep old code:
public final quickmsg_db db = new preferences( this ); //new quickmsg_db( this );
protected preferences prefs = null; // new ...

public static volatile Pgp pgp = null;
public static volatile boolean isActive = false;
//public static final mail mail = new mail();
public static volatile String toast = null;

protected static long initialDialogPassed = 100;

public String version_get()
{
	String app_ver;
	try
	{
		app_ver = this.getPackageManager().getPackageInfo( this.getPackageName(), 0 ).versionName;
	} catch (NameNotFoundException e)
	{
		return "unknown";
	}
	return app_ver;
}

protected void checkBackground() {
	String offline = prefs.get( "offline", "" );
	char off = ((null == offline) || (0 >= offline.length())) ? 'T' : offline.charAt( 0 );
	background.setOffline( ('T' == off) ? 2 : (('F' == off) ? -1 : 0) );
	local_message.send_background( getApplicationContext(), 'T' != off );
}

@Override
protected void onCreate( Bundle savedInstanceState ) {
	super.onCreate( savedInstanceState );
//	if (null == prefs) { //Dib2Config.qContextQm) {
	prefs = (preferences) db;
	Dib2Config.init( 'A', db );
//	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( this.getApplication() );
//	String last = SP.getString( "lastId", "" );
//	if (8 < last.length()) {
//		UtilMisc.initLastId( last );
//	}
	isActive = true;
}

@Override
protected void onPause() {
	if (isActive) {
		// onUserLeaveHint() had not been called.
		initialDialogPassed = UtilMisc.currentTimeMillisLinearized();
	}
	isActive = false;
	local_message.zUiHandler = null;
	local_message.zUiActivity = null;
	if (null == pgp) {
		super.onPause();
		return;
	}
//	SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences( this.getApplication() );
//	String acd = SP.getString( "accessCodeDate", "" );
//	Editor editor = SP.edit();
//	editor.putString( "lastId", UtilMisc.qLastId );
	db.set( "lastId", UtilMisc.qLastId );
//	if (1 >= acd.length()) {
//		editor.commit();
//	} else {
//		String ac = acd.substring( 0, acd.indexOf( '/' ) );
//		String day = acd.substring( ac.length() + 1 );
//		byte[] toSave = db.preference_get( "save_ac" );
//		toSave = ((null == toSave) || (0 >= toSave.length) || (0 == toSave[ 0 ])) ? null : toSave;
//		if ((null == toSave) || !UtilMisc.getShortDay().equals( day )) {
//			editor.putString( "accessCodeDate", "0" );
//			editor.commit();
//		}
//		db.preference_set( "log", UtilMisc.toBytes( UtilMisc.logBuffer.toString() ) );
//	}
	db.save();
//	Dib2Config.log( "qm", "pause" );
	background.setIdleAlarm();
	super.onPause();
}

@Override
public void onUserLeaveHint() {
	isActive = false;
}

@Override
public void onUserInteraction() {
	// check for unread ...
}

@Override
protected void onResume() {
	super.onResume();
	local_message.zUiActivity = this;
	local_message.zUiHandler = new Handler();
	Dib2Config.log( "qm", "resume from " + isActive + initialDialogPassed );
	isActive = true;
	background.unread = (0 == background.unread) ? -1 : background.unread;
	if ((initialDialogPassed + Dib2Constants.MAX_DELTA_ACCESS_CHECK) < UtilMisc.currentTimeMillisLinearized()) {
		initialDialogPassed = (100 <= initialDialogPassed) ? 100 : initialDialogPassed;
	} else {
		local_message.send_statusMsg( getApplicationContext(), "?" );
		checkBackground();
	}
}

protected int findTimeDiff( String shortDate ) {
	String cmp = UtilMisc.toShortDate4Millis( UtilMisc.currentTimeMillisLinearized(), -3 );
	int diff = cmp.length() - 5;
	if (!cmp.substring( 0, 4 ).equals( shortDate.substring( 0, 4 ) )) {
		diff = 2;
	} else if (!cmp.substring( 4, 7 ).equals( shortDate.substring( 4, 7 ) )) {
		diff = 4;
	} else if (cmp.substring( 0, diff + 2 ).equals( shortDate.substring( 0, diff + 2 ) )) {
		diff = cmp.length() - 2;
	}
	return diff;
}

public void send_key( Contact contact )
{
	final Context c = this;
	final Contact sendcontact = contact;

	new Thread( new Runnable() {
		@Override
		public void run() {
			attachment attachment = pgp.key_attachment( pgp.my_user_id );

			List< attachment > attachments = new LinkedList< attachment >();
			attachments.add( attachment );

			if (null == background.mail.send( c, sendcontact.address_get(), attachments )) {
				Dib2Config.log( "qmAct send_key", "done" );
			} else {
				Dib2Config.log( "qmAct send_key", "key ready" );
			}
			switch (sendcontact.keystat_get()) {
				case Contact.KEYSTAT_NONE:
					sendcontact.keystat_set( Contact.KEYSTAT_SENT );
					break;
				case Contact.KEYSTAT_PENDING:
					sendcontact.keystat_set( Contact.KEYSTAT_VERIFIED );
					break;
				case Contact.KEYSTAT_RECEIVED: // had been verified
					sendcontact.keystat_set( Contact.KEYSTAT_PENDING );
					break;
				default:
					;
			}
		}
	} ).start();
}

void contact_view( Contact c )
{
	Intent intent = new Intent( this, MainActivity.class );
	intent.putExtra( "id", c.id_get() );
	startActivity( intent );
}

protected void send_key_dialog( final Contact c )
{
	AlertDialog.Builder alert = new AlertDialog.Builder( this );

	alert.setTitle( R.string.action_send_key );
	String fi = pgp.fingerprint( db.contact_get_by_id( 1 ).address_get() );
	if (null == fi) {
		Dib2Config.log( "qmAct send key dialog", "pgp missing" );
	}
	alert.setMessage( getString( R.string.dialog_send_key ) + " (Fi-Code: " + fi.substring( 0, 3 ) + ".." + fi.substring( fi.length() - 3 )
		+ ')' );
	alert.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
			Dib2Config.log( "qmAct send key dialog", "(re-)send key" );
			if (1 < c.id_get()) {
				switch (c.keystat_get()) {
					case Contact.KEYSTAT_SENT:
						// other key might have changed:
						pgp.public_keyring_remove_by_address( c.address_get() );
						// temporarily switch color on UI as feedback:
						c.keystat_set( Contact.KEYSTAT_NONE );
						break;
					case Contact.KEYSTAT_VERIFIED:
					case Contact.KEYSTAT_CONFIRMED:
//						pgp.public_keyring_remove_by_address( c.address_get() );
						// initial attempt of (re-)sending own key, keeping other key:
						c.keystat_set( Contact.KEYSTAT_RECEIVED );
						break;
					case Contact.KEYSTAT_PENDING:
						break;
					default:
						c.keystat_set( Contact.KEYSTAT_SENT );
						local_message.send_statusMsg( getApplicationContext(), "" );
				}
				send_key( c );
			}
		}
	} );
	alert.setNegativeButton( "Stop", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
		}
	} );
	alert.setCancelable( true );
	alert.show();
}

protected void view_fingerprint_dialog( Contact c )
{
	AlertDialog.Builder alert = new AlertDialog.Builder( this );

	alert.setTitle( R.string.action_fingerprint );
	alert.setMessage( getString( R.string.dialog_fingerprint ) + " " +
		c.name_get() + " " + c.address_get() + ": " +
		pgp.fingerprint( c.address_get() ) );
	alert.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
		}
	} );
	alert.show();
}

protected void verify_dialog( final Contact c )
{
	AlertDialog.Builder alert = new AlertDialog.Builder( this );

	alert.setTitle( R.string.action_verify );
	alert.setMessage( getString( R.string.dialog_verify ) + ": " +
		pgp.fingerprint( c.address_get() ) );
	alert.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {

			Dib2Config.log( "qmAct verify_dialog", "verified contact" );
			if (Contact.KEYSTAT_RECEIVED == c.keystat_get()) {
				c.keystat_set( Contact.KEYSTAT_PENDING );
			}
			send_key( c );
			db.contact_update( c );
			update_ui();
		}
	} );
	alert.setNegativeButton( "Stop", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
		}
	} );
	alert.setCancelable( true );
	alert.show();
}

protected void warn_remove_self()
{
	AlertDialog.Builder alert = new AlertDialog.Builder( this );

	alert.setTitle( R.string.action_remove_self );
	alert.setMessage( getString( R.string.dialog_remove_self ) );
	alert.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
		}
	} );
	alert.setCancelable( true );
	alert.show();
}

public void group_update( Contact c )
{
	db.contact_update( c );

	quickmsg qm = new quickmsg();
	attachment qma = qm.send_group( c, pgp, db );

	List< String > to_adds = c.members_get();
	Dib2Config.log( "qmAct group_update", "mem " + to_adds );

	attachment id = pgp.pgpmime_id();

	for (int i = 0; i < to_adds.size(); i ++) {
		String to = to_adds.get( i );
//		Contact cx = db.contact_get_person_by_address( to );
		if (to.equals( pgp.my_user_id ))
			continue;

		attachment enc = pgp.encrypt_sign( qma, to );
		enc.disposition = "inline";

		Dib2Config.log( "qmAct group_update", "message encrypted for: " + to );
//		mail mail = new mail();
		List< attachment > attachments = new LinkedList< attachment >();
		attachments.add( id );
		attachments.add( enc );

		background.mail.send( false, this, to, attachments, "encrypted" );
	}
	Dib2Config.log( "qmAct group_update", "mail.send done" );

	update_ui();
}

protected void set_member_dialog( final Contact c )
{
//	if (!c.address_get().equals( pgp.my_user_id )) {
//		Dib2Config.log( "qmAct set_member_dialog", "not my group" );
//		return;
//	}

	final List< String > old_members = c.members_get();
	final List< Contact > persons = db.contact_get_by_type( Contact.TYPE_PERSON );
	final List< CharSequence > names = new ArrayList< CharSequence >();
	final boolean[] checked = new boolean[ persons.size() ];
	final int[] indexed = new int[ persons.size() ];
	final String me = db.contact_get_by_id( 1 ).address_get();
	int cnt = 0;
	names.add( "=/ " + me );
	checked[ cnt ++ ] = true;
	for (int i = 0; i < persons.size(); i ++) {
		Contact p = persons.get( i );
		if (1 >= p.id_get()) {
			indexed[ 0 ] = (1 == p.id_get()) ? i : indexed[ 0 ];
			continue;
		}
		String addr = p.address_get();
		String name = p.name_get() + "/ " + addr;
		boolean check = false;
		for (int j = 0; j < old_members.size(); j ++) {
			if (addr.equals( old_members.get( j ) )) {
				check = true;
			}
		}
		if (!check) {
			if ((Contact.KEYSTAT_BIT_ACTIVE & p.keystat_get()) == 0) {
				continue;
			}
		}
		names.add( name );
		checked[ cnt ] = check;
		indexed[ cnt ++ ] = i;
	}

	AlertDialog.Builder builder = new AlertDialog.Builder( this );
	// Set the dialog title
	builder.setTitle( R.string.action_set_member );
	// Specify the list array, the items to be selected by default (null for none),
	// and the listener through which to receive callbacks when items are selected
	builder.setMultiChoiceItems(
		names.toArray( new CharSequence[ names.size() ] ),
		checked,
		new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which,
				boolean isChecked ) {
				checked[ which ] = isChecked;
			}
		} );
	// Set the action buttons
	builder.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int id ) {
			List< String > members = new LinkedList< String >();
			for (int i = 0; i < names.size(); i ++) {
				/* add all members */
				if (checked[ i ]) {
//					if ((Contact.KEYSTAT_BIT_ACTIVE & persons.get( i ).keystat_get()) != 0)
					members.add( persons.get( indexed[ i ] ).address_get() );
				}
			}
			if (!members.contains( me ) && (1 >= old_members.size()) && old_members.contains( me )) {
				members.add( me );
			}
			c.members_set( members );
			group_update( c );
		}
	} );

	builder.setNegativeButton( "Stop", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int id ) {
			return;
		}
	} );
	builder.show();

}

protected void group_name_dialog( final Contact c )
{
//	if (!c.address_get().equals( pgp.my_user_id )) {
//		Dib2Config.log( "qmAct set_name_dialog", "not my group" );
//		return;
//	}

	AlertDialog.Builder alert = new AlertDialog.Builder( this );

	alert.setTitle( R.string.action_group_name );
	alert.setMessage( R.string.dialog_group_name );

	// Set an EditText view to get user input 
	final EditText input = new EditText( this );
	input.setInputType( InputType.TYPE_CLASS_TEXT );
	alert.setView( input );

	alert.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
			String value = input.getText().toString();

			c.name_set( value );

			group_update( c );
		}
	} );

	alert.setNegativeButton( "Stop", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
		}
	} );

	alert.show();

}

protected void remove( final Contact c )
{
	AlertDialog.Builder alert = new AlertDialog.Builder( this );

	alert.setTitle( R.string.action_remove );
	alert.setMessage( getString( R.string.dialog_remove ) + ": " +
		c.name_get() );
	alert.setPositiveButton( "Ok", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
			String address = c.address_get();

			if (c.id_get() == 1) {
				warn_remove_self();
				return;
			}
			Dib2Config.log( "qmAct remove", "remove contact" );
			if (c.id_get() <= 0) {
				db.message_remove_all_by_contact( 0 );
			} else if (c.type_get() == Contact.TYPE_PERSON) {
				List< Contact > contacts = db.contact_get_by_address( address );
				for (int i = 0; i < contacts.size(); i ++)
					db.contact_remove( contacts.get( i ) );
				if (!address.equals( pgp.my_user_id )) {
					pgp.public_keyring_remove_by_address( address );
				}
			} else {
				db.contact_remove( c );
			}

			update_ui();
		}
	} );
	alert.setNegativeButton( "Stop", new DialogInterface.OnClickListener() {
		@Override
		public void onClick( DialogInterface dialog, int whichButton ) {
		}
	} );
	alert.setCancelable( true );
	alert.show();
}

protected OnItemClickListener list_OnItemClickListener = new OnItemClickListener() {

	@Override
	public void onItemClick( AdapterView< ? > arg0, View view, int position,
		long id ) {
		final List< Contact > contacts = db.contact_get_all();
		final Contact c = contacts.get( position );

		if (c.type_get() == Contact.TYPE_GROUP) {
			contact_view( c );
		} else if (c.type_get() == Contact.TYPE_PERSON) {
			switch (c.keystat_get()) {
				case Contact.KEYSTAT_SENT:
				case Contact.KEYSTAT_NONE: {
					Dib2Config.log( "qmAct onItemClick", "contact ask action" );
					AlertDialog.Builder builder = new AlertDialog.Builder( view.getContext() );
					CharSequence[] items = {
						getString( R.string.action_send_key ),
						getString( R.string.action_remove ),
						getString( R.string.action_edit_contact ),
						getString( R.string.action_contact_dial ),
						getString( R.string.action_force ) };
					builder.setTitle( R.string.dialog_contact )
						.setItems( items, new DialogInterface.OnClickListener() {
							@Override
							public void onClick( DialogInterface dialog, int which ) {
								Dib2Config.log( "qmAct onItemClick", "contact which: " + which );
								if (which == 0) {
									Dib2Config.log( "qmAct onItemClick", "contact send_key_dialg(c)" );
									send_key_dialog( c );
								}
								if (which == 1) {
									Dib2Config.log( "qmAct onItemClick", "contact remove(c)" );
									remove( c );
								} else if (which == 2) {
									edit_contact_dialog( c );
								} else if (which == 3) {
									contact_dial( c );
								} else if (which == 4) {
									contact_view( c );
								}
							}
						} );
					builder.show();
					break;
				}
				case Contact.KEYSTAT_RECEIVED: {
					Dib2Config.log( "qmAct onItemClick", "contact ask action" );
					AlertDialog.Builder builder = new AlertDialog.Builder( view.getContext() );
					CharSequence[] items = {
						getString( R.string.action_verify ),
						getString( R.string.action_send_key ),
						getString( R.string.action_remove ) };
					builder.setTitle( R.string.dialog_contact )
						.setItems( items, new DialogInterface.OnClickListener() {
							@Override
							public void onClick( DialogInterface dialog, int which ) {
								Dib2Config.log( "qmAct onItemClick", "contact which: " + which );
								if (which == 0) {
									Dib2Config.log( "qmAct onItemClick", "contact verify_dialog(c)" );
									verify_dialog( c );
								}
								if (which == 1) {
									Dib2Config.log( "qmAct onItemClick", "contact send_key_dialg(c)" );
									send_key_dialog( c );
								}
								if (which == 2) {
									Dib2Config.log( "qmAct onItemClick", "contact remove(c)" );
									remove( c );
								}
							}
						} );
					builder.show();
					break;
				}
				case Contact.KEYSTAT_PENDING: {
					send_key( c );
					contact_view( c );
					break;
				}
//				case Contact.KEYSTAT_VERIFIED/....: {
				default: {
					contact_view( c );
				}
			}
		}
	}
};

protected OnItemLongClickListener list_OnItemLongClickListener = new OnItemLongClickListener() {

	@Override
	public boolean onItemLongClick( AdapterView< ? > arg0, View view,
		int position, long id ) {
		final List< Contact > contacts = db.contact_get_all();
		final Contact c = contacts.get( position );

		if (c.type_get() == Contact.TYPE_GROUP) {
			Dib2Config.log( "qmAct onItemLong", "contact ask action" );
			AlertDialog.Builder builder = new AlertDialog.Builder( view.getContext() );
			CharSequence[] items = {
				getString( R.string.action_set_member ),
				getString( R.string.action_set_name ),
				getString( R.string.action_remove )
			};
			builder.setTitle( R.string.dialog_contact )
				.setItems( items, new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialog, int which ) {
						Dib2Config.log( "qmAct onItemLong", "contact which: " + which );
						if (which == 0) {
							Dib2Config.log( "qmAct onItemLong", "contact group set member" );
							set_member_dialog( c );
						}
						if (which == 1) {
							Dib2Config.log( "qmAct onItemLong", "contact group set name" );
							group_name_dialog( c );
						}
						if (which == 2) {
							Dib2Config.log( "qmAct onItemLong", "contact group remove(c)" );
							remove( c );
						}
					}
				} );
			builder.show();
			return true;

		}
		if (c.type_get() == Contact.TYPE_PERSON) {
//		switch (c.keystat_get()) {
//			case Contact.KEYSTAT_VERIFIED: {
			Dib2Config.log( "qmAct onItemLong", "contact ask action" );
			AlertDialog.Builder builder = new AlertDialog.Builder( view.getContext() );
			CharSequence[] items = {
				getString( R.string.action_send_key ),
				getString( R.string.action_fingerprint ),
				getString( R.string.action_remove ),
				getString( R.string.action_edit_contact ),
				getString( R.string.action_contact_dial )
			};
			builder.setTitle( R.string.dialog_contact )
				.setItems( items, new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialog, int which ) {
						Dib2Config.log( "qmAct onItemLong", "contact which: " + which );
						if (which == 0) {
							Dib2Config.log( "qmAct onItemLong", "contact send_key_dialg(c)" );
							send_key_dialog( c );
						} else if (which == 1) {
							view_fingerprint_dialog( c );
						} else if (which == 2) {
							Dib2Config.log( "qmAct onItemLong", "contact remove(c)" );
							remove( c );
						} else if (which == 3) {
							Dib2Config.log( "qmAct onItemLong", "contact edit(c)" );
							edit_contact_dialog( c );
						} else if (which == 4) {
							contact_dial( c );
						}
					}
				} );
			builder.show();
			return true;
		}
		return false;
	}
};

private Uri zLastUri;
private String zLastPath;

protected void openFile( String content ) {
	if (!content.startsWith( "::" )) {
		//!content.startsWith( "::content" ) && !content.startsWith( "::application" )) {
		Dib2Config.log( "openFile", "Unexpected content: " + content );
		return;
	}
	Intent sendIntent = new Intent( "android.intent.action.MAIN" );
	sendIntent.setAction( Intent.ACTION_VIEW );
	Uri uri = //Uri.fromFile( null ); //file in download area
	//Uri.parse( content.substring( 2 ).replace( "::", ":" ) );
	getUri( content );
	if (null == uri) {
		return;
	}
	if (content.startsWith( "::application" )) {
		sendIntent.setType( "application/vnd.ms-word" );
	}
	sendIntent.setData( uri );
//	sendIntent.putExtra( Intent.EXTRA_STREAM, uri );
	try {
		startActivity( sendIntent );
	} catch (Exception e) {

	}
}

protected Uri getUri( Object uriOrDescr ) {
	if ((uriOrDescr == null) || (uriOrDescr instanceof Uri)) {
		return (Uri) uriOrDescr;
	}
	if (!(uriOrDescr instanceof String)) {
		return null;
	}
	String descr = (String) uriOrDescr;
	if (!descr.startsWith( "::" )) {
		return null;
	}
	if (0 < descr.indexOf( '\n' )) {
		descr = descr.substring( 0, descr.indexOf( '\n' ) );
	}
	try {
		if (descr.startsWith( "::content" )) {
			return Uri.parse( descr.substring( 2 ).replace( "::", ":" ) );
		}
		String path = descr.substring( descr.lastIndexOf( "::" ) + 2 );
		if (path.equals( zLastPath )) {
			return zLastUri;
		}
		MediaScannerConnection.scanFile( this,
			new String[] { path }, null,
			new MediaScannerConnection.OnScanCompletedListener() {
				@Override
				public void onScanCompleted( String path, Uri uri ) {
					zLastPath = path;
					zLastUri = uri;
				}
			} );
		Thread.sleep( 50 );
		if (path.equals( zLastPath )) {
			return zLastUri;
		}
	} catch (Exception e) { // NOP
	}
	return null;
}

//=====
}
