// Copyright (C) Roland Horsch and others:
// -- Changes:  Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See dibdib/assets/license.txt for detailed information.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android_joined.qm;

import android.app.*;
import android.content.*;
import android.graphics.BitmapFactory;
import android.media.*;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.util.Log;
import android.webkit.MimeTypeMap;
import java.io.*;
import java.util.*;
import javax.mail.internet.ContentType;
import localhost.optional.chat.Pgp;
import net.sourceforge.dibdib.all.util.UtilMisc;
import net.sourceforge.dibdib.all_joined.chat.Contact;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android.qm.*;
import net.sourceforge.dibdib.config.*;
import net.vreeken.quickmsg.*;

public class background_1 extends Service { // net.vreeken.quickmsg.background_0 {
//=====

public static final int MIN_IDLE_TIME = 11 * 1000;
public static final int STD_IDLE_TIME = 4 * 60 * 1000;
public static final int MAX_IDLE_TIME = 14 * 60 * 1000;

protected static background_1 svcContext = null;
protected preferences prefs = null;

public static volatile int unread;
protected static volatile boolean alarmDone = false;
public static volatile long flagsNotify = 3; // 1: LED, 2: vibrate, 4: sound
protected static volatile int idleTimeNet = MIN_IDLE_TIME;

protected Notification.Builder notification = null;
protected final int notification_id = 0x1337;
protected static quickmsg_db db = null;

public static background_1 getSvcContext() {
	return svcContext;
}

public static mail mail = new mail() {
	@Override
	public Boolean recv_quickmsg_cb( String from, List< attachment > attachments, String subtype )
	{
		Dib2Config.log( "bg recv cb", "Received quickmsg from " + from + " of subtype: " + subtype );
		background.unread = (0 > background.unread) ? 0 : background.unread;

		Pgp pgp = new Pgp( db ); //svcContext );

		for (int i = 0; i < attachments.size(); i ++) {
			attachment attachment = attachments.get( i );
//			Dib2Config.log( "bg recv cb", "name: " + attachment.name +
//				", content type :" + attachment.datahandler.getContentType() );
			try {
				ContentType ct = new ContentType( attachment.datahandler.getContentType() );
				if (subtype.toLowerCase().equals( "encrypted" ) &&
					ct.getSubType().toLowerCase().equals( "octet-stream" )) {
//					Dib2Config.log( "bg recv cb", "Got encrypted message" );
					attachment ac;
					try {
						ac = pgp.decrypt_verify( attachment );
					} catch (OutOfMemoryError e) {
						Log.e( "recv_cb", "out of memory: attachment to big?" );
						return false;
					}
					if (ac != null) {
						Dib2Config.log( "bg recv cb", "Got decrypted message - " + attachment.name );
						List< attachment > as = multipart_get_attachments( ac );
//						Dib2Config.log( "bg recv cb", "Got decrypted message attachments" );

						attachment a_msg = null;
						message msg = null;
						String a_ext = ".dat";

						for (int j = 0; j < as.size(); j ++) {
							attachment a = as.get( j );
							ContentType ct2 = new ContentType( a.datahandler.getContentType() );
							String subtype2 = ct2.getSubType();
							String basetype = ct2.getBaseType();
//							Dib2Config.log( "bg recv db", "attachment " + j + " " + basetype );

							if (subtype2.toLowerCase().equals( "pgp-keys" )) {
								svcContext.received_key( pgp, db, from, a, true );
							} else if (subtype2.toLowerCase().equals( "quickmsg" )) {
								msg = svcContext.received_quickmsg( pgp, db, from, a );
							} else {
								/* maybe part of a message */
								String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType( basetype );

								Dib2Config.log( "bg recv db", "probably an attachment ext: " + ext );
								a_msg = a;
								a_ext = ext;
							}
						}

						if (msg != null && a_msg != null) {
							svcContext.received_attachment( db, msg, a_msg, a_ext );
						}
						if (msg != null) {
							db.message_add( msg );
						}

					} else {
//						Dib2Config.log( "bg recv cb", "Message could not be decrypted/verified" );
						Contact contact = db.contact_get_person_by_address( from );
						if (contact != null) {
							contact.keystat_set( Contact.KEYSTAT_SENT );
						}
						local_message.send_statusMsg( svcContext, "?Key error - " + from );
					}
				}
				if (ct.getSubType().toLowerCase().equals( "pgp-keys" )) {
					svcContext.received_key( pgp, db, from, attachment, false );
				}
			}
			catch (Exception e) {
				Log.e( "recv_cb", "" + e + e.getMessage() );
			}

		}
		return true;
	}
};

@Override
public IBinder onBind( Intent intent )
{
	return null;
}

@Override
public void onCreate()
{
//	svcContext = this;
//	db = new quickmsg_db( svcContext );
	Dib2Config.log( "background_1", "onCreate" );
}

public message received_quickmsg( Pgp pgp, quickmsg_db db, String from, attachment a )
{

	quickmsg qm = new quickmsg();
	qm.parse_attachment( a );
//	Dib2Config.log( "bg recv db", "got quickmsg" );
//	idleTimeNet = MIN_IDLE_TIME;

	Contact cdb, cdbf;
	Contact cf = qm.get_contact();
	message mf = qm.get_message();
	int group = qm.get_group();

	cdbf = db.contact_get_person_by_address( from );
//	Dib2Config.log( "bg recv db", "got quickmsg " + mf.time_get() + '/' + cdbf.unread_get() );
	mf.time_set( UtilMisc.alignTime( mf.time_get(), cdbf.unread_get() ) );
	if (mf.time_get() <= (cdbf.unread_get() + Dib2Constants.TIME_SHIFTED)) {
		cdbf.unread_set( mf.time_get() - Dib2Constants.TIME_SHIFTED );
	}
	if ((null == cdbf) || (Contact.KEYSTAT_BIT_ACTIVE & cdbf.keystat_get()) == 0) {
		local_message.send_statusMsg( svcContext, "?Chat-error - re-send key to " + from );
		return null;
	}

	if (qm.is_group || qm.is_grouppost) {
		cdb = db.contact_get_group_by_address_and_group( qm.group_owner, group );
		if (cdb == null) {
			if (qm.group_owner.length() == 0)
				qm.group_owner = from;
			cdb = new Contact();
			cdb.type_set( Contact.TYPE_GROUP );
			cdb.address_set( qm.group_owner );
			cdb.group_set( qm.group_id );
			cdb.time_lastact_set( (null != mf) ? mf.time_get() //,
				: ((null != cf) ? cf.time_lastact_get() : -1) );
			cdb.name_set( "group" + group + "..." );
			cdb.members_set( qm.group_owner );
			db.contact_add( cdb );
			cdb = db.contact_get_group_by_address_and_group( qm.group_owner, group );
		}
	} else {
		cdb = cdbf;
	}
	if (qm.is_post && mf != null) {
//		Dib2Config.log( "bg recv cb", "Has a post, add to db" );
		mf.id_set( cdb.id_get() );
		mf.from_set( cdbf.id_get() );
//		Dib2Config.log( "bg recv cb", "id: " + cdb.id_get() );
		if (cdb.id_get() == 0) {
			Dib2Config.log( "bg recv cb", "Not a valid id" );
			return null;
		}
		List< String > members = cdb.members_get();
		if ((qm.is_grouppost) && (members != null)) {
			boolean foundmember = false;

			for (int i = 0; i < members.size(); i ++) {
				if (members.get( i ).equals( from )) {
					foundmember = true;
					break;
				}
			}
			if (!foundmember) {
//				Dib2Config.log( "bg msg received", "Sender is not a group member" );
//				return null;
				members.add( from );
			}
		} else if ((null == cdbf.name_get()) || (0 >= cdbf.name_get().length()) || (0 <= cdbf.name_get().indexOf( '@' )))
			cdbf.name_set( cf.name_get() );
		cdb.time_lastact_set( mf.time_get() );
//		doAlarm = true;
//		pollthread.interrupt();
	}
	if (qm.is_group) {
		cdb.time_lastact_set( cf.time_lastact_get() );
//			if (!from.equals(qm.group_owner)) {
//				Log.e("recv cb", "Only owner can modify group");
//				return null;
//			}
		List< String > members = cf.members_get();
		List< String > mem0 = cdb.members_get();
		boolean foundSender = false;
		for (int i = 0; i < members.size(); i ++) {
			if (!mem0.contains( members.get( i ) )) {
				mem0.add( members.get( i ) );
			}
			if (members.get( i ).equals( from )) {
				foundSender = true;
			}
		}

//		cdb.members_set( cf.members_get() );
		if (!foundSender) {
			// You may remove yourself.
			mem0.remove( from );
		}

		if (mem0.size() == 0) {
			Dib2Config.log( "bg recv cb", "group has no members" ); // , delete it" );
//			db.contact_remove( cdb );
			return null;
		}
		cdb.name_set( cf.name_get() );
		for (String mem : mem0) {
			if (null == db.contact_get_person_by_address( mem )) {
				Contact add = new Contact();
				add.name_set( mem );
				add.address_set( mem );
				add.type_set( Contact.TYPE_PERSON );
				db.contact_add( add );
			}
		}
	}

	db.contact_update( cdb );
	db.contact_update( cdbf );

	return mf;
}

public void received_key( Pgp pgp, quickmsg_db db, String from, attachment a, Boolean signed )
{
	String add;
	Boolean signedOk = false;
	Dib2Config.log( "bg recv cb", "Received pgp key attachement" );

	byte[] is;
	ByteArrayOutputStream buf = new ByteArrayOutputStream();
	try {
//		is = a.datahandler.getInputStream();
		a.datahandler.writeTo( buf );
		buf.flush();
		is = buf.toByteArray();
	} catch (IOException e) {
		Dib2Config.log( "bg recv cb", e.getMessage() );
		//e.printStackTrace();
		return;
	}
	//Dib2Config.log("recv cb", is.toString());
	String[] getter = new String[] { from }; // 1 ];
	boolean added = pgp.public_keyring_add_key( is, getter );
	add = getter[ 0 ];

	if (add != null) {
		Contact contact = db.contact_get_person_by_address( from );
		if (contact != null)
			if ((Contact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()) != 0)
			{
				signedOk = signed;
			}

		Dib2Config.log( "bg recv cb", "adding key to ring was successfull" );
		contact = db.contact_get_person_by_address( add );
		if ((contact == null) || (0 == (Contact.KEYSTAT_BIT_ACTIVE & contact.keystat_get()))) {
			Dib2Config.log( "bg recv_cb", "new key, add contact" );
			if (null == contact) {
				contact = new Contact();
			}
			contact.address_set( add );
			contact.type_set( Contact.TYPE_PERSON );
			contact.time_lastact_set(); // time_now );
			//if (!signedOk)
			contact.keystat_set( Contact.KEYSTAT_RECEIVED );
			//else
			//contact.keystat_set(contact.KEYSTAT_VERIFIED);

			db.contact_add( contact );
			local_message.send_statusMsg( svcContext, "" );
			return;
		}
		if (1 >= contact.id_get()) {
			return;
		}
		contact.time_lastact_set();
		if (added && add.equals( from )) {
			contact.keystat_set( Contact.KEYSTAT_RECEIVED );
			return;
		}
		if (add.equals( from )) {
			String oldfp = pgp.fingerprint( add );
			try {
				int old = contact.keystat_get(); // : Contact.KEYSTAT_CONFLICT;
				contact.keystat_set( 0 );
				pgp.public_keyring_remove_by_address( add );
				is = buf.toByteArray();
				pgp.public_keyring_add_key( is, getter );
				if (null != getter[ 0 ]) {
//					if ((Contact.KEYSTAT_CONFLICT == old) && pgp.fingerprint( add ).equals( oldfp )) {
//						old = Contact.KEYSTAT_VERIFIED;
//					}
					contact.keystat_set( old );
				}
			} catch (Exception e) {
				Dib2Config.log( "bg recv_db", "key change failed " + e );
				oldfp = null;
			}
			Dib2Config.log( "bg recv_db", "Existing contact, update if needed " + contact.keystat_get() );
//			long time_now = UtilMisc.currentTimeMillisLinearized();
//			contact.time_lastact_set(); // time_now );
			if (!pgp.fingerprint( add ).equals( oldfp )) {
				contact.keystat_set( Contact.KEYSTAT_RECEIVED );
			} else {
				switch (contact.keystat_get()) {
					case Contact.KEYSTAT_RECEIVED:
						break;
					case Contact.KEYSTAT_VERIFIED:
						contact.keystat_set( Contact.KEYSTAT_CONFIRMED );
						break;
					default:
						if (!signedOk)
							contact.keystat_set( Contact.KEYSTAT_PENDING );
				}
			}
			Dib2Config.log( "bg recv_db", "update db " + contact.keystat_get() );
			db.contact_update( contact );
		}
	} else {
		Dib2Config.log( "bg recv_db", "something is wrong with the key" );
	}
	local_message.send_statusMsg( svcContext, "" );
}

public void received_attachment( quickmsg_db db, message m, attachment a, String ext )
{
	Dib2Config.log( "bg received attachement", "going to save attachment" );
//		File sd = Environment.getExternalStorageDirectory();
//		String dir = sd.getAbsolutePath() + "/QuickMSG";
	File df = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS );
	df = new File( df, "dibdib" );
	df.mkdirs();
	String dir = df.getAbsolutePath();
//		File df = new File(dir);
	String filenamebase;
	if (a.name == null) {
		filenamebase = UUID.randomUUID().toString() + "." + ext;
	} else {
		filenamebase = a.name;
	}
	File mf;
	int nr = 0;
	String filename;
	do {
		filename = filenamebase + (nr == 0 ? "" : "_" + nr);
		filename = ((0 != nr) && (1 < filename.indexOf( '.' ))) ? filenamebase.replace( ".", "_" + nr + '.' ) : filename;
		filename = ((ext == null) || filename.endsWith( ext )) ? filename : (filename + '.' + ext);
		mf = new File( dir, filename );
		if (!mf.exists()) {
			break;
		}
		Dib2Config.log( "bg received attachment", "exists: " + nr );
		nr ++;
	} while (true);
	String name = mf.getPath();
	df.mkdirs();
	OutputStream os;
	String type = a.datahandler.getDataSource().getContentType();
	type = type.split( ";" )[ 0 ];
	String basetype = type.split( "/" )[ 0 ].toLowerCase();

	Dib2Config.log( "bg received attachment", "name: " + name );

	try {
		os = new FileOutputStream( mf );
		a.datahandler.writeTo( os );
		os.flush();
		os.close();
	} catch (Exception e) {
		String err = e.getMessage();
		Log.e( "received attachment", "err: " + (err != null ? err : "null") );
		return;
	}

	Uri uri;
	if (basetype.equals( "image" )) {
		Dib2Config.log( "bg received attachment", "add image to media library" );
		ContentValues values = new ContentValues( 5 );

		values.put( Images.Media.TITLE, filename );
		values.put( Images.Media.DISPLAY_NAME, name );
		values.put( Images.Media.DATE_TAKEN, m.time_get() );
		values.put( Images.Media.MIME_TYPE, type );
		values.put( Images.Media.DATA, name );

		ContentResolver cr = svcContext.getContentResolver();
		uri = cr.insert( Images.Media.EXTERNAL_CONTENT_URI, values );
	} else if (basetype.equals( "video" )) {
		Dib2Config.log( "bg received attachment", "add video to media library" );
		ContentValues values = new ContentValues( 5 );

		values.put( Video.Media.TITLE, filename );
		values.put( Video.Media.DISPLAY_NAME, name );
		values.put( Video.Media.DATE_TAKEN, m.time_get() );
		values.put( Video.Media.MIME_TYPE, type );
		values.put( Video.Media.DATA, name );

		ContentResolver cr = svcContext.getContentResolver();
		uri = cr.insert( Video.Media.EXTERNAL_CONTENT_URI, values );
	} else if (basetype.equals( "audio" )) {
		Dib2Config.log( "bg received attachment", "add audio to media library" );
		ContentValues values = new ContentValues( 5 );

		values.put( Audio.Media.TITLE, filename );
		values.put( Audio.Media.DISPLAY_NAME, name );
		values.put( Audio.Media.DATE_ADDED, m.time_get() );
		values.put( Audio.Media.MIME_TYPE, type );
		values.put( Audio.Media.DATA, name );

		ContentResolver cr = svcContext.getContentResolver();
		uri = cr.insert( Audio.Media.EXTERNAL_CONTENT_URI, values );
	} else {
		uri = Uri.parse( name );
	}
	m.text_set( "::" + basetype + "::" + name + '\n' + m.text_get() );
	m.uri_set( uri );
}

public int findUnread() {
//	int unread = 0;
	int unread_contacts = 0;
	int unread_contact = 0;

	/* get unread messages */
	List< Contact > contacts = db.contact_get_all();
//	Dib2Config.log( "background", "notification " + contacts.size() );
	boolean arriving = (unread >= 0);
	unread = 0;
	for (int i = 0; i < contacts.size(); i ++) {
		Contact contact = contacts.get( i );
		if (contact.id_get() <= 1) {
			continue;
		}
		long lastact = contact.time_lastact_get();
		long unreadt = contact.unread_get();

		if (lastact > unreadt) {
			int cu = db.message_get_unread_by_chat( contact.id_get(), unreadt );
			unread += cu;
			if (cu > 0) {
				unread_contacts ++;
				unread_contact = contact.id_get();
			}
		}
	}
	unread = ((0 < unread) || (arriving && !alarmDone)) ? unread : -1;
	return (unread_contacts == 1) ? unread_contact : ((unread > 0) ? 0 : (unread - 1));
}

public void update_ui( boolean ack )
{
	int unread_contact = findUnread();
	Dib2Config.log( "bg update_ui", "unread: " + unread + alarmDone + idleTimeNet / 1000 + ' ' + flagsNotify );

	if (notification != null) {

		// Creates an explicit intent for an Activity in your app
		Intent resultIntent;
		if (unread_contact <= 1) {
			resultIntent = new Intent( this, net.sourceforge.dibdib.android.qm.ListActivity.class );
		} else {
			resultIntent = new Intent( this, MainActivity.class );
			resultIntent.putExtra( "id", unread_contact );
//			Dib2Config.log( "bg update_ui", "extra id: " + unread_contact + ", " + background_1.flagsNotify + ack );
		}

		resultIntent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP ); // Intent.FLAG_ACTIVITY_CLEAR_TASK );
		PendingIntent pending_intent = PendingIntent.getActivity( svcContext, 0, resultIntent,
			PendingIntent.FLAG_CANCEL_CURRENT ); // .FLAG_UPDATE_CURRENT);

		NotificationManager mNotificationManager =
			(NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );

		notification.setNumber( (0 < unread) ? unread : 0 );
		notification.setContentTitle( getString( R.string.app_name ) );
		if (0 <= unread) { // || (0 < unread_contacts)) { // && !ack) {
			notification.setSmallIcon( R.drawable.ic_launcher );
			notification.setLargeIcon( BitmapFactory.decodeResource( svcContext.getResources(),
				R.drawable.ic_launcher ) );
			int flags = ((0 != (background_1.flagsNotify & 1)) ? Notification.DEFAULT_LIGHTS : 0);
			if (!alarmDone) {
				if (0 < unread) {
					alarmDone = true;
					flags |= ((0 != (background_1.flagsNotify & 2)) ? Notification.DEFAULT_VIBRATE : 0) //,
						| ((0 != (background_1.flagsNotify & 4)) ? Notification.DEFAULT_SOUND : 0);
				}
			}
			notification.setDefaults( flags );
		} else {
			alarmDone = false;
			notification.setSmallIcon( R.drawable.ic_stat_qm );
			notification.setLargeIcon( BitmapFactory.decodeResource( svcContext.getResources(),
				R.drawable.ic_stat_qm ) );
			notification.setDefaults( 0 );
		}
		notification.setContentText( getString( R.string.status_unread ) + ((0 < unread) ? unread : 0) );
		notification.setContentIntent( pending_intent );
		mNotificationManager.notify( notification_id, notification.getNotification() ); //.build() );

	}
}

public void alarm()
{
	// Done by Notification flags ...
	// NOP
	;
	Uri uri = Uri.parse( "default ringtone" );
	if ((null != uri) && (0 != (flagsNotify & 4))) {
		Ringtone r = RingtoneManager.getRingtone( getApplicationContext(), uri );
		if (null != r) {
			r.play();
		}
	}
}

public void alarm_OLD()
{
//	SharedPreferences getAlarms = PreferenceManager.
//		getDefaultSharedPreferences( this ); // getBaseContext() );
//	Boolean notify = getAlarms.getBoolean( "notifications_new_message", false );
	boolean notify = false;
	Dib2Config.log( "alarm", "notify? " + (notify ? "yes" : "no") );
	if (!notify)
		return;
//
//	String alarms = getAlarms.getString( "notifications_new_message_ringtone", "default ringtone" );
	Uri uri = Uri.parse( "default ringtone" ); //alarms );
	if (null != uri) {
		Ringtone r = RingtoneManager.getRingtone( getApplicationContext(), uri );
		if (null != r) {
			r.play();
		}
	}
	Boolean vibrate = true; // getAlarms.getBoolean( "notifications_new_message_vibrate", false );
	Dib2Config.log( "alarm", "vibrate? " + (vibrate ? "yes" : "no") );
	if (!vibrate)
		return;

	Vibrator v = (Vibrator) this.getSystemService( VIBRATOR_SERVICE );
	if (null != v) {
		// Vibrate for 500 milliseconds
		v.vibrate( 500 );
	}
}
//=====
}
