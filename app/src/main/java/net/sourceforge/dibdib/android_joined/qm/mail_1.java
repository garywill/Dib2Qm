// Copyright (C) Roland Horsch and others:
// -- Changes:  Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// -- Original: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See dibdib/assets/license.txt for detailed information.
// This part is based on the corresponding code from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.android_joined.qm;

import android.content.Context;
import android.util.Log;
import javax.mail.Store;

public class mail_1 extends net.vreeken.quickmsg.mail_0 {
//=====

//DIFF (_0):
// public static boolean imap_connected = false;
// public static ...
// public List<attachment> multipart_get_attachments(attachment mpa)
// intent.setAction("net.sourceforge.dibdib.android.update_ui");
// Keep track of background.unread.

@Override
public Store open_imap( Context context )
{
	try {
		return super.open_imap( context );
	} catch (Exception e2) {
		Log.e( "open_imap", "connect imap: " + e2.getMessage() );
	}
	return null;
}

//=====
}
