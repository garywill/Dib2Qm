// By Bruce Eckel:
// http://www.mindview.net/Etc/Discussions/CheckedExceptions,
// with adaptation idea from Lukas Eder:
// https://dzone.com/articles/throw-checked-exceptions,
// as available on 2017-09-02.

/* Usage:

catch(ExceptionAdapter ea) {
  try {
    ea.rethrow();
  } catch(IllegalArgumentException e) {
    // ...
  } catch(FileNotFoundException e) {
    // ...
  }
  // etc.
}

if(futzedUp)
    throw new ExceptionAdapter(new CloneNotSupportedException());
    
 
public class ExceptionAdapterTest {
  public static void main(String[] args) {
    try {
      try {
        throw new java.io.FileNotFoundException("Bla");
      } catch(Exception ex) {
        ex.printStackTrace();
        throw new ExceptionAdapter(ex);
      }   
    } catch(RuntimeException e) {
      e.printStackTrace();
    }
    System.out.println("That's all!");
  }
}    
*/

package net.sourceforge.dibdib.all_joined.util;

import java.io.*;

public class ExceptionAdapter extends RuntimeException {
//=====

// Added:
private static final long serialVersionUID = -8947215243109261997L;

private final String stackTrace;
public Exception originalException;

public ExceptionAdapter( Exception e ) {
	super( e.toString() );
	originalException = e;
	StringWriter sw = new StringWriter();
	e.printStackTrace( new PrintWriter( sw ) );
	stackTrace = sw.toString();
}

@Override
public void printStackTrace() {
	printStackTrace( System.err );
}

@Override
public void printStackTrace( java.io.PrintStream s ) {
	synchronized (s) {
		s.print( getClass().getName() + ": " );
		s.print( stackTrace );
	}
}

@Override
public void printStackTrace( java.io.PrintWriter s ) {
	synchronized (s) {
		s.print( getClass().getName() + ": " );
		s.print( stackTrace );
	}
}

public void rethrow() {
	// Changed:
	//throw originalException;
	doThrow( originalException );
}

///// Lukas Eder:

static void doThrow( Exception e ) {
	ExceptionAdapter.< RuntimeException > doThrow0( e );
}

@SuppressWarnings( "unchecked" )
static < E extends Exception > void doThrow0( Exception e ) throws E {
	throw (E) e;
}

//=====
}