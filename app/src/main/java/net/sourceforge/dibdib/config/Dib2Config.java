// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package net.sourceforge.dibdib.config;

import java.util.Locale;
import localhost.optional.util.CsvCodecAes;
import net.sourceforge.dibdib.all.join.*;
import net.sourceforge.dibdib.all.util.*;

public class Dib2Config implements Dib2Constants {
//=====

///// Fixed values:

/** First one is default for encoding. */
public static final CsvCodecIf[] csvCodecs = new CsvCodecIf[] {
	CsvCodecAes.instance,
	CsvCodec1.instance,
	CsvCodec0.instance,
};

public static final Locale locale = null; // null for multilingual texts with CLDR (UCA DUCET) sorting

///// To be initialized:

public static char platform = 0;
/** Use this cautiously: potential threading issues. */
public static ContextIf qMainContext = null;

/** Referenced value (to be set in Main): true if processor does fast multiplication, compared to shifting and adding. */
public static boolean[] useFastMultiplication = new boolean[] { false };

/** Option: use linked list instead of array for TfPortBuffer etc. */
public static boolean useLinkedList = false;

/** General initialization here, specific values at caller. */
public static void init( char xPlatform, ContextIf mainContext ) {
	if (null != mainContext) {
		qMainContext = mainContext;
		qMainContext.init( platform );
	}
	if (0 != platform) {
		return;
	}
	platform = xPlatform;
	switch (platform) {
		case '0': {
			// Assuming i686 or amd64 or similar processor:
			useFastMultiplication = new boolean[] { true };
			break;
		}
		default:
			;
	}
	UtilMisc.timeZoneDone = false;
	for (CsvCodecIf codec : csvCodecs) {
		codec.init( platform );
	}
}

/** Enable early logging as much as possible */
public static void log( String... aMsg ) {
	UtilMisc.keepLog( aMsg[ 0 ], aMsg[ 1 ] );
	if (null != qMainContext) {
		qMainContext.log( aMsg );
	}
}

//=====
}
