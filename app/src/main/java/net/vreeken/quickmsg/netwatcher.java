/*
    QuickMSG
    Copyright (C) 2014  Jeroen Vreeken <jeroen@vreeken.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// ... with small adaptations for Dib2QM: Dib2Config.log() <= Log.d() etc.

package net.vreeken.quickmsg;

import net.sourceforge.dibdib.config.Dib2Config;

import net.sourceforge.dibdib.all.util.UtilMisc;

import net.sourceforge.dibdib.android.qm.background;

import android.content.*;
import android.net.*;

public class netwatcher extends BroadcastReceiver {

@Override
public void onReceive( Context context, Intent intent ) {
	//here, check that the network connection is available. If yes, start your service. If not, stop your service.
	ConnectivityManager cm = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
	NetworkInfo info = (null == cm) ? null : cm.getActiveNetworkInfo();
	Intent bg;

	if (info != null) {
		if (info.isConnected()) {
			Dib2Config.log( "netwatcher", "connection, sending 1" );
			bg = new Intent( context, background.class );
			bg.putExtra( "state", "T" );
			context.startService( bg );
			return;
		}
	}

	Dib2Config.log( "netwatcher", "no connection, sending 0" );
	bg = new Intent( context, background.class );
	bg.putExtra( "state", "F" );
//       context.stopService(bg);
	context.startService( bg );
}
}
