/*
    QuickMSG
    Copyright (C) 2014  Jeroen Vreeken <jeroen@vreeken.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// ... with small adaptations for Dib2QM: Dib2Config.log() <= Log.d() etc.

package net.vreeken.quickmsg;

import net.sourceforge.dibdib.config.*;
import net.sourceforge.dibdib.android.qm.*;

import javax.activation.*;
import javax.mail.*;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.*;
import javax.mail.util.*;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.*;
import android.util.Log;

public class mail_0 {
	public static volatile IMAPFolder mail_folder = null;
	protected static volatile Store mail_store = null;
	public static volatile boolean receive = false; // rho

//	public static volatile boolean imap_connected = false;
	
	// rho:	
	protected void log_e(Context c, String task, String msg, Exception e) {
//		logData += "(" + task + ") " + msg + '\n';
		if (e != null) {
			String msg2 = e.getMessage();
			msg = (msg2 == null) ? msg : msg2;
			msg = "" + e + ((msg != null) ? (" (" + msg + ")") : "");
		}
		if (c != null
		) local_message.send_statusMsg(c, "E(" + task + ") " + msg);
		else Dib2Config.log( task, msg );
		Log.e(task,msg);
	}
	
	public String get_mqueue_dir(Context c)
	{
		File dir = c.getExternalFilesDir(null);
		if (dir == null) {
			dir = c.getFilesDir();
		}
		if (dir == null) {
			return null;
		}
		
		return dir.toString() + "/mailqueue/";
	}
	
	public Session get_smtp_session(Context context, boolean use_tls)
	{
		preferences preferences = new preferences(context);
		
		String host = preferences.get("smtp_server", "example.com");
		String user = preferences.get("smtp_user", "");
	 
		String pass = preferences.get("smtp_pass", "");

		/* These settings might appear insecure, but we don't trust email providers
		 * anyway. That is why we use encryption end to end.
		 * We might leak our meta-data a bit easier this way.
		 */
		Properties props = System.getProperties();
		if (use_tls) {
			props.put("mail.smtp.starttls.enable", "true");
		} else {
			props.put("mail.smtp.starttle.enable", "false");
		}
		props.put("mail.smtp.starttls.required", "false");
			
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", user);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.port", preferences.get("smtp_port", "587"));
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.checkserveridentity", "false");
		props.put("mail.smtp.ssl.trust", "*");
		props.put("mail.smtps.ssl.checkserveridentity", "false");
		props.put("mail.smtps.ssl.trust", "*");
		
		Session session = Session.getDefaultInstance(props, null);
		//session.setDebug(true);
			return session;
		}
		
		public String send(Context c, String to) {
			return send(c, to, null);
		}
		public String send(Context c, String to, List<attachment> attachments) {
			return send(c, to, attachments, "mixed");
		}
		public String send(Context context, String to, List<attachment> attachments, String subtype) {
			return send( true, context, to, attachments, subtype );
		}
		public String send(boolean immediately, Context context, String to, List<attachment> attachments, String subtype) {
			String queue = null;
			preferences preferences = new preferences(context);
			
		String from = preferences.get("email_address", "nobody@example.com");
		
		Multipart multiPart;
		String finalString="";
		 
		Session session = get_smtp_session(context, true);
		
		DataHandler handler=new DataHandler(new ByteArrayDataSource(finalString.getBytes(),"text/plain" ));
		MimeMessage message = new MimeMessage(session);
		
		try {
			message.setFrom(new InternetAddress(from));
			message.setDataHandler(handler);

			message.addHeader("X-QuickMSG", "v1.0");
		 
			multiPart=new MimeMultipart(subtype);
	 
			InternetAddress toAddress;
			toAddress = new InternetAddress(to);
			message.addRecipient(Message.RecipientType.TO, toAddress);

			message.setSubject("[quickmsg]");
			Date sent_date = new Date();
			message.setSentDate(sent_date);
			message.setText("This is a QuickMSG.");

	//        	Dib2Config.log("smtp_send", "Message headers ready");
			
			if (attachments != null) {
				Dib2Config.log("send", "attachments: " + attachments.size());
				for (int i = 0; i < attachments.size(); i++) {
					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setDataHandler(attachments.get(i).datahandler);
					messageBodyPart.setFileName(attachments.get(i).name);
					messageBodyPart.setDisposition(attachments.get(i).disposition);
					multiPart.addBodyPart(messageBodyPart);
				}
			}
			message.setContent(multiPart);
		}
		catch (Exception e) {
			log_e(context,"send_msg", "could not create message: ", e);
			e.printStackTrace();
			return queue;
		}
		
		if (/* rho */ !immediately || !smtp_send(context, message)) {
			queue = save(context, message);
			if (!immediately)
				local_message.send_statusMsg(context,
					"TMessage saved.");
	//   			"Sending message failed, " + "the message has been saved for a later attempt.");
		}
		
		return queue;
	}

	public String save(Context c, Message m)
	{
		String dir = get_mqueue_dir(c); 
		if (dir == null)
			return null;

		File df = new File(dir);
		String mfile = UUID.randomUUID().toString();
		File mf = new File(dir, mfile);
		df.mkdirs();
		Dib2Config.log("mail save", "save message to dir: " + dir + " file: " + mfile);

		OutputStream of;
		try {
			of = new FileOutputStream(mf);
		} catch (FileNotFoundException e) {
			String msg = e.getMessage();
			Dib2Config.log("save", "Could not create file: " + (msg != null ? msg : "null"));
			return null;
		}
		
		try {
			m.writeTo(of);
		} catch (IOException e) {
			String msg = e.getMessage();
			Dib2Config.log("save", "IOException: " + (msg != null ? msg : "null"));
			e.printStackTrace();
		} catch (MessagingException e) {
			String msg = e.getMessage();
			Dib2Config.log("save", "MessagingException: " + (msg != null ? msg : "null"));
			e.printStackTrace();
		}
		
		try {
			of.close();
		} catch (Exception e) {
			//String err = e.getMessage();
			log_e(c,"mail save", null, e);
		}
		
		return mfile;
	}
	
	public boolean queue_check(Context c, String filename)
	{
		File f = new File(get_mqueue_dir(c), filename);
		
		return f.exists();
	}
	
	public void flush(Context c)
	{
		String dir = get_mqueue_dir(c); 
		if (dir == null)
			return;
		
		File[] files = new File(dir).listFiles();
		if (files == null)
			return;
		for ( File f : files ) {
			if (f.isFile()) {
				Dib2Config.log("mail flush", "Message in queue, size: " + f.length());
				if (f.length() == 0) {
					f.delete();
					Dib2Config.log("mail flush", "Delete empty message, propably from a previous buggy version");
					continue;
				}
				InputStream is;
				try {
					is = new FileInputStream(f);
				} catch (FileNotFoundException e1) {
					continue;
				}
				Session session = get_smtp_session(c, true);
				try {
					Message message = new MimeMessage(session, is);
					if (smtp_send(c, message)) {
						f.delete();
						Intent intent = new Intent();
//						intent.setAction("net.sourceforge.dibdib.android.update_ui");
//						c.sendBroadcast(intent);
					}
//					Dib2Config.log("mail flush", "Message send");
				} catch (MessagingException e) {
//					String msg = e.getMessage();
//					msg = (null == msg) ? "" + e : msg;
					log_e(c,"flush", "Sending message failed", e); // + (msg != null ? msg : ""));
//		   			local_message.send_statusMsg(c, 
//	   	   				"Sending message failed: " + msg);
					continue;
				}
			
			}
		}
	}

	public boolean smtp_send(Context context, Message message)
	{
		preferences preferences = new preferences(context);

		String host = preferences.get("smtp_server", "example.com");
		String user = preferences.get("smtp_user", "");
		String pass = preferences.get("smtp_pass", "");
		String port = preferences.get("smtp_port", "587");
		String transport1;
		String transport2;
		
		if (port.equals("465")) {
			transport1 = "smtps";
			transport2 = "smtp";
		} else {
			transport1 = "smtp";
			transport2 = "smtps";
		}
		
		try {

			Session session = get_smtp_session(context, true);
			Transport transport = session.getTransport(transport1);

			transport.connect(host, user, pass);

			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		}
		catch (Exception e) {
			log_e(context,"send_msg", "could not send message with " + transport1, e); // + ": " + e.getMessage());
			e.printStackTrace();
			try {

				Session session = get_smtp_session(context, true);
				Transport transport = session.getTransport(transport2);

				transport.connect(host, user, pass);

				transport.sendMessage(message, message.getAllRecipients());
				transport.close();
			}
			catch (Exception e2) {
				log_e(context,"send_msg", "could not send message with " + transport2, e2); // + ": " + e.getMessage());
					e.printStackTrace();
				return false;
			}
		}
		Dib2Config.log("send_msg", "message has been sent.");

		return true;
	}
	
	public Boolean recv_quickmsg_cb(String from, List<attachment> attachments, String subtype)
	{
		return true;
	}

	public Store open_imap(Context context)
	{
		if (mail_store != null) {
			if (mail_store.isConnected())
				return mail_store;
		}
		imap_close(); // rho
//		mail_store = null;
//		mail_folder = null;
		
		preferences preferences = new preferences(context);
		
		String host = preferences.get("imap_server", "example.com");
		String port = preferences.get("imap_port", "993");
		String user = preferences.get("imap_user", "nobody");
		String pass = preferences.get("imap_pass", "");

		Properties props = System.getProperties();
		props.setProperty("mail.imap.host", host);
		props.setProperty("mail.imap.port", port);
		props.setProperty("mail.imap.connectiontimeout", "5000");
		props.setProperty("mail.imap.ssl.trust",  "*");
		props.setProperty("mail.imap.starttls.enable", "true");
		//props.setProperty("mail.imap.timeout", "5000");

		Session session = Session.getDefaultInstance(props);
		URLName urlName = new URLName("imaps", host, Integer.parseInt(port), "", user, pass);
		Store store;
		
//		Dib2Config.log("recv_msg", urlName.toString());
		try { 
			store = session.getStore(urlName);
			if (!store.isConnected()) {
				store.connect();
			}
		} catch (Exception e) {
			log_e(context,"recv_msg", "connect imap", e); // + e.getMessage());
			
			urlName = new URLName("imap", host, Integer.parseInt(port), "", user, pass);
//    			Dib2Config.log("recv_msg", urlName.toString());
			
			try {
				store = session.getStore(urlName);
				if (!store.isConnected()) {
					store.connect();
				}
			} catch (Exception e2) {
				log_e(context,"recv_msg", "connect imap", e2); // + e2.getMessage());
				return null;
			}
		}
		
//		Dib2Config.log("recv_msg", "store.isConnected():" + store.isConnected());
				
		mail_store = store;

//		imap_connected = true;
		local_message.send_connection(context, true); //imap_connected);

		return store;
	}
	
	public IMAPFolder open_folder(Context context, Store store) 
	{
		// Open the Folder
		String mbox = "INBOX";
		IMAPFolder folder;
		if (store != null) { // rho
			if (store != mail_store) {
				if (mail_store != null) { // || ! mail_store.isConnected()) {
					imap_close();
				}
				mail_store = store;
			}
		}
		if ((mail_store != null) && ! mail_store.isConnected()) {
			imap_close();
		}
		if (mail_store == null) {
			mail_folder = null;
			return null;
		}
		if ((mail_folder != null) && mail_folder.isOpen()) {
			return mail_folder;
		}
		if (mail_folder != null) {
			imap_close();
			return null;
		}
		store = mail_store;
//		imap_connected = false;
		try {
			folder = (IMAPFolder) store.getDefaultFolder();
			if (folder == null) {
				log_e(context,"recv_msg", "No default folder", null);
				return null;
			}

			folder = (IMAPFolder) folder.getFolder(mbox);
			if (!folder.exists()) {
			   log_e(context,"recv_msg", mbox + "  does not exist", null);
			   return null;
			}
		
			folder.open(Folder.READ_WRITE);

		} catch (MessagingException e) {
			log_e(context,"open_folder", null, e); //e.getMessage());
			return null;
		} catch (IllegalStateException ei) {
//			//String msg = ei.getMessage();
//			log_e(null,"open_folder", "illegal state", ei); // +(msg != null ? msg : "null"));
//			mail_folder = null;
//			try {
//				mail_store.close();
//			} catch (Exception e) {
////				e.printStackTrace();
//			}
//			mail_store = null;
			imap_close();
			return null;
		}

	
		try {
			folder.setSubscribed(true);
		} catch (MessagingException e) {
			log_e(context,"open folder", "setSubscribed", e); // + e.getMessage());
		}
		
		mail_folder = folder;
		return folder;
	}

	public void noop(IMAPFolder folder) {
		local_message.usingOriginalThrowNull = 42 / local_message.usingOriginalThrowNull; // rho
		if (folder == null) {
			if (mail_folder != null)
				folder = mail_folder;
			else
				return;
		}
		try {
			folder.doCommand(new IMAPFolder.ProtocolCommand() {
				public Object doCommand(IMAPProtocol p)
						throws ProtocolException {
					p.simpleCommand("NOOP", null);
					return null;
				}
			});
		} catch (MessagingException e) {
			log_e(null,"noop", "noop failed", e); // + e.getMessage());
		}
	}

	
	public void idle(Context context, final long timeout) 
	{
		Store store = open_imap(context);
		if (store == null) {
			Dib2Config.log("mail idle", "store null");
			try {
				Thread.sleep(500); // 10000
			} catch (InterruptedException e) {
			}
			return;
		}
		final IMAPFolder folder = open_folder(context, store);
		if (folder == null) {
			Dib2Config.log("mail idle", "folder null");
			return;
		}
		
		Thread t = new Thread(new Runnable() {
			public void run() {
				boolean intr = false;
				try {
					Thread.sleep(timeout);
				} catch (InterruptedException e) {
					intr = true;
				}
				if (!intr) { // rho
//					Dib2Config.log("mail idle", "timeout " + timeout);
					noop(folder);
				}
			}
		});
		
		t.start();

		folder.addMessageCountListener(new MessageCountListener() {
			@Override
			public void messagesAdded(MessageCountEvent arg0) {
				Dib2Config.log("mail idle", "message added");
				++ background.unread; // rho
				noop(folder);
			}

			@Override
			public void messagesRemoved(MessageCountEvent arg0) {
			}
		});
		
		try {
			folder.idle();
			t.interrupt();
		} catch (MessagingException e) {
			log_e(context,"idle", null, e); //e.getMessage());
			if (!store.isConnected()) {
				Dib2Config.log("idle", "connection closed");
				
//				imap_connected = false;
				local_message.send_connection(context, false);// imap_connected);

				mail_folder = null;
				mail_store = null;
			}
		}
//		Dib2Config.log("mail idle", "end of idle");
	}
	
	public void recv(Context context) {
		Store store = open_imap(context);
		if (store == null)
			return;

		Folder folder = open_folder(context, store);
		if (folder == null)
			return;
		
		Boolean exp = false;
		
		try {

			if (!(folder instanceof UIDFolder)) {
			   log_e(context,"recv_msg", "This Provider or this folder does not support UIDs", null);
			   return;
			}

			UIDFolder ufolder = (UIDFolder)folder;

			int totalMessages = folder.getMessageCount();

			if (totalMessages == 0) {
			   Dib2Config.log("recv_msg", "Empty folder");
			   return;
			}

			// Attributes & Flags for ALL messages ..
			Message[] msgs = ufolder.getMessagesByUID(1, UIDFolder.LASTUID);
			// Use a suitable FetchProfile
			FetchProfile fp = new FetchProfile();
			fp.add(FetchProfile.Item.ENVELOPE);
			fp.add(FetchProfile.Item.FLAGS);
			fp.add("X-QuickMSG");
			fp.add("From");
			folder.fetch(msgs, fp);

			for (int i = 0; i < msgs.length; i++) {
				// ufolder.getUID(msgs[i]) + ":");
				InternetAddress add = (InternetAddress)msgs[i].getFrom()[0];
				String add_str = add.getAddress();
				Pattern pattern = Pattern.compile("<(.*?)>");
				Matcher matcher = pattern.matcher(add_str);
				if (matcher.find())
				{
				   add_str = matcher.group(1);
				}

				String h[] = msgs[i].getHeader("X-QuickMSG");
				if (h != null) {
					if (msgs[i].isSet(Flags.Flag.DELETED))
					   continue;

					Object o = msgs[i].getContent();
					if (!(o instanceof MimeMultipart))
					   continue;

					List<attachment> attachments = new LinkedList<attachment>();

					Multipart mp = (Multipart)o;
					o = null;
					ContentType ct = new ContentType(mp.getContentType());
					String subtype = ct.getSubType();

					for (int j = 0; j < mp.getCount(); j++) {
					   BodyPart bp = mp.getBodyPart(j);
					   attachment attachment = new attachment();
					   attachment.name = bp.getFileName();
					   attachment.datahandler = bp.getDataHandler();
					   attachments.add(attachment);
					}
					mp = null;
					ct = null;

					if (recv_quickmsg_cb(add_str, attachments, subtype)) {
					   msgs[i].setFlag(Flags.Flag.SEEN, true);
					   msgs[i].setFlag(Flags.Flag.DELETED, true);
					   exp = true;
					}
				}
			}

			if (exp) {
//			   imap_connected = false;
//			   local_message.send_connection(context, imap_connected);
				folder.expunge();
				folder.close(false);
				store.close();
				mail_folder = null;
				mail_store = null;
			}
		} catch (Exception e) {
//			log_e(context,"recv_msg", "recv", e); // + e.getMessage());
			// Possible race condition.
			Dib2Config.log( "recv_msg", "exception: " + e );
		}
		receive = false; // rho
	}
	
	public void imap_close()
	{
		IMAPFolder f0 = mail_folder; // rho
		Store s0 = mail_store;
		mail_folder = null;
		mail_store = null;
		if (f0 != null) {
			try {
				if (f0.isOpen())
				f0.expunge();
			} catch (Exception e) {
			}
			try {
				f0.close(false);
			} catch (Exception e) {
			}
		}
		if (s0 != null) {
			try {
				s0.close();
			} catch (Exception e) {
//			e.printStackTrace();
			}
		}
//	   mail_folder = null;
//	   mail_store = null;
//	   imap_connected = false;
	}
	
	public attachment multipart_create(List<attachment> attachments)
	{
		MimeMultipart multiPart;
		multiPart=new MimeMultipart("mixed");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			Properties props = System.getProperties();
			props.put("mail.host", "smtp.dummydomain.com");
			props.put("mail.transport.protocol", "smtp");

			Session mailSession = Session.getDefaultInstance(props, null);
			MimeMessage message = new MimeMessage(mailSession);
			
			for (int i = 0; i < attachments.size(); i++ ) {
				attachment attachment = attachments.get(i);
				BodyPart messageBodyPart = new MimeBodyPart();

				messageBodyPart.setDataHandler(attachment.datahandler);
				    messageBodyPart.setFileName(attachment.name);
				    messageBodyPart.setDisposition(attachment.disposition);
				    multiPart.addBodyPart(messageBodyPart);
			}
		
		    
//			Dib2Config.log("mp", multiPart.getPreamble());
//			multiPart.setPreamble("T");
		    
			message.setContent(multiPart);

			DataHandler dh = message.getDataHandler();
			Dib2Config.log("message", "contenttype: " + dh.getContentType());

			Dib2Config.log("message", "len " + message.toString().length());
			message.writeTo(bos);
		}
		catch (Exception e) {
			String em = e.getMessage();
			if (em == null) {
				em = "Could not create message with attachments";
			}
			log_e(null,"create mime", em, e);
		}
		DataSource ads = new ByteArrayDataSource(bos.toByteArray(), "multipart/mixed");
		attachment aout = new attachment();
		aout.datahandler = new DataHandler(ads);
		aout.name = "encrypted";
	
		return aout;
	}
	
	public List<attachment> multipart_get_attachments(attachment mpa)
	{
		Properties props = System.getProperties();
		props.put("mail.host", "smtp.dummydomain.com");
		props.put("mail.transport.protocol", "smtp");
		Session mailSession = Session.getDefaultInstance(props, null);
		MimeMessage msg = null;
		
		try {
			msg = new MimeMessage(mailSession, mpa.datahandler.getInputStream());
		} catch (Exception e) {
			log_e(null,"multipart_get_attachments", "Could not create mime message", e); // + e.getMessage());
			e.printStackTrace();
			return null;
		}
		if (msg == null)
			return null;
		
		Object o;
		try {
			o = msg.getContent();
		} catch (Exception e) {
			log_e(null,"multipart_get_attachments", "Could not get content", e); // + e.getMessage());
			e.printStackTrace();
			return null;
		}
		if (!(o instanceof MimeMultipart))
		   	return null;

		List<attachment> attachments = new LinkedList<attachment>();
		   
		Multipart mp = (Multipart)o;
		try {
			for (int j = 0; j < mp.getCount(); j++) {
			   BodyPart bp = mp.getBodyPart(j);
			   attachment attachment = new attachment();
			   attachment.name = bp.getFileName();
			   attachment.datahandler = bp.getDataHandler();
			   attachments.add(attachment);
			   Dib2Config.log("multipart_get_attachments", "Got attachment with name:" + attachment.name);
			}
		}
		catch (Exception e) {
			log_e(null,"multipart_get_attachments", "Could not get attachments", e); // + e.getMessage());
			e.printStackTrace();
			return null;
		}
		
		return attachments;
	}
}
