/*
    QuickMSG
    Copyright (C) 2014  Jeroen Vreeken <jeroen@vreeken.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// ... with small adaptations for Dib2QM: Dib2Config.log() <= Log.d() etc.

package net.vreeken.quickmsg;

import localhost.optional.chat.Pgp;
import net.sourceforge.dibdib.config.Dib2Config;
import net.sourceforge.dibdib.android_joined.qm.quickmsg_activity_1;
import net.sourceforge.dibdib.all_joined.chat.Contact;
import net.sourceforge.dibdib.all.util.*;
import net.sourceforge.dibdib.android.dib2qm.R;
import net.sourceforge.dibdib.android.qm.*;


import android.text.style.ClickableSpan;

import android.content.ContentValues;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.app.AlertDialog;
import android.text.Html;
import android.text.Layout.Alignment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.*;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.EditText;
import android.util.*;

public class MainActivity_0 extends quickmsg_activity_1 {

//    protected quickmsg_db db = new quickmsg_db(this);
    protected Contact contact;
    protected TextView msg_viewer;
//    final MainActivity act = this;
    protected long unreadTime = 0;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        int id;
        String action = intent.getAction();
    	EditText new_msg = (EditText)findViewById(R.id.new_msg);
    	
    	new_msg.setText("");
    	//maxview = MAXVIEW_INIT;
    	msg_viewer = (TextView)findViewById(R.id.msg_viewer);
    	
        if (Intent.ACTION_SEND.equals(action)) {
        	Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (uri != null) {
            	select_contact(this, uri);
            }
        } else {
            id = intent.getIntExtra("id", -1);        
            Dib2Config.log("main actvitity", "id: " + id);
            
            contact = db.contact_get_by_id(id);
        }
        if (contact == null)
        	return;
        
        setTitle(getString(R.string.title_activity_main) + " - " + contact.name_get());

        // rho
        unreadTime = contact.unread_get();
        display_contact();
        checkBackground();
    }
    
    public void select_contact(final Context context, final Uri uri)
    {
	    final List<Contact> contactlist = db.contact_get_sendable();
	    List<CharSequence> names = new ArrayList<CharSequence>();

	    for (int i = 0; i < contactlist.size(); i++) {
			Contact p = contactlist.get(i);
			String add = p.address_get();	
			String name = p.name_get() + " (" + add + ")";
			
			names.add(name);
		}

	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    // Set the dialog title
	    builder.setTitle(R.string.action_select_contact);
	    // Specify the list array, the items to be selected by default (null for none),
	    // and the listener through which to receive callbacks when items are selected
	    builder.setSingleChoiceItems(
	    		names.toArray(new CharSequence[names.size()]),
	    		-1,
	            new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				contact = contactlist.get(which);
				Dib2Config.log("select contact", "which: " + which);
			}
	    });
	    // Set the action buttons
	    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            	Dib2Config.log("select contact", "OK " + (contact == null ? "null" : "contact"));
            	if (contact != null) {
            		Dib2Config.log("onresume", "going to send message");
            		send_msg_attachment(context, uri);
            	}
            	finish();
            	return;
	        }
	    });
	    
	    builder.setNegativeButton("Stop", new DialogInterface.OnClickListener() {
	    	@Override
	        public void onClick(DialogInterface dialog, int id) {
	    		Dib2Config.log("select contact", "Stop");
	    		contact = null;
	    		finish();
	    		return;
	        }
	    });
	    builder.show();
    }
    
    public void on_update_ui()
    {
		update_ui();
    }
    
    protected void update_ui() {
    	if (contact == null)
    		return;
    	display_contact();
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        case R.id.action_settings:

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);

            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    }    
*/
    
    /** Called when the user clicks the Send button */
    public void send_msg(View view)
    {
    	// rho
    	checkBackground();
    	send_msg_attachment(view.getContext(), null);
    	contact.time_lastact_set( -1 );
//    	contact.unread_set( -1 );
//    	unreadTime = contact.unread_get() - 4;
	
    	display_contact();
    }
    
    public void send_msg_attachment(final Context context, Uri uri) {
    	EditText new_msg = (EditText)findViewById(R.id.new_msg);
    	String message_text = new_msg.getText().toString();

	// rho:
	if ((contact.type_get() == Contact.TYPE_PERSON) && (contact.keystat_get() != Contact.KEYSTAT_VERIFIED))
		message_text = "Not sent (MISSING KEY):\n" + message_text;

    	final attachment unenc;
    	
    	if (message_text.length() < 1 && uri == null)
    		return;
    	
//    	Date now = new Date();
//		long time_now = now.getTime() / 1000;

		final message m = new message();
		m.id_set(contact.id_get());
		m.from_set(1);
		m.time_set(-1); // rho <= time_now);
		m.text_set(message_text);
		if (uri != null) {
			m.uri_set(uri);
		}
		
		db.message_add(m);
		
   		//new_mail.send(this);
   		Dib2Config.log("send_msg", "send to " + contact.name_get() + "id: " + contact.id_get());
   	
   		quickmsg qmsg = new quickmsg();
   		unenc = qmsg.send_message(this, db.contact_get_by_id(1), contact, m);
   		
//   		Dib2Config.log("send_msg", "got message unencrypted");
   		
   		
   		final List<String> to_adds;
   		if (contact.type_get() == contact.TYPE_GROUP)
   			to_adds = contact.members_get();
   		else {
   			to_adds = new LinkedList<String>();
   			to_adds.add(contact.address_get());
   		}

        new Thread(new Runnable() {
            public void run() {
            	Pgp pgp_enc = new Pgp(db); //context);
           		
           		attachment id = pgp_enc.pgpmime_id();

           		for (int i = 0; i < to_adds.size(); i++ ) {
           			String to = to_adds.get(i);
           			if (to.equals(pgp.my_user_id))
           				continue;

				// rho:
				if ((contact.type_get() == Contact.TYPE_PERSON) && (contact.keystat_get() != Contact.KEYSTAT_VERIFIED))
           				continue;

           			attachment enc;
           			try {
           				enc = pgp_enc.encrypt_sign(unenc, to);
           			} catch (OutOfMemoryError e) {
           				Log.e("send_msg", "Out of memory during encryption, attachment to big?");
           				enc = null;
           			}
           	   		if (enc == null)
           	   			continue;
           	   		enc.disposition = "inline";
           	   		
           	   		Dib2Config.log("send_msg", "got message encrypted");
//           			mail mail = new mail();
           			List<attachment> attachments = new LinkedList<attachment>();
           			attachments.add(id);
           			attachments.add(enc);
        		
           			String queue = background.mail.send(context, to, attachments, "encrypted");
           			if (queue != null) {
           				m.queue_set(queue);
           				db.message_update(m);

//           				Intent intent = new Intent();
//           				intent.setAction("net.vreeken.quickmsg.update_ui");
//           				sendBroadcast(intent);
           				local_message.send_statusMsg( context, "?" );
           			}
           		}
        		Dib2Config.log("send_msg", "mail.send done");            	
            }
        }).start();
        
        new_msg.setText("");
    }
    
    public void display_contact() {
    	{} // moved up
    }
}


