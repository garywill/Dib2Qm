// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om> (and others).
// See dibdib/assets/license.txt for detailed information.
// Parts of this code are based on other people's ideas and snippets as indicated in the methods.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package localhost.optional.util;

//import android.annotation.SuppressLint;
import java.security.spec.KeySpec;
import java.util.Arrays;
import javax.crypto.*;
import javax.crypto.spec.*;
import net.sourceforge.dibdib.config.Dib2Config;

public class UtilCodec {
//=====

/** PKCS7 or 5: 'AES/CBC/PKCS7Padding'/5 */
public static String INIT_cryptoAes = // 
// (Dib2Config.platform == '0') ? "AES/CBC/PKCS5Padding" : 
"AES/CBC/PKCS7Padding";
public static String INIT_cryptoPbkd = "PBKDF2WithHmacSHA1"; // "PBKDF2WithHmacSHA256";
public static int INIT_saltIterations = 1174; // 25519;

/** Cmp. Wikipedia 'Jenkins_hash_function' (2015), http://www.burtleburtle.net/bob/hash/doobs.html. */
public static int hash32_jenkinsOneAtATime( long[] data, int bytes4val, int... len_seed ) {
	// uint:
	int len = (len_seed.length < 1) ? data.length : len_seed[ 0 ];
	int hash = (len_seed.length < 2) ? bytes4val * len : len_seed[ 1 ];
	for (int i = 0; i < len; ++ i) {
		for (int sh = (bytes4val - 1) << 3; sh >= 0; sh -= 8) {
			hash += (data[ i ] >>> sh) & 0xff;
			hash += (hash << 10);
			hash ^= (hash >>> 6);
		}
	}
	hash += (hash << 3);
	hash ^= (hash >>> 11);
	hash += (hash << 15);
	return hash;
}

/** Cmp. Wikipedia 'Jenkins_hash_function' (2015), http://www.burtleburtle.net/bob/hash/doobs.html. */
public static int hash32_jenkinsOneAtATime( byte[] key, int... len_seed ) {
	// uint:
	int len = (len_seed.length < 1) ? key.length : len_seed[ 0 ];
	int hash = (len_seed.length < 2) ? len : len_seed[ 1 ];
	for (int i = 0; i < len; ++ i) {
		hash += key[ i ] & 0xff;
		hash += (hash << 10);
		hash ^= (hash >>> 6);
	}
	hash += (hash << 3);
	hash ^= (hash >>> 11);
	hash += (hash << 15);
	return hash;
}

/** Cmp. Wikipedia 'Jenkins_hash_function' (2015), http://www.burtleburtle.net/bob/hash/doobs.html. */
public static int hash32_jenkinsOneAtATime( String key ) {
	// uint:
	int hash = key.length();
	for (int i = hash - 1; i >= 0; -- i) {
		int c0 = key.charAt( i );
		int b0 = c0 >>> 8;
		int b1 = c0 & 0xff;
		if (b0 != 0) {
			hash += b0;
			hash += (hash << 10);
			hash ^= (hash >>> 6);
		}
		hash += b1;
		hash += (hash << 10);
		hash ^= (hash >>> 6);
	}
	hash += (hash << 3);
	hash ^= (hash >>> 11);
	hash += (hash << 15);
	return hash;
}

/** Cmp. Wikipedia 'Jenkins_hash_function' (2015), http://www.burtleburtle.net/bob/hash/doobs.html. */
public static int hash32_jenkinsOneAtATime( byte[] key ) {
	// uint:
	int hash = key.length;
	for (int i = hash - 1; i >= 0; -- i) {
		hash += key[ i ];
		hash += (hash << 10);
		hash ^= (hash >>> 6);
	}
	hash += (hash << 3);
	hash ^= (hash >>> 11);
	hash += (hash << 15);
	return hash;
}

/** FNV-1a, also for short keys.
 * Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
 * 
 * @param bits >= 16, <= 32
 */
public static int hash32_fnv1a( byte[] key, int bits ) {
//	final int prime = 0x1000193; // 16777619;
	final int offs = 0x811c9dc5; // (int) 2166136261L;
	int hash = offs;
	for (int i0 = 0; i0 < key.length; ++ i0) {
		hash ^= key[ i0 ];
		//hash *= prime;
		hash += (hash << 1) + (hash << 4) + (hash << 7) + (hash << 8) + (hash << 24);
	}
	if (bits == 32) {
		return hash;
	}
	// XOR folding:
	return ((hash >>> bits) ^ hash) & ((1 << bits) - 1);
}

/** FNV-1a, also for short keys.
 * Cmp. http://www.isthe.com/chongo/tech/comp/fnv.
 * 
 * @param bits >= 32, <= 64
 */
public static long hash64_fnv1a( byte[] key, int bits ) {
//	final long prime = 0x100000001b3L; // 1099511628211L;
	final long offs = 0xcbf29ce484222325L; // any non-zero: 14695981039346656037
	long hash = offs;
	for (int i0 = 0; i0 < key.length; ++ i0) {
		hash ^= key[ i0 ];
		// hash *= prime;
		hash += (hash << 1) + (hash << 4) + (hash << 5) + (hash << 7) + (hash << 8) + (hash << 40);
	}
	if (bits == 64) {
		return hash;
	}
	// XOR folding:
	return ((hash >>> bits) ^ hash) & ((1L << bits) - 1L);
}

public static long[] hash_xorshift( byte[] key, int size ) {
	long seed = 999999937; // could be added ...
	long[] out = new long[ size ];
	for (int i = 0; i < size; i ++) {
		out[ i ] = hash32_jenkinsOneAtATime( key, key.length, (int) seed );
		// Marsaglia.
		seed ^= (seed << 21);
		seed ^= (seed >>> 35);
		seed ^= (seed << 4);
	}
	return out;
}

/** Normalize pass phrase for ease of debugging.
 */
public static byte[] toPass32( byte[] pass ) {
	// Silly key stretching:
	int len = (pass.length >= 32) ? 32 : pass.length;
	byte[] kx = new byte[ 65 ];
	System.arraycopy( pass, 0, kx, 0, len );
	for (int count = len; count < 32; count += len) {
		kx[ count ++ ] = '.';
		System.arraycopy( pass, 0, kx, count, len );
	}
	return Arrays.copyOf( kx, 32 );
}

/** @param ivSalt16 16 byte nonce used as salt for stretching and as IV (instead of '0'), with first byte != 0
 * @return encrypted data with prepended 16 byte salt */
//@SuppressLint( "Assert" )
public static byte[] encrypt( byte[] zippedDat, int from, int to, byte[] passphrase, byte[] ivSalt16, int prepostLen ) throws Exception {
	// Dummy initial step of stretching for ease of debugging:
	byte[] pass32b = toPass32( passphrase );
//	PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator( new SHA256Digest() );
//	generator.init( pass32b, ivSalt16, INIT_saltIterations );
//	KeyParameter key = (KeyParameter) generator.generateDerivedMacParameters( 256 );
//	SecretKeySpec skspec = new SecretKeySpec( key.getKey(), "AES" );
	char[] pass32 = new String( pass32b ).toCharArray();
	KeySpec kspec = new PBEKeySpec( pass32, ivSalt16, INIT_saltIterations, 256 );
	SecretKeySpec skspec = new SecretKeySpec( //.
		SecretKeyFactory.getInstance( INIT_cryptoPbkd ).generateSecret( kspec ).getEncoded(),
		"AES" );

	// PKCS7 or 5: 'AES/CBC/PKCS7Padding'/5
	Cipher cipher = Cipher.getInstance( INIT_cryptoAes );
	// '0' as IV would be enough for AES in this case, even though header of plain text is partly known:
	cipher.init( Cipher.ENCRYPT_MODE, skspec, new IvParameterSpec( ivSalt16 ) ); // new byte[16]));
	assert Arrays.equals( ivSalt16, cipher.getParameters().getParameterSpec( IvParameterSpec.class ).getIV() );
	byte[] enc = cipher.doFinal( zippedDat, from, to - from );
	byte[] out = Arrays.copyOf( ivSalt16, ivSalt16.length + enc.length + 2 * prepostLen );
	if (0 < prepostLen) {
		System.arraycopy( ivSalt16, 0, out, prepostLen, ivSalt16.length );
	}
	System.arraycopy( enc, 0, out, ivSalt16.length + prepostLen, enc.length );
	return out;
}

/** @param ivSalt nonce used as salt for stretching and IV if first byte != 0 */
public static byte[] decrypt( byte[] dat, int from, int to, byte[] passphrase, byte[] ivSalt ) throws Exception {
	byte[] iv = new byte[ 16 ];
	SecretKeySpec skspec;
	if (ivSalt[ 0 ] != 0) {
		iv = Arrays.copyOfRange( ivSalt, 0, 16 );
		// Dummy initial step of stretching for ease of debugging:
		byte[] pass32b = toPass32( passphrase );
		char[] pass32 = new String( pass32b ).toCharArray();
		KeySpec kspec = new PBEKeySpec( pass32, iv, INIT_saltIterations, 256 );
		skspec = new SecretKeySpec( //.
			SecretKeyFactory.getInstance( INIT_cryptoPbkd ).generateSecret( kspec ).getEncoded(),
			"AES" );
//		PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator( new SHA256Digest() );
//		generator.init( pass32b, iv, INIT_saltIterations );
//		KeyParameter key = (KeyParameter) generator.generateDerivedMacParameters( 256 );
//		skspec = new SecretKeySpec( key.getKey(), "AES" );
	} else {
		skspec = new SecretKeySpec( toPass32( passphrase ), "AES" );
	}
	Cipher cipher = Cipher.getInstance( INIT_cryptoAes );
	cipher.init( Cipher.DECRYPT_MODE, skspec, new IvParameterSpec( iv ) );
	// Check block length:
	if (0 != (to - from) % 16) {
		Dib2Config.log( "decrypt", "AES?/ bad block length?" );
		to -= (to - from) % 16;
	}
	byte[] out = cipher.doFinal( dat, from, to - from );
	return out;
}

/** @param ivSalt nonce used as salt for stretching and IV if first byte != 0 */
/*
public static byte[] decrypt256( byte[] dat, int from, int to, byte[] passphrase, byte[] ivSalt ) throws Exception {
	byte[] iv = new byte[ 16 ];
	SecretKeySpec skspec;
	if (ivSalt[ 0 ] != 0) {
		iv = Arrays.copyOfRange( ivSalt, 0, 16 );
		// Dummy initial step of stretching for ease of debugging:
		byte[] pass32b = toPass32( passphrase );
		PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator( new SHA256Digest() );
		generator.init( pass32b, iv, INIT_saltIterations );
		KeyParameter key = (KeyParameter) generator.generateDerivedMacParameters( 256 );
		skspec = new SecretKeySpec( key.getKey(), "AES" );
	} else {
		skspec = new SecretKeySpec( toPass32( passphrase ), "AES" );
	}
	Cipher cipher = Cipher.getInstance( INIT_cryptoAes );
	cipher.init( Cipher.DECRYPT_MODE, skspec, new IvParameterSpec( iv ) );
	// Check block length:
	if (0 != (to - from) % 16) {
		Dib2Config.log( "decrypt", "AES?/ bad block length?" );
		to -= (to - from) % 16;
	}
	byte[] out = cipher.doFinal( dat, from, to - from );
	return out;
}
*/

public static byte[] mac( byte[] xKey32, byte[] xEncryptedData ) throws Exception {
	SecretKey hmacKey = new SecretKeySpec( xKey32, "HmacSHA256" );
	Mac mac = Mac.getInstance( "HmacSHA256" );
	mac.init( hmacKey );
	return mac.doFinal( xEncryptedData );
}

//=====
}
