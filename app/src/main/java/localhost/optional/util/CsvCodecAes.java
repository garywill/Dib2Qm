// Copyright (C) 2016,2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package localhost.optional.util;

import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.*;
import net.sourceforge.dibdib.all.join.CsvCodecIf;
import net.sourceforge.dibdib.all.util.UtilMisc;
import net.sourceforge.dibdib.config.*;

public class CsvCodecAes implements CsvCodecIf {
//=====

public static final CsvCodecAes instance = new CsvCodecAes();
public static StringBuffer buf = new StringBuffer();

@Override
public void init( char platform, Object... parameters ) {
	UtilCodec.INIT_cryptoAes = (Dib2Config.platform == '0') ? "AES/CBC/PKCS5Padding" :
		"AES/CBC/PKCS7Padding";
}

@Override
public byte[] compress( byte[] xyData, int xOffset4Reuse, int to ) {
	return UtilMisc.compress( 'z', xyData, xOffset4Reuse, to );
}

@Override
public byte[] decompress( byte[] xData, int len ) {
	return UtilMisc.decompress( xData, 0, len );
}

@Override
public byte[] encode( byte[] compressedData, int from, int to, byte[] key, int keyInfo, byte[] keyData, byte[] signatureKey )
	throws Exception {
	// PKCS7 or 5: 'AES/CBC/PKCS7Padding'/5
	Cipher cipher = Cipher.getInstance( UtilCodec.INIT_cryptoAes );
	int preBlocks = (16 + keyData.length + 15) / 16;
	int postBlocks = 5;
	byte[] iv16 = UtilMisc
		.createHeaderSalt16( Dib2Constants.magicBytes, new byte[] { (byte) preBlocks, (byte) postBlocks, (byte) keyInfo }, this );
	SecretKeySpec skspec = new SecretKeySpec( key, "AES" );
	cipher.init( Cipher.ENCRYPT_MODE, skspec, new IvParameterSpec( iv16 ) ); // new byte[16]));
//	assert Arrays.equals( iv16, cipher.getParameters().getParameterSpec( IvParameterSpec.class ).getIV() );
	byte[] enc = cipher.doFinal( compressedData, from, to - from );
	PKCS8EncodedKeySpec ecSpec = new PKCS8EncodedKeySpec( signatureKey );
	Signature dsa = Signature.getInstance( "SHA256withECDSA" );
	dsa.initSign( KeyFactory.getInstance( "EC" ).generatePrivate( ecSpec ) );
	dsa.update( keyData );
	if (keyData.length < ((preBlocks - 1) * 16)) {
		dsa.update( new byte[ ((preBlocks - 1) * 16) - keyData.length ] );
	}
	dsa.update( enc );
	byte[] signature = dsa.sign();
	if (signature.length > (postBlocks * 16)) {
		return null;
	}
	int iPad = signature.length;
	signature = Arrays.copyOf( signature, postBlocks * 16 );
	signature[ iPad ] = (byte) 0x80;
	byte[] out = new byte[ 8 + preBlocks * 16 + enc.length + postBlocks * 16 ];
	System.arraycopy( iv16, 0, out, 8, iv16.length );
	System.arraycopy( keyData, 0, out, 8 + iv16.length, keyData.length );
	System.arraycopy( enc, 0, out, 8 + preBlocks * 16, enc.length );
	System.arraycopy( signature, 0, out, 8 + preBlocks * 16 + enc.length, signature.length );
	out = UtilMisc.toPacket( Dib2Constants.RFC4880_EXP2, null, out, 8, out.length );
	return Arrays.copyOfRange( out, out[ 0 ], out.length );
}

@Override
public byte[] encode4DerivedKey( byte[] compressedData, int from, int to, byte[] pass ) throws Exception {
	byte[] out = UtilCodec.encrypt( compressedData, from, to, pass,
		UtilMisc.createHeaderSalt16( Dib2Constants.magicBytes, new byte[] { 1, 0, 0 }, this ),
		8 );
	int end = out.length - 8;
	out = UtilMisc.toPacket( Dib2Constants.RFC4880_EXP2, null, out, 8, end );
	return Arrays.copyOfRange( out, out[ 0 ], end );
}

@Override
public byte[] decode( byte[] data, int from, int to, byte[] keyOrPass, byte[] signatureKey ) throws Exception {
	int offs = from;
	if (data[ offs ] == (byte) Dib2Constants.RFC4880_EXP2) {
		offs += UtilMisc.getPacketHeaderLen( data, offs );
	}
	if (data[ offs ] != Dib2Constants.magicBytes[ 0 ]) {
		return null;
	}
	int headerlen = 16;
	// Check version and key info (0 = local).
	byte[] info = new byte[ 3 ];
	final int version = UtilMisc.getHeaderInfo( data, offs, info );
	final byte keyInfo = info[ 2 ];
	if ((1 < version) && (0 != keyInfo)) {
		Cipher cipher = Cipher.getInstance( UtilCodec.INIT_cryptoAes );
		byte[] iv = Arrays.copyOfRange( data, offs, offs + 16 );
		SecretKeySpec skspec = new SecretKeySpec( keyOrPass, "AES" );
		cipher.init( Cipher.DECRYPT_MODE, skspec, new IvParameterSpec( iv ) );
		headerlen = 16 * data[ offs + 2 + Dib2Constants.magicBytes.length ];
		int trailerlen = 16 * data[ offs + 3 + Dib2Constants.magicBytes.length ];
		if (0 >= trailerlen) {
			Dib2Config.log( "aes decode", "Missing signature." );
			return null;
		}
		byte[] signature = Arrays.copyOfRange( data, to - trailerlen, to );
		for (int i0 = signature.length - 1; i0 > 40; -- i0) {
			if (signature[ i0 ] == (byte) 0x80) {
				signature = Arrays.copyOf( signature, i0 );
				break;
			}
		}
		X509EncodedKeySpec ecSpec = new X509EncodedKeySpec( signatureKey );
		Signature dsa = Signature.getInstance( "SHA256withECDSA" );
		dsa.initVerify( KeyFactory.getInstance( "EC" ).generatePublic( ecSpec ) );
		dsa.update( data, offs + 16, to - trailerlen - offs - 16 );
		if (!dsa.verify( signature )) {
			Dib2Config.log( "aes decode", "Unexpected signature." );
			return null;
		}
		byte[] out = cipher.doFinal( data, offs + headerlen, to - trailerlen - offs - headerlen );
		return out;
	}
	if (3 > version) {
		// Waiting for Android support ...
		return null; // UtilCodec.decrypt256( data, offs + headerlen, to, keyOrPass, Arrays.copyOfRange( data, offs, offs + 16 ) );
	}
	return UtilCodec.decrypt( data, offs + headerlen, to, keyOrPass, Arrays.copyOfRange( data, offs, offs + 16 ) );
}

@Override
public byte getMethodTag() {
	return 'A'; // AES
}

//@Override
//public StringBuffer getLog() {
//	return buf;
//}

@Override
public byte[] getInitialValue( int len ) {
	byte[] rand = new byte[ len ];
	new SecureRandom().nextBytes( rand );
	return rand;
}

//=====
}
