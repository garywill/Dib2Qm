// Copyright (C) 2016,2017  Roland Horsch <gx work s{at}g mail.c om>.
// See dibdib/assets/license.txt for detailed information.
// This part is new code for working with QuickMSG.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package localhost.optional.chat;

import net.sourceforge.dibdib.all.join.ContextIf;

import net.sourceforge.dibdib.all.join.PrivProvIf;

import net.vreeken.quickmsg.attachment;

public class Pgp {
//=====

public static String my_user_id = null;
protected int miProv = 0;
/** First one is default for sending. */
public static PrivProvIf[] privProvs = null; // EcDhQm(), Pgp_1()

public Pgp( ContextIf app_context ) {
	// To be set whenever DB is used.
//	if (null == Dib2Config.qContextQm) {
//		Dib2Config.qContextQm = app_context;
//	}
	my_user_id = app_context // Dib2Config.qContextQm
		.get( "email_address", null );
	for (PrivProvIf pp : Pgp.privProvs) {
		pp.init( app_context );
	}
}

public void setProv( int xiProv ) {
	miProv = xiProv;
}

public void load_keys() {
	for (PrivProvIf pp : Pgp.privProvs) {
		pp.load_keys();
	}
}

public attachment pgpmime_id() {
	return Pgp.privProvs[ miProv ].pgpmime_id();
}

public attachment encrypt_sign( attachment unenc, String to ) {
	return Pgp.privProvs[ miProv ].encrypt_sign( unenc, to );
}

public attachment key_attachment( String member ) {
	return Pgp.privProvs[ miProv ].key_attachment( member );
}

/**
 * @param xInputStream
/** @param yKeyFound
/** @return false if error or if value has not changed
 */
public boolean public_keyring_add_key( byte[] xInputStream, String[] yKeyFound ) {

	/* WRAPPER:
	public boolean public_keyring_add_key(byte[] is, String[] added) {
		Object oldkr = public_keyring_collection;
		added[0] = public_keyring_add_key(new ByteArrayInputStream( is));
		return oldkr != public_keyring_collection;
	}
	*/

	if (Pgp.privProvs[ miProv ].public_keyring_add_key( xInputStream, yKeyFound )) {
		return true;
	}
	for (PrivProvIf pp : Pgp.privProvs) {
		if (pp.public_keyring_add_key( xInputStream, yKeyFound )) {
			return true;
		}
	}
	return false;
}

public attachment decrypt_verify( attachment attachment ) {
	attachment att = Pgp.privProvs[ miProv ].decrypt_verify( attachment );
	if (null != att) {
		return att;
	}
	for (PrivProvIf pp : Pgp.privProvs) {
		att = pp.decrypt_verify( attachment );
		if (null != att) {
			return att;
		}
	}
	return null;
}

public String fingerprint( String addr ) {
	return Pgp.privProvs[ miProv ].fingerprint( addr );
}

public void public_keyring_remove_by_address( String addr ) {
	for (PrivProvIf pp : Pgp.privProvs) {
		pp.public_keyring_remove_by_address( addr );
	}
}

//=====
}
