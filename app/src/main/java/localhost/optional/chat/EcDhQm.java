// Copyright (C) Roland Horsch and others:
// -- Changes:  Copyright (C) 2017,2018  Roland Horsch <gx work s{at}g mail.c om>.
// -- Structure/API: Copyright (C) 2014/2015  Jeroen Vreeken.
// All under GNU license:
// See dibdib/assets/license.txt for detailed information.
// This part is based on 'pgp' from net.vreeken.quickmsg.
// ABSOLUTELY NO WARRANTY. Formatted by Eclipse.

package localhost.optional.chat;

import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import javax.activation.*;
import javax.crypto.KeyAgreement;
import javax.mail.util.ByteArrayDataSource;
import net.sourceforge.dibdib.all.join.*;
import net.sourceforge.dibdib.all.util.UtilMisc;
import net.sourceforge.dibdib.config.Dib2Config;
import net.vreeken.quickmsg.attachment;

/** Static keys for authentication, session keys for encryption.
 * Key info for alternating keys (1/2/0): CAAU = Count, Added/Include, Acknowledge, Use.
 */
public class EcDhQm implements PrivProvIf {

protected ContextIf prefs;

@Override
public void init( ContextIf app_context ) {
	Dib2Config.log( "ecdh init", "a" );
//	if (null == Dib2Config.qContextQm) {
//		Dib2Config.qContextQm = app_context;
//	}
	prefs = app_context; // Dib2Config.qContextQm;
	if (0 >= prefs.get( "KEY.0.SIG.ECDSA256.S", "" ).length()) {
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance( "EC" );
		} catch (NoSuchAlgorithmException e) {
			Dib2Config.log( "ecdh init", "err1 " + e );
			return;
		}
		// Waiting for Java/BC/Android to support other curves ...
		kpg.initialize( 256 );
		KeyPair kp = kpg.generateKeyPair();
		String sigPk = UtilMisc.toHexString( kp.getPublic().getEncoded() );
		String sigSk = UtilMisc.toHexString( kp.getPrivate().getEncoded() );
		prefs.set( "KEY.0.SIG.ECDSA256.P", sigPk );
		prefs.set( "KEY.0.SIG.ECDSA256.S", sigSk );
		Dib2Config.log( "ecdh init", "created " + fingerprint( "0" ) );
	} else {
		Dib2Config.log( "ecdh init", "fi " + fingerprint( "0" ) );
	}
}

@Override
public boolean load_keys() {
	return 0 < prefs.get( "KEY.0.SIG.ECDSA256.S", "" ).length();
}

protected byte[] getPkEncoded( String addr ) {
	String which = (prefs.get( "email_address", "0" ).equalsIgnoreCase( addr )) ? "0" : addr;
	byte[] pk = UtilMisc.fromHexString( prefs.get( "KEY." + which + ".SIG.ECDSA256.P", null ) );
	return new X509EncodedKeySpec( pk ).getEncoded();
}

@Override
public String fingerprint( String addr ) {
	MessageDigest digest;
	try {
		// Waiting for standard format with SHA256 that includes address/ hash extension ...
		digest = MessageDigest.getInstance( "SHA1" );
	} catch (NoSuchAlgorithmException e) {
		return null;
	}
	byte[] pk = getPkEncoded( addr );
	digest.update( (byte) 0x99 );
	digest.update( (byte) (pk.length >> 8) );
	digest.update( (byte) pk.length );
	digest.update( pk );
	return UtilMisc.toHexString( digest.digest() );
}

@Override
public boolean public_keyring_add_key( byte[] xInputStream, String[] xyAddr ) {
	boolean changed = false;
	if (xyAddr[ 0 ].contains( "@" )) {
		// TODO check ...
		String pk = UtilMisc.toHexString( xInputStream );
		if ((null == pk) || (10 >= pk.length())) {
			return false;
		}
		String old = prefs.get( "KEY." + xyAddr[ 0 ] + ".SIG.ECDSA256.P", null );
		prefs.set( "KEY." + xyAddr[ 0 ] + ".SIG.ECDSA256.P", pk );
		///// DEPRECATED: Use it as extra session key:
		prefs.set( "KEY." + xyAddr[ 0 ] + ".ENC.ECDH.P1", pk ); // P1/S1
		prefs.set( "KEY.0.ECDH." + xyAddr[ 0 ] + ".P1", prefs.get( "KEY.0.SIG.ECDSA256.P", null ) );
		prefs.set( "KEY.0.ECDH." + xyAddr[ 0 ] + ".S1", prefs.get( "KEY.0.SIG.ECDSA256.S", null ) );
		// Use it as initial session key:
		prefs.set( "KEY.0.ECDH." + xyAddr[ 0 ] + ".CAAU", "0000" ); //0111" );
		changed = !pk.equals( old );
	}
	return changed;
}

@Override
public attachment key_attachment( String keyuser ) {
	Dib2Config.log( "key_attachment", "generate public key" );
	attachment attachment = new attachment();
	byte[] pk = getPkEncoded( keyuser );
	attachment.datahandler = new DataHandler( new ByteArrayDataSource( pk, "application/pgp-keys" ) );
	attachment.name = fingerprint( keyuser ) + ".asc";
	prefs.set( "KEY.0.ECDH." + keyuser + ".CAAU", "0000" ); // 0101" );
	return attachment;
}

@Override
public attachment pgpmime_id() {
	attachment a = new attachment();
	a.disposition = "inline";
	DataSource ds;
	try {
		ds = new ByteArrayDataSource( "Version: 1\n", "application/pgp-encrypted" );
	} catch (IOException e) {
		return null;
	}
	a.datahandler = new DataHandler( ds );
	a.name = "pgp_mime_id";
	return a;
}

public byte[] getAesKey( String to, char usedKeyNumber, byte[] optTempKey ) {
	byte[] out = null;
	try {
		//TODO buffer for the value
		byte[] xPk = optTempKey;
		if ((null == xPk) || (10 >= xPk.length)) {
			xPk = UtilMisc.fromHexString( prefs.get( "KEY." + to //,
				+ ((usedKeyNumber <= '0') ? ".SIG.ECDSA256.P" : (".ENC.ECDH.P" + usedKeyNumber)), //,
				"" ) );
		}
		if ((null == xPk) || (10 >= xPk.length)) {
			return null;
		}
		byte[] mySk = UtilMisc.fromHexString( prefs.get( (usedKeyNumber <= '0')//,
		? ("KEY.0.SIG.ECDSA256.S") //,
			: ("KEY.0.ECDH." + to + ".S" + usedKeyNumber), //,
			null ) );
		byte[] myPk = UtilMisc.fromHexString( prefs.get( (usedKeyNumber <= '0')//,
		? ("KEY.0.SIG.ECDSA256.P") //,
			: ("KEY.0.ECDH." + to + ".P" + usedKeyNumber), //,
			null ) );
		X509EncodedKeySpec pkSpec = new X509EncodedKeySpec( xPk );
		PKCS8EncodedKeySpec skSpec = new PKCS8EncodedKeySpec( mySk );
		KeyFactory kfp = KeyFactory.getInstance( "EC" );
		KeyFactory kfs = KeyFactory.getInstance( "EC" );
		KeyAgreement ka = KeyAgreement.getInstance( "ECDH" );
		ka.init( kfs.generatePrivate( skSpec ) );
		ka.doPhase( kfp.generatePublic( pkSpec ), true );
		byte[] sharedSecret = ka.generateSecret();
		///// Derive a key from shared secret.
		boolean meFirst = false;
		for (int i0 = 0; (i0 < myPk.length) && (i0 < xPk.length); ++ i0) {
			if (myPk[ i0 ] != xPk[ i0 ]) {
				meFirst = (myPk[ i0 ] > xPk[ i0 ]);
				break;
			}
		}
		MessageDigest hash = MessageDigest.getInstance( "SHA-256" );
		hash.update( sharedSecret );
		hash.update( meFirst ? myPk : xPk );
		hash.update( meFirst ? xPk : myPk );
		out = hash.digest();
	} catch (Exception e) {
		return null;
	}
	return out;
}

@Override
public attachment encrypt_sign( attachment unenc, String to ) {
	String filename = unenc.name;
	byte[] enc;
	String caau = prefs.get( "KEY.0.ECDH." + to + ".CAAU", "" );
	String hint = "xxxxx" + prefs.get( "KEY.0.ECDH." + to + ".P" + caau.charAt( 3 ), "" );
	Dib2Config.log( "encrypt sign", to + " caau " + caau + " " + hint.substring( hint.length() - 5 ) );
	if (4 != caau.length()) {
		return null;
	}
	try {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		unenc.datahandler.writeTo( os );
		unenc = null;
		byte[] dat = os.toByteArray();
		if (('0' >= caau.charAt( 0 )) && ('0' >= caau.charAt( 1 ))) { // || (caau.charAt( 1 ) != caau.charAt( 2 ))) {
			///// Countdown done. Need a new key.
			KeyPairGenerator kpg;
			kpg = KeyPairGenerator.getInstance( "EC" );
			kpg.initialize( 256 );
			KeyPair kp = kpg.generateKeyPair();
			String pk = UtilMisc.toHexString( kp.getPublic().getEncoded() );
			String sk = UtilMisc.toHexString( kp.getPrivate().getEncoded() );
			char id = ('1' == caau.charAt( 3 )) ? '2' : '1';
			caau = "X" + id + caau.charAt( 2 ) + caau.charAt( 3 );
			prefs.set( "KEY.0.ECDH." + to + ".P" + id, pk );
			prefs.set( "KEY.0.ECDH." + to + ".S" + id, sk );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau );
			Dib2Config.log( "encrypt sign", "new key " + pk.substring( 0, 3 ) + " caau " + caau );
		} else if (((caau.charAt( 1 ) == caau.charAt( 3 )) || (caau.charAt( 2 ) == caau.charAt( 3 ))) //,
			&& ('0' != caau.charAt( 3 ))) {
			caau = "" + caau.charAt( 0 ) + '0' + caau.substring( 2 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau );
		} else if ('0' != caau.charAt( 1 )) {
			caau = "X" + caau.substring( 1 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau );
		} else if (('9' == caau.charAt( 0 )) && ('0' >= caau.charAt( 2 ))) {
			caau = "800" + caau.charAt( 3 );
			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau );
		}
		byte[] aesKey = getAesKey( to, //
			caau.charAt( 3 ), // ('0' == caau.charAt( 3 )) ? caau.charAt( 1 ) : caau.charAt( 3 )
			null );
		byte[] compr = Dib2Config.csvCodecs[ 0 ].compress( dat, 0, dat.length );
		final int coFrom = compr[ 0 ] + UtilMisc.getPacketHeaderLen( compr, compr[ 0 ] );
		final int coTo = coFrom + UtilMisc.getPacketLen( compr, 1 + compr[ 0 ] );
		int caauInfo = 0;
		String keyData = UtilMisc.toHexString( prefs.get( "email_address", null ).getBytes( "UTF-8" ) );
		if (160 < keyData.length()) {
			Dib2Config.log( "encrypt sign", "error addr len " + keyData.length() );
			return null;
		}
//		if ((caau.charAt( 1 ) == caau.charAt( 2 )) && (caau.charAt( 2 ) == caau.charAt( 3 ))) {
//			// Session established, start countdown.
//			caau = "" + (('X' == caau.charAt( 0 )) ? 9 : ((caau.charAt( 0 ) + 1) % 10)) + caau.substring( 1 );
//			prefs.set( "KEY.0.ECDH." + to + ".CAAU", caau );
//		} else {
		if ('0' != caau.charAt( 1 )) {
			caauInfo = (caau.charAt( 1 ) & 0x3) << 4;
			keyData += " " + prefs.get( "KEY.0.ECDH." + to + ".P" + caau.charAt( 1 ), null );
		}
		keyData += "                ".substring( 0, 16 ).substring( 15 - keyData.length() % 16 );
		keyData += ":";
		caauInfo |= ((caau.charAt( 2 ) & 0x3) << 2) | (caau.charAt( 3 ) & 0x3);
		byte[] sigKey = UtilMisc.fromHexString( prefs.get( "KEY.0.SIG.ECDSA256.S", null ) );
		// caauInfo == keyInfo (!= 0 !!!).
		caauInfo |= 0x40;
		enc = Dib2Config.csvCodecs[ 0 ].encode( compr, coFrom, coTo, aesKey, caauInfo, keyData.getBytes(), sigKey );
	} catch (Exception e) {
		Dib2Config.log( "encrypt sign", "error " + caau + " " + e + e.getMessage() );
		return null;
	}
	attachment a_out = new attachment();
	a_out.name = filename + ".asc";
	DataSource ds = new ByteArrayDataSource( enc, "application/octet-stream" );
	a_out.datahandler = new DataHandler( ds );
	return a_out;
}

@Override
public attachment decrypt_verify( attachment att ) {
	String filename = att.name;
	byte[] dat;
	String addr = "";
	int caauInfo = 0;
	byte[] newKey = new byte[ 0 ];
	byte[] aesKey = null;
	try {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		att.datahandler.writeTo( os );
		att = null;
		dat = os.toByteArray();
		int offs = 0;
		if (dat[ offs ] != (byte) Dib2Config.RFC4880_EXP2) {
			Dib2Config.log( "decrypt_verify", "unexpected format " + offs );
			return null;
		}
		offs += UtilMisc.getPacketHeaderLen( dat, offs );
		if ((dat[ offs ] != Dib2Config.magicBytes[ 0 ]) || (dat[ offs + 1 ] != Dib2Config.magicBytes[ 1 ])) {
			Dib2Config.log( "decrypt_verify", "unexpected format " + offs );
			return null;
		}
		if ('A' != dat[ offs + 3 ]) {
			Dib2Config.log( "decrypt_verify", "unexpected format " + offs );
			return null;
		}
		int hdlen = 16 * (dat[ offs + 4 ] & 0xff);
		caauInfo = dat[ offs + 6 ] & 0x3f;
		for (int i0 = 16; i0 < hdlen; ++ i0) {
			if ((' ' > dat[ offs + i0 + 1 ]) || (':' == dat[ offs + i0 ])) {
				++ i0;
				addr = new String( Arrays.copyOfRange( dat, offs + 16, offs + i0 ) );
				offs = ((offs + i0) / 16) * 16;
				while (addr.endsWith( ":" )) {
					addr = addr.substring( 0, addr.length() - 1 );
				}
				addr = addr.trim();
				break;
			}
		}
		if ((16 >= hdlen) || (32 > offs) || (0 >= addr.length())) {
			Dib2Config.log( "decrypt_verify", "address not found/ " + hdlen );
			return null;
		}
		if (0 < addr.indexOf( ' ' )) {
			// New key arrived.
			newKey = UtilMisc.fromHexString( addr.substring( 1 + addr.indexOf( ' ' ) ) );
			addr = addr.substring( 0, addr.indexOf( ' ' ) );
		}
		addr = new String( UtilMisc.fromHexString( addr ), "UTF-8" );
		{
			String caau = prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000" );
			String hint = "xxxxx" + prefs.get( "KEY.0.ECDH." + addr + ".P" + (caauInfo & 3) // caau.charAt( 3 )
			, "" );
			Dib2Config
				.log(
					"decrypt_verify",
					addr + " " + caauInfo + " " + dat.length + " " + newKey.length + " caau " + caau + " "
						+ hint.substring( hint.length() - 5 ) );
		}
		if ((3 << 4) <= caauInfo) {
			// Bad format.
			newKey = new byte[ 0 ];
		} else if ((1 << 4) > caauInfo) {
			newKey = new byte[ 0 ];
			// ACK == USED?
//			if ((caauInfo >> 2) == (caauInfo & 3)) {
//				// Session established.
//				prefs.set( "KEY.0.ECDH." + addr + ".CAAU", prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0111" ).substring( 0, 3 )
//					+ (caauInfo & 3) );
//			}
		} else if (0 == (caauInfo & 3)) {
			Dib2Config.log( "decrypt_verify", "sync " + caauInfo );
			///// TODO: Selectable fallback mechanism.
//			caauInfo |= (caauInfo >>> 4) & 3;
			aesKey = getAesKey( addr, '0' // (char) ('0' + (caauInfo & 3))
				, null ); //, newKey );
//			String caau = prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0111" );
//			prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "X000" ); //"0" + caau.substring( 1 ) );
		}
	} catch (Exception e) {
		Dib2Config.log( "decrypt_verify", "error " + e + e.getMessage() );
		return null;
	}
	try {
		if (null == aesKey) {
			aesKey = getAesKey( addr, (char) ('0' + (caauInfo & 3)), null );
		}
		byte[] sigKey = UtilMisc.fromHexString( prefs.get( "KEY." + addr + ".SIG.ECDSA256.P", null ) );
		dat = Dib2Config.csvCodecs[ 0 ].decode( dat, 0, dat.length, aesKey, sigKey );
		if (null == dat) {
			//if (16 < key.length) ...;
			Dib2Config.log( "decrypt_verify", "error regarding signature?" );
			prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000" );
			return null;
		}
		dat = Dib2Config.csvCodecs[ 0 ].decompress( dat, dat.length );
		///// After having checked the signature.
		String caau = prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000" );
		int included = (caauInfo >>> 4) & 3;
		int acknowledged = (caauInfo >>> 2) & 3;
		int used = caauInfo & 3;
		if ((16 < newKey.length) && (0 != included)) {
			prefs.set( "KEY." + addr + ".ENC.ECDH.P" + ((caauInfo >>> 4) & 3), UtilMisc.toHexString( newKey ) );
		} else {
			included = 0;
		}
		// To be acknowledged.
		caau = caau.substring( 0, 2 ) + included + caau.charAt( 3 ); // ((caauInfo >>> 2) & 3) );
		if (included == used) {
			caau = "0000";
		} else if (acknowledged == used) {
			caau = ((0 == used) ? "00" : "90") + caau.charAt( 2 ) + used;
		} else if ((included == acknowledged) && (0 != acknowledged) && (acknowledged == (caau.charAt( 1 ) & 3))) {
			caau = ("90" + acknowledged) + acknowledged;
		} else if ((0 == used) && (included == acknowledged)) {
			caau = ("90" + included) + included;
		} else if (('8' >= caau.charAt( 0 )) && ('1' <= caau.charAt( 0 )) && (used == (caau.charAt( 3 ) & 3))) {
			// Countdown:
			caau = "" + ((char) (caau.charAt( 0 ) - 1)) + caau.substring( 1 ); // + caau.charAt( 2 ) + used;
//		} else if (used != caau.charAt( 3 )) {
//			caau = "000" + used;
		}
		prefs.set( "KEY.0.ECDH." + addr + ".CAAU", caau );
	} catch (Exception e) {
		Dib2Config.log( "decrypt_verify", "(key sync?) error " + e + e.getMessage() );
		String caau = prefs.get( "KEY.0.ECDH." + addr + ".CAAU", "0000" );
		String pk = prefs.get( "KEY." + addr + ".SIG.ECDSA256.P", null );
		///// Verification might have succeeded => let the info pass a single time.
		if (('0' == caau.charAt( 3 )) || (null == pk) || (10 >= pk.length())) {
			return null;
		}
		dat = "(KEY SYNC ERROR)".getBytes();
		// Fallback/ Try to revert.
		prefs.set( "KEY.0.ECDH." + addr + ".CAAU", "0000" ); //((caauInfo & 3) == 1) ? "0120" : "0210" );
	}
	attachment a_out = new attachment();
	a_out.name = filename + ".asc";
	DataSource ds = new ByteArrayDataSource( dat, "application/octet-stream" );
	a_out.datahandler = new DataHandler( ds );
	return a_out;
}

@Override
public void public_keyring_remove_by_address( String addr ) {
	if (prefs.get( "email_address", "0" ).equalsIgnoreCase( addr ) || (1 >= addr.length())) {
		// Must not remove myself.
		return;
	}
	prefs.set( "KEY." + addr + ".SIG.ECDSA256.P", null );

}
}
