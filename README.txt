Dib2Qm
======

Messenger for secured chats and groups: works via e-mail server.

(It is based on IMAP and QuickMSG, and therefore
does not depend on a centralized server).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version (or, but only for the 'dibdib.all'
    parts of the source code, the matching LGPL variant of GNU).

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License and the LICENSE file for more details.


An installable APK file can be downloaded from:

https://www.magentacloud.de/lnk/AervLNju

